<h1>Introducción al Desarrollo Backend</h1>

<h1>Facundo García Martoni</h1>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [Todo lo que aprenderás sobre el desarrollo backend](#todo-lo-que-aprenderás-sobre-el-desarrollo-backend)
- [2. Fundamentos del desarrollo web](#2-fundamentos-del-desarrollo-web)
  - [Yin y Yang de una aplicación: frontend y backend](#yin-y-yang-de-una-aplicación-frontend-y-backend)
  - [Framework vs. librería](#framework-vs-librería)
  - [Cómo se conecta el frontend con el backend: API y JSON](#cómo-se-conecta-el-frontend-con-el-backend-api-y-json)
  - [El lenguaje que habla Internet: HTTP](#el-lenguaje-que-habla-internet-http)
  - [¿Cómo es el flujo de desarrollo de una aplicación web?](#cómo-es-el-flujo-de-desarrollo-de-una-aplicación-web)
  - [El hogar de tu código: el servidor](#el-hogar-de-tu-código-el-servidor)
- [3. Diseño de una API](#3-diseño-de-una-api)
  - [Proyecto: diseño y bosquejo de una API](#proyecto-diseño-y-bosquejo-de-una-api)
  - [Proyecto: diseñando los endpoints de los Tweets](#proyecto-diseñando-los-endpoints-de-los-tweets)
  - [Proyecto: diseñando los endpoints para los usuarios](#proyecto-diseñando-los-endpoints-para-los-usuarios)
- [4. Conclusiones](#4-conclusiones)
  - [Qué lenguaje y framework escoger para backend](#qué-lenguaje-y-framework-escoger-para-backend)
  - [Comienza tu camino](#comienza-tu-camino)

# 1. Introducción

## Todo lo que aprenderás sobre el desarrollo backend

- Bases de Backend

- Servidor

- Json

- API

# 2. Fundamentos del desarrollo web

## Yin y Yang de una aplicación: frontend y backend

![fb3.png](https://static.platzi.com/media/user_upload/fb3-400a4ee9-bf79-4dd9-ac8e-eb26fc68841c.jpg)

**Diferencia entre Next.Js y Nest.Js**
	Next.Js: Es un framework de la librería React.js que nos hace la vida mucho mas fácil.
NestJs: Es un framework de Node.js (Es el que se especifico en el diagrama) este funciona para trabajar sobre Node.js que funciona del lado del servidor con Javascript.

Las aplicaciones web se componen de

- **Frontend:**

1. Código
   **HTML** (HyperText Markup Language)
   **CSS** (cascade stylesheet)
   **JS **(Javascript)
   **PHP**(Hypertext Preprocessor) permite hacer algunas cosas.
2. **Diseño**
   UX (Experiencia de usuario)
   UI (Interfaz del Usuario).

- **Backend:**
  Javascript (Node.js)
  PHP
  JAVA
  GO
  Rust
  Ruby
  Python > Fast API, Flask y Django

### Front End

Se puede entender el frontend como la interfaz de usuario, es decir, la parte de la aplicación con la que el usuario interactúa. El “Frontend” suele ser un conjunto de archivos (HTML, CSS, JS, entre otros) que un servidor nos envía cuando consultamos una dirección WEB, el servidor puede realizar esto gracias a aplicaciones que permiten exponer en red estos archivos.

También podríamos mencionar que el “Frontend” contiene toda la lógica de programación (JS) que se ejecutará del lado del cliente, es decir, cuando consultemos la página o aplicación web habrá código JS corriendo en nuestro navegador.

### Backend

Contiene toda la lógica de negocio de nuestra aplicación, la cual se ejecuta dentro de un servidor WEB; como mencionaba el instructor, es el motor de nuestro proyecto.

Una de las tareas más comunes del backend es tratar con datos, por ejemplo:
Si tenemos una aplicación que muestra registros de productos en una página WEB, necesitaremos código en el servidor (Backend) que busque estos registros en una base de datos, los inserte en un template (HTML) y nos envíe a nuestro navegador por medio del protocolo HTTP, o si la dinámica es SPA, que nos envíe estos datos en formato JSON.

## Framework vs. librería

**Framework:** Será la base sobre la cual podras construir y desarrollar tu proyecto, incluye todas las herramientas necesarias para completarlo (incluye librerías, estándares y reglas).
**Librería:** Solo aborda una utilidad especifica, pudiendo agregar más de una en tu proyecto. Eso si, asegurate que no interfieran con el código de otra librería.

> Recuerda: Ninguno es mejor que el otro, todo va a depender de la necesidad de tu proyecto 😉

**Librerías en Python:**

- requests.
- tqdm.
- pillow.
- scrapy.
- numpy.
- pandas.
- scapy.
- matplotlib.

**Frameworks en Python:**

- Fast API
- Flask
- Django

**Framework:**

> Conjunto de librerias, reglas y estandares para construir un producto digital

[![img](https://www.google.com/s2/favicons?domain=https://www.youtube.com/s/desktop/9745c18d/img/favicon.ico)Diferencia entre Librería y Framework explicada | Clase abierta - YouTube](https://www.youtube.com/watch?v=yC6wR3szGz8&ab_channel=Platzi)

## Cómo se conecta el frontend con el backend: API y JSON

**Términos hablados en la clase:**

**API** —> Application Programming Interface
**SOAP** —> Simple Object Access Protocol
**REST** —> Representational State Transfer
**XML** —> Extensible Markup Language
**JSON** —> JavaScript Object Notation

## API y JSON

Una **API** es una sección del motor, que permite que el frontend, pueda comunicarse con el backend, y que puedan haber mensajes de ida y vuelta.

No pude evitar agregar esta referencia xD.

![https://i.redd.it/gxswf46j5vj61.jpg](https://i.redd.it/gxswf46j5vj61.jpg)

Existen dos maneras de comunicarse:

- **SOAP:** Single Object Access Protocol, usaba (y sigue usando) **XML (**Extensible Markup Language). Aquí va un ejemplo de XML:

```xml
<?xml version="1.0"?>
<note>
	<to>Miguel</to>
	<from>Facundo</from>
	<heading>Recordatorio</heading>
	<body>No olvides publicar el curso!</body>
</note>
```

- **REST:** Representational state transfer.

Vamos a tener que confiar en **JSON (**JavaScript Object Notation)

**JSON** también es conocido como “un diccionario de Python”, y esto es lo mismo que los “objetos de JavaScript”.

> Básicamente API es un sistema de comunicación entre el Frontend y el Backend.

## El lenguaje que habla Internet: HTTP

**H**yper**t**ext **T**ransfer **P**rotocol

**Request**

- Cabezeras: piezas de informacion (Host, Accept-Language)
- Metodos: GET, POST, PUT, DELETE

**Responce**

- body: JSON o diccionarios
- Header: Status Code, Server: aplicativo que permite distribuir, Date, Etag: hast, cahe, Accept-Rnages: como entregamos la info, Content-Length, Content-Type

![img](https://plataforma.josedomingo.org/pledin/cursos/flask/curso/u01/img/dia2.png)

Respuestas informativas (100–199),
Respuestas satisfactorias (200–299),
Redirecciones (300–399),
Errores de los clientes (400–499),
y errores de los servidores (500–599).

![Captura de pantalla 2021-10-15 173709.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-10-15%20173709-c9329110-e601-4ec1-af80-1fed45b2cbbf.jpg)

![Screenshot from 2021-10-25 07-53-55.png](https://static.platzi.com/media/user_upload/Screenshot%20from%202021-10-25%2007-53-55-6a4e9ded-e88f-40d9-9ffb-88eeb7cf8865.jpg)

![Screenshot from 2021-10-25 07-54-18.png](https://static.platzi.com/media/user_upload/Screenshot%20from%202021-10-25%2007-54-18-fdafe473-384e-49af-a403-e42e26af8be1.jpg)

![Screenshot from 2021-10-25 07-54-31.png](https://static.platzi.com/media/user_upload/Screenshot%20from%202021-10-25%2007-54-31-89e37fb7-40f2-42d1-97fa-d2a6dbe8587d.jpg)



![Preview](https://i.imgur.com/8Ql7ST7.jpg)[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/es/docs/Web/HTTP/Status/favicon-48x48.97046865.png)Códigos de estado de respuesta HTTP - HTTP | MDN](https://developer.mozilla.org/es/docs/Web/HTTP/Status)

## ¿Cómo es el flujo de desarrollo de una aplicación web?

### **La famosa nube** 😮

Así se vería lo que llamamos nube, lo que realmente son servidores o computadoras trabajando 24/7 .

![sala-de-servidores-dedicados.jpg](https://static.platzi.com/media/user_upload/sala-de-servidores-dedicados-122ca6b2-5934-4478-af5d-ac44a2ff464d.jpg)

Es importante aclarar que Continuos Delivery y Continous Deployment no son términos intercambiables sino diferentes

![Captura de pantalla 2021-10-15 173709.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-10-15%20173709-8ac7fb17-ada9-4dcf-a81b-41e410df7f07.jpg)

Entorno local (Editor de Código → Git → mi local) Pasar del entorno local al servidor se llama **deploy.**

Normalmente se hace un **push** a un repositorio remoto (**Github),** el cual es un servidor que solo contiene el código de tu proyecto.

Cada vez que uno trae código de Github a un entorno local es un **pull.**

Lo que se hace normalmente en un entorno profesional es **CI/CD (Continuous Integration and Continuous Delivery) o Continous Deployment.** Se prueba (**testea),** si funciona bien, el código se va a al **server (production).**

El proyecto se guarda en **production** en un **domain (dominio).**

Se puede comprar un dominio, por ejemplo en **namecheap.**

En mi computadora, las cosas van a vivir en un **entorno local:**

Dirección y puerto, por ejemplo:

```sh
127.0.0.1 : 8000
    IP      Port
```

[**Continuous Integration, Continuous Delivery y Continuous Deployments**](https://platzi.com/blog/continuous-integration-continuous-delivery-y-continuous-deployments/)

[![img](https://www.google.com/s2/favicons?domain=https://www.namecheap.com/assets/img/nc-icon/favicon.ico)Buy a domain name - Register cheap domain names from $0.99 - Namecheap](https://www.namecheap.com/)

## El hogar de tu código: el servidor

**IaaS**
Infraestructura como servicio. Es el tipo de servicio que nos permite indicarle al provdedor del servidor las especificaciones que requerimos en nuestro proyecto (RAM, SSD, CPU). Las más populares de este tipo son: AWS, Azure y Digital Ocean

**PaaS**
Plataforma como servicio. Es el tipo de servicio que nos permite indicarle al provdedor del servidor que se encargue de realizar todas las actualizaciones que requerimos en nuestra app. Solo nos permite elegir que cosas particulares requere nuestro proyecto a nivel general a través de una interfaz. Las más populares de este tipo son: Firebase, Heroku y Google App Engine

**SaaS**
Software como servicio. Esta opción nos permite utilizar una aplicación de un proveedor para hacer funcionar nuestro proyecto. Las más populares de este tipo son: Slack, Wordpress y Google Docs.

**IaaS: Infrastructure as a service**

- AWS
- Microsoft Azure
- Digital Ocean

------

**PaaS**

- Firebase
- Heroku
- Google App Engine

------

**SaaS: Software as a service**

- Slack
- Workdpress
- Google Docs

## El servidor: Hogar de tu código

Es una computadora que contiene una aplicación y la distribuye mediante el protocolo HTTP.

## La nube

Son servidores juntos que se encuentan en algún lugar del mundo funcionando y distribuyendo aplicaciones.

Estos se encuentran en un sitio llamado Data Centers

## Data Centers

Son los lugares donde se encuentran los servidores, los cuales almacenan datos (como tu página web, o una aplicación como Facebook)

## Hosting

Es el acto de guardar tu aplicación en un server.

Un espacio en un servidor, donde tu aplicación será guardada.

Existen diferentes tipos de hosting (formas de guardar aplicaciones):

- **IaaS:** **Infrastructure as a Service**
- **PaaS: Platform as a Service**
- **SaaS: Software as a Service**

### IaaS

Este servicio te da el control de las cosas importantes como

- La cantidad de CPU
- RAM
- SSD

Para esto existen diferentes opciones:

AWS, Microsoft Azure, Digital Ocean.

**Existen dos tipos de IaaS:**

- VPS (Virtual Private Server)

  Servidor privado, recursos solo para tí.

- Shared Hosting

  Compartes los recursos con otras personas.

Investigar AWS, Azure.

### PaaS

El servidor se asegura de actualizar las aplicaciones que hacen que viva tu aplicación:

- Base de datos
- Seguridad
- Firewall ???

Qué es un Firewall?

Los firewall o cortafuegos en su traducción, son son programas de software o dispositivos de hardware que filtran y examinan la información que viaja a través de tu conexión a Internet. Representan la primera defensa porque pueden evitar que un programa malicioso o un atacante obtengan acceso a tu red y a tu información antes de que se produzca cualquier posible daño.

Las PaaS tienen una interfaz gráfica que te permite elegir lo que tu app necesita (como una DB o que tipo de Firewall). Creando así un nivel de abstracción donde no tienes que preocuparte de las especificaciones de los servers.

**Los PaaS son Just Deploy**

Existen diferentes:

- Google APP Engine
- Firebase
- Heroku

### SaaS

Es cuando necesitas un software ya hecho.

Es una aplicación que un provedor te presta para que hagas funcionar tu negocio.

**No Code**

Existen varias opciones:

- Google Docs
- Slack
- WordPress

# 3. Diseño de una API

## Proyecto: diseño y bosquejo de una API

#### CRUD Create Read Update Delete.

Framework

- FastApi
- Django → REST
- Flask

El lugar al que las APIs envían las peticiones y donde vive el recurso, se llama endpoint.

Endpoint / Route / Path

```sh
http://example.com/api/twet
```

Crear usuarios, crear usuario, editar usuario, editar tweet.

Crear, Leer, Eliminar y Actualizar

**CRUD:** Create, Read, Update and Delete.

Casi siempre las aplicaciones tienen un **CRUD,** y podemos usar una **API** para que haga funcionar este **CRUD.**

Y si logro hacer un **frontend** que se comunique a través de una **API.**

**API (Application Program Interface)**

Hay diferentes librerías y frameworks que te permiten construir **API.**

Vamos a usar FastAPI, Django y Flask

Un **Endpoint / Route / Path (conocidos con esos tres nombres),** es una **sección** de la URL de nuestro proyecto.

```sh
http://twitter.com/api/tweets (api/tweets es el **endpoint o route o path)
```

Los **endpoints** que nos van a permitir hacer un **CRUD (Create, Read, Update and Delete).**

Protocolo: **https** , pudiera ser también **http** pero por seguridad se recomienda **https**. [Ver enlace](https://www.google.com/search?q=https&rlz=1C5CHFA_enCL890CL890&sxsrf=AOaemvIyKcUBgVwwbp4OqFksCSDpdPkgZw%3A1634348486782&ei=xi1qYZyGL4TK1sQP1sWI0AI&ved=0ahUKEwicgfnH5s3zAhUEpZUCHdYiAioQ4dUDCA4&uact=5&oq=https&gs_lcp=Cgdnd3Mtd2l6EAMyBAgjECcyBQgAEIAEMgUIABDLATIFCAAQgAQyBQgAEIAEMgUIABCABDIECAAQAzIFCAAQgAQyBQgAEIAEMgQIABADOgcIABBHELADOgcIABCwAxBDOggIABCxAxCDAToICAAQgAQQsQNKBAhBGABQjF1YlGNggXxoAnACeACAAViIAfwBkgEBM5gBAKABAcgBCsABAQ&sclient=gws-wiz)

Dominio: https:twitter.com, el dominio es el nombre que se le da a las dirección IP del servidor para que sea más entendible por los humanos, los encargados de transformar los nombres en IP y viceversa se denominan proveedores DNS.

Endpoints: son las rutas que prosiguen del dominio, y son la forma de acceder a los recursos del sistema.

![api.png](https://static.platzi.com/media/user_upload/api-03f678d4-9a58-4876-9436-220f9dcf519a.jpg)

## Proyecto: diseñando los endpoints de los Tweets

> ***“Un buen Frontend sabe algo de Backend y buen Backend sabe algo de Frontend”***

Para una introducción básica y sin complicar mucho están bien esos endpoints, pero generalmente se usa el mismo endpoint para diferentes operaciones, que las determina el verbo o método http utilizado en la petición al servidor (GET, POST, DELETE…)

 Cada elemento del CRUD tiene su propio verbo o método Http y los más comunes son los siguientes:

- GET para read
- POST para publicar
- PUT para updates
- DELETE para eliminar

Nota importante ⛔️⛔️⛔️

Aqui se simplifica la creación de los `endpoints`, sin embargo hay que saber que cada sigla del CRUD tiene una representación en el mundo de las APIs y se llaman *verbos http* que sencillamente indican en la petición que se realiza al servidor que se quiere hacer.

Aqui la transformacion de CRUD a los verbos HTTP
Create -> POST
Read -> GET
Update -> PUT
Delete -> Delete

https://developer.mozilla.org/es/docs/Web/HTTP/Methods

Seria buena practica mejor definir los endpoints de la siguiente manera

Create a tweet -> `/tweets/` : POST
Read all tweets -> `/tweets/` : GET
Read a tweet -> `/tweets/{id}/` : GET
Update a tweet -> `/tweets/{id}` : PUT
Delete a tweet -> `/tweets/{id}/` : DELETE

La simplificación no tiene nada de malo pero esta simplemente es una forma mas profesional para hacerlo.

![api.png](https://static.platzi.com/media/user_upload/api-cbac34e8-e727-4157-bf71-1ead5859012d.jpg)

![Captura de pantalla de 2021-10-20 13-47-41.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%20de%202021-10-20%2013-47-41-23da2034-59b7-481e-bc5d-b2a1ca7d431c.jpg)

## Proyecto: diseñando los endpoints para los usuarios

![api.png](https://static.platzi.com/media/user_upload/api-a71b0e3c-d476-444d-986a-06aa8ecc1810.jpg)

```python
## Endpoints para Tweets

(GET) /api/tweets => shows all tweets
(POST) /api/post => publish a tweet
(GET) /api/tweets/:id => shows a single tweet
(UPDATE) /api/tweets/:id => updates a tweet
(DELETE) /api/tweets/:id => deletes a tweet

## Endpoints para Usuarios

(GET) /api/users => shows all users
(POST) /api/signup => registers an user
(GET) /api/users/:userID => shows an user
(UPDATE) /api/users/:userID => updates an user
(DELETE) /api/users/:userID => deletes an user
```

```sh
/ users / { user_id } / update
```

y

```sh
/ users / { user_id } / delete
```

Les comparto una lectura que me ayudó a aterrizar los conceptos:

![Captura de pantalla de 2021-10-20 13-47-41.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%20de%202021-10-20%2013-47-41-3997fe10-1bac-4c5a-9ab9-30b0a180e6f4.jpg)

# 4. Conclusiones

## Qué lenguaje y framework escoger para backend

- **Node** => express 😁
- **Python** => django 🐍
- **Go** ==> bin 💙

Django es un fuerte framework en Python, el cual está dirigido al desarrollo web escalable y de buena calidad.

Flask es un micro-framework orientado a API’s o a una gran carga de visitas a una página web. Es apto para proyectos altamente personalizados y sencillos.

PYTHON (Sirve para trabajar con datos e inteligencia artificial)
• Django: El más grande y robusto, para datos tiene un panel de administración muy completo
• Flask: Sirve para trabajar con aplicaciones simples, es fácil de aprender. Sirve para hacer cosas personalizables
• FastApi: Es el framework más rápido en desarrollo y desempeño.

JAVASCRIPT (Es el lenguaje por defecto para frontend pero también sirve para backend). Javascript es una buena opción para convertirte en un programador todo en uno (full stack)
• Express: Es simple, fácil de escribir
• Nest: Está escrito con un nivel de complejidad un poco mayor. Da más ventajas y código aprovechable.

PHP (Tiene muchas salidas laborales, es un lenguaje de desarrollo web que ha tenido una vida bastante larga)
• Laravel: Es fácil, sirve para los primeros proyectos
• Symfony: Un poco más completo y complejo.

JAVA (Uno de los lenguajes que más demanda tiene). Buena opción para desarrollo mobile
• Spring: Sirve para crear backend’s de aplicaciones web

GO (Lenguaje nuevo, veloz)
• Gin: El más popular
• BEEGO: Está en crecimiento

Ruby (Tiene una comunidad sólida)
• Rails: Es muy rápido y flexible.

Python 💚🚀😜👾

## Comienza tu camino

Ser un Full Stack Developer 🧠