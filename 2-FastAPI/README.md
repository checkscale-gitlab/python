<h1>FastAPI: Modularización, Datos Avanzados y Errores</h1>

<h3>Facundo</h3>

<h1>Table of Contents</h1>

- [1. Bienvenida](#1-bienvenida)
  - [¿Qué necesitas para seguir aprendiendo FastAPI?](#qué-necesitas-para-seguir-aprendiendo-fastapi)
- [2. Refactorización y modularización](#2-refactorización-y-modularización)
  - [Response Model](#response-model)
  - [Mejorando la calidad del código: eliminando líneas duplicadas](#mejorando-la-calidad-del-código-eliminando-líneas-duplicadas)
  - [Status Code personalizados](#status-code-personalizados)
- [3. Entradas de datos avanzadas](#3-entradas-de-datos-avanzadas)
  - [Formularios](#formularios)
  - [Resolviendo errores en el código](#resolviendo-errores-en-el-código)
  - [Cookie y Header Parameters](#cookie-y-header-parameters)
  - [Archivos](#archivos)
  - [Utilizando las clases File y UploadFile](#utilizando-las-clases-file-y-uploadfile)
- [4. Manejo de errores](#4-manejo-de-errores)
  - [HTTPException](#httpexception)
- [5. Documentación interactiva](#5-documentación-interactiva)
  - [Comenzando a ordenar nuestra documentación: etiquetas](#comenzando-a-ordenar-nuestra-documentación-etiquetas)
  - [Nombre y descripción de una path operation](#nombre-y-descripción-de-una-path-operation)
  - [Deprecar una path operation](#deprecar-una-path-operation)
- [6. Tu primera API](#6-tu-primera-api)
  - [Presentación del proyecto: Twitter](#presentación-del-proyecto-twitter)
  - [Configuración inicial del proyecto](#configuración-inicial-del-proyecto)
  - [Modelos: User](#modelos-user)
  - [Modelos: Tweet](#modelos-tweet)
  - [Esqueleto de las Path Operations: Users](#esqueleto-de-las-path-operations-users)
  - [Esqueleto de las Path Operations: Tweets](#esqueleto-de-las-path-operations-tweets)
  - [Registrando usuarios](#registrando-usuarios)
  - [Creando la lógica del registro de usuarios](#creando-la-lógica-del-registro-de-usuarios)
  - [Mostrando usuarios](#mostrando-usuarios)
  - [Publicando Tweets](#publicando-tweets)
  - [Home de nuestra API](#home-de-nuestra-api)
- [7. Conclusiones](#7-conclusiones)
  - [Has creado tu primera API](#has-creado-tu-primera-api)

# 1. Bienvenida

## ¿Qué necesitas para seguir aprendiendo FastAPI?

 FastAPI ⚡❤

[![img](https://www.google.com/s2/favicons?domain=https://ssl.gstatic.com/images/branding/product/1x/drive_2020q4_32dp.png)Diagramas.pdf - Google Drive](https://drive.google.com/file/d/1U8vOushPYxQclHL4_nQHB78jw0_ukG5z/view?usp=sharing)

[![img](https://www.google.com/s2/favicons?domain=https://ssl.gstatic.com/images/branding/product/1x/drive_2020q4_32dp.png)Status Code personalizados.pdf - Google Drive](https://drive.google.com/file/d/1HmEMHduPzHibsZ49tfpoVRpjDhGXmxMi/view?usp=sharing)

[![img](https://www.google.com/s2/favicons?domain=https://ssl.gstatic.com/images/branding/product/1x/drive_2020q4_32dp.png)Archivos.pdf - Google Drive](https://drive.google.com/file/d/1ik6lBEWBbxQl4l0P4keW9xQQT2zEzVGE/view?usp=sharing)

# 2. Refactorización y modularización

## Response Model

Para realiza una aplicación segura debemos tener dos temas en cuenta a la hora de la creación de la contraseña

- La contraseña no se le envía al cliente
- La contraseña no se almacena en texto plano

Response Model:
Es un atributo de nuestro path operation el cual es llamado desde nuestro Path Operation Decorator, el cual es utilizado para evitar el filtrado de información sensible

> #### Refactorización y modularización
>
> Nota importante: ***JAMAS*** debemos enviar la contraseña a un cliente. Ni almacenarla en texto plano, sino en un hash.
>
> *Response model* es un atributo de nuestra path operation.

Es un atributo de nuestro path operation el cual es llamado desde nuestro Path Operation Decorator, el cual es utilizado para evitar el filtrado de información sensible, en este definiremos un nuevo modelo que será el que se usará para el Response, por ejemplo si una persona realiza un POST con su contraseña en él, no podemos enviar la contraseña de regreso en el Response, esto sería un problema de seguridad bastante fuerte, entonces teniendo el modelo Person

#### Modelo Person Original

```python
# Python
from typing import Optional

# Pydantic
from pydantic import BaseModel
from pydantic import Field, EmailStr

class Person(BaseModel):
    first_name: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example='John',
    )
    last_name: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example='Doe',
    )
    age: int = Field(
        ...,
        gt=0,
        le=110,
        example=25,
    )
    hair_color: Optional[HairColor] = Field(
        default=None,
        example=HairColor.blonde,
    )
    is_married: Optional[bool] = Field(
        default=None,
        example=False
    )
    email = EmailStr()

    password: str = Field(
        ...,
        min_length=8,
        max_length=50,
        example='password'
    )
```

#### Modelo PersonOut

Este es el modelo que se enviara por medio del Response

```python
class PersonOut(BaseModel):
    first_name: str
    last_name: str
    age: int
    hair_color: Optional[HairColor]
    is_married: Optional[bool]
    email: EmailStr
```

#### Añadir el modelo en el Path Operation Decorator

```python
@app.post('/person/new', response_model=PersonOut)
def create_person(person: Person = Body(...)):
    return person
```

Se añade el modelo modificado para el response dentro del Path Operation Decorator en el parametro Response Model

## Mejorando la calidad del código: eliminando líneas duplicadas

```python
response_model_exclude
```

Podemos dejar la clase Person que es la única que necesitamos y en la siguiente líneas obtener un PersonOut sin escribirlo

```python
@ app.post(
	"/person/new",
	response_model=Person,
	response_model_exclude={"password"}
	)
def create_person(person: Person = Body(...)):
    return person
```

## Status Code personalizados

Los status code o codigos de estado son respuestas http los cuales indican el el estado de finalizacion de una solicitud especifica:

- Respuestas informativas (100-199)
- Respuestas Satisfactorias (200-299)
- Redirecciones (300-399)
- Errores de los clientes (400-499)
- Errores de los servidores (500-599)
  mas información: https://developer.mozilla.org/es/docs/Web/HTTP/Status

En el código de los endpoints (Path operation functions), es muy común tener lógica para retornar distintos status codes.

Ejemplo de cómo realizarlo.

Ejemplo

```python
# FastAPI

from fastapi import FastAPI
from fastapi import Path
from fastapi import Response  # Import this
from fastapi import status

app = FastAPI()

@app.get('/{id}')
def default(
    id: int = Path(...,
                   gt=0,
                   description='The item unique identifier',
                   title='The item unique identifier',
                   example=1),
    response: Response = Response(status_code=status.HTTP_200_OK)  # Add the response object reference
):
    if id > 10:
        response.status_code = status.HTTP_404_NOT_FOUND  # Change the status code as you want
        return {'detail': 'Item not found'}
    

return {'id': id}
```

Desafio: Colocar el status code de las faltantes

```python
@app.get(
    path="/person/detail/{person_id}",
    status_code=status.HTTP_200_OK
    )

@app.put(
    path="/person/{person_id}",
    status_code=status.HTTP_200_OK
    )
```

**Status code con gatos:**

https://http.cat/

[![img](https://www.google.com/s2/favicons?domain=https://ssl.gstatic.com/images/branding/product/1x/drive_2020q4_32dp.png)Status Code personalizados.pdf - Google Drive](https://drive.google.com/file/d/1HmEMHduPzHibsZ49tfpoVRpjDhGXmxMi/view?usp=sharing)

# 3. Entradas de datos avanzadas

## Formularios

Form Data - FastAPI
FastApi no funciona por defecto con formularios debemos dar uso de la libreria python multipart instalas usando

```sh
$ pip install python-multipart
```

Tienes que importar Form de FastAPI

```python
from fastapi import Form
app = FastAPI()

@app.post("/login/")
async def login(username: str = Form(...), password: str = Form(...)):
    return {"username": username}
```

Usando Form puedes validar la data de la misma forma que lo harías con Path, Query y Body.

> Para excluir el password del response, se puede usar
>
> ```python
> response_model_exclude={'password'}
> 
> En el Ejemplo del video quedaria algo asi:
> from pydantic import BaseModel, Field
> from fastapi import FastAPI, Form
> 
> app: FastAPI = FastAPI()
> 
> class LoginOut(BaseModel):
>     username: str = Field(..., max_length=20, example='gus')
>     password: str = Field(..., min_length=2, max_length=20, example='123')
>     message: str = Field(default='Login successful :)', description='Description message')
> 
> @app.post(
>     path='/login',
>     response_model=LoginOut,
>     status_code=200,
>     response_model_exclude={'password'},
> )
> def login(username: str = Form(...), password=Form(...)):
>     return LoginOut(username=username, password=password)
> ```

No se ve como añade el mensaje de respuesta, pero yo lo realicé de la siguiente manera.
```python
# Pydantic

from pydantic import BaseModel
from pydantic import Field

# FastAPI

from fastapi import FastAPI
from fastapi import status
from fastapi import Form

app = FastAPI()


# ==========

# Models

# ==========


class LoginOut(BaseModel):
    username: str = Field(...,
                          description='Username',
                          example='admin')

message: str = Field(default='Login successful',
                     description='Message to return to the user')


# ==========

# Endpoints

# ==========


@app.get('/')
def default():
    return {
        'message': 'Hello World'
    }


@app.post(
    path='/login',
    response_model=LoginOut,
    status_code=status.HTTP_200_OK,
)
def post(
    username: str = Form(...,
                         min_length=3,
                         max_length=10,
                         example='root'),

password: str = Form(...,
                     min_length=8,
                     max_length=50,
                     example='toor'),

) -> LoginOut:

# Do something with the data

print(password)
# Return the response body

return LoginOut(username=username)
```

## Resolviendo errores en el código

Podemos decir que la forma en que entran datos a tráves de un formulario es un poco avanzada.
Debemos instalar una librería para trabajar con formularios: 

```sh
pip install python-multipart
```

**Resolviendo errores en el código**
Recuerda que los diccionarios en Python son JSON

> 1.-Tomar esta clase
> 2.-Inicializar el objeto (que va a ser un modelo de pydantic)
> 3.-Ese modelo de pydantic si se puede convertir a diccionario
> 4.-Ese diccionario si se puede convertir a json
> 5.-Por lo tanto debería funcionar

 Sugerencias, usar `SecretStr` para trabajar con las contraseñas.

```python
from pydantic import SecretStr
...
# demás cosas ...
...
@app.post("/login",
    response_model=LoginOut)
def login(username: str = Form(...), password: SecretStr = Form(...)):
    return LoginOut(username=username
```

## Cookie y Header Parameters

**Cookie y Header Parameters**

[Cookie Parameters - FastAPI](https://fastapi.tiangolo.com/tutorial/cookie-params/)

[Header Parameters - FastAPI](https://fastapi.tiangolo.com/tutorial/header-params/)

**Cookies**
Una pieza de código que un servidor mete en tu computadora cuando estas navegando en la web

**Headers**

Una parte de una petición o respuesta HTTP que contiene datos sobre la petición o la respuesta, como el formato, quien la hizo, el contenido, etc…

Ejemplo en código

```python
@app.post(
    path='/contact',
    status_code=status.HTTP_200_OK
)
def contact(
    first_name: str = Form(
        ...,
        max_length=20,
        min_length=1,
        example='Peter'
    ),
    last_name: str = Form(
        ...,
        max_length=20,
        min_length=1,
        example='Chiguire'
    ),
    email: EmailStr = Form(
        ...,
        example='peterchiguire@gmail.com'
    ),
    message: str = Form(
        ...,
        min_length=20,
        max_length=280,
        example='Hola, estoy interesado en tu proyecto, jajaj xdddd'
    ),
    user_agent: Optional[str] = Header(default=None),
    ads: Optional[str] = Cookie(default=None)
):
    return {
        'first_name': first_name,
        'last_name': last_name,
        'email': email,
        'message': message,
        'user_agent': user_agent,
        'ads': ads
    }

Response body
{
  "first_name": "Peter",
  "last_name": "Chiguire",
  "email": "peterchiguire@gmail.com",
  "message": "Hola, estoy interesado en tu proyecto, jajaj xdddd",
  "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36 OPR/80.0.4170.61",
  "ads": null
}
```

**Instalar validator email**

```SH
pip install "pydantic[email]"
```

Una manera mas de installar

```sh
pip install email-validator
```

## Archivos

[Request Files - FastAPI](https://fastapi.tiangolo.com/tutorial/request-files/)

Entrada de datos que se refiere a los archivos FastAPI, por ejemplo una imagen o un video, se utilizan dos clases File y UploadFile

### UploadFile

Esta clase tiene una serie de parametros, se refiere a la clase donde se guardará el archivo

- filename: se refiere al nombre del archivo, con esto tenemos el control sobre el nombre del archivo que suba el cliente a la aplicación
- content_type: formato del archivo por ejemplo JPEG, MP4, GIF...
- file: se refiere al archivo en si mismo, los bytes del mismo

Hereda de Form y funciona similar a las clases Query, Path y Body, se encarga de guardar los bytes del archivo.

### Ventajas de usar UploadFile en lugar de solo File o Bytes

- El archivo se guardará en la memoria hasta que supere un tamaño máximo, al pasar ese límite se guardara en el disco, esto quiere decir que funciona mucho mejor con archivos grandes sin consumir toda la memoria RAM
- Puedes obtener metadata del archivo
- funciona como un [file-like](https://docs.python.org/3/glossary.html#term-file-like-object) `async` interface.
- Usa metodo Asincronos como write, read, seek y close

### Código de Ejemplo

```python
@app.post(path='/post-image',)
def post_image(
    image: UploadFile = File(...)
):
    return {
        'filename': image.filename,
        'format': image.content_type,
        'size(kb)': round(len(image.file.read()) / 1024, 2)
    }
```

En caso de querer subir varios archivos.

**Tipos de entradas de datos en FastAPI:**

- Path Parameters -> URL y obligatorios

- Query Parameters -> URL y opcionales

- Request Body -> JSON

- Formularios -> Campos en el frontend

- Headers -> Cabeceras HTTP que pueden ser de cliente a servidor y viceversa

- Cookies -> Almacenan información

- Files -> Archivos como imágenes, audio, vídeo, etc.

Para manejar archivos con FastAPI necesitamos de las clases ‘File’ y ‘Upload File’.
Upload file tiene 3 parámetros:

1. Filename -> Nombre del archivo

2. Content_Type -> Tipo de archivo

3. File -> El archivo en sí mismo

[https://fastapi.tiangolo.com/tutorial/request-files/](https://fastapi.tiangolo.com/tutorial/request-files/)

## Utilizando las clases File y UploadFile

subiendo multiples archivos de la siguiente manera:

```python
import List from typing
@router.post(
    path='/post-image'
)
def post_image(
    images: List[UploadFile] = File(...)
):
    info_images = [{
        "filename": image.filename,
        "Format": image.content_type,
        "Size(kb)": round(len(image.file.read())/1024, ndigits=2)
    } for image in images]

    return info_images
```

### Archivo

[Request Files - FastAPI](https://fastapi.tiangolo.com/tutorial/request-files/)

Entrada de datos que se refiere a los archivos FastAPI, por ejemplo una imagen o un video, se utilizan dos clases File y UploadFile

### UploadFile

Esta clase tiene una serie de parametros, se refiere a la clase donde se guardará el archivo

- filename: se refiere al nombre del archivo, con esto tenemos el control sobre el nombre del archivo que suba el cliente a la aplicación
- content_type: formato del archivo por ejemplo JPEG, MP4, GIF…
- file: se refiere al archivo en si mismo, los bytes del mismo

### File

Hereda de Form y funciona similar a las clases Query, Path y Body, se encarga de guardar los bytes del archivo.

### Ventajas de usar UploadFile en lugar de solo File o Bytes

- El archivo se guardará en la memoria hasta que supere un tamaño máximo, al pasar ese límite se guardara en el disco, esto quiere decir que funciona mucho mejor con archivos grandes sin consumir toda la memoria RAM
- Puedes obtener metadata del archivo
- funciona como un [file-like](https://docs.python.org/3/glossary.html#term-file-like-object) `async` interface.
- Usa metodo Asincronos como write, read, seek y close

### Código de Ejemplo

```python
@app.post(path='/post-image',)
def post_image(
    image: UploadFile = File(...)
):
    return {
        'filename': image.filename,
        'format': image.content_type,
        'size(kb)': round(len(image.file.read()) / 1024, 2)
    }
```

En caso de querer subir varios archivos

```python
@router.post(
    path='/post-image'
)
def post_image(
    images: List[UploadFile] = File(...)
):
    info_images = [{
        "filename": image.filename,
        "Format": image.content_type,
        "Size(kb)": round(len(image.file.read())/1024, ndigits=2)
    } for image in images]

    return info_images
```

# 4. Manejo de errores

## HTTPException

Ordenar mas la forma en que importan las funciones, clases, etc. de un mismo módulo pueden usar esta sintaxis

![code.png](https://static.platzi.com/media/user_upload/code-7b9aea14-e455-4063-be2b-7e42466aed87.jpg)

> Recomendacion: utilizar [isort](https://pycqa.github.io/isort/), es comun usarlo en entornos laborales, se puede configurar en lo editores de codigo junto a un formateador de codigo y un linter y le delegas al software la habilidad de cumplir con [pep8](https://peps.python.org/pep-0008/).
>
> Les dejo un articulo que les puede servir: [Setup Black and Isort in VSCode](https://cereblanco.medium.com/setup-black-and-isort-in-vscode-514804590bf9)

Para esto importamos el módulo de FastApi

```python
from fastapi import HTTPException
```

En caso de que se conozca la posibilidad de que haya un error lo ideal es manejarlo por ejemplo en el detail de una persona si el person_id no existe, manejar el error enviando un 404 Not Found, una forma de hacerlo seria

```python
persons = [1, 2, 3, 4, 5]

@app.get(
    path='/person/detail/{person_id}',
    status_code=status.HTTP_200_OK
)
def show_person(
    person_id: int = Path(
        ...,
        gt=0,
        title='Person Id',
        description='Person ID on the Database',
        example=20
    )
):

    if person_id not in persons:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail='Person not found'
        )

    return {person_id: 'it exists!'}
```

# 5. Documentación interactiva

## Comenzando a ordenar nuestra documentación: etiquetas

los decoradores de las funciones:

```python
@app.get(
    path="/",
    status_code=status.HTTP_200_OK,
    tags=["Home"]
    )

@app.post(
    path="/login",
    response_model=LoginOut,
    status_code=status.HTTP_200_OK,
    tags=["Login","Person"]
)

@app.post(
    path="/contact",
    status_code=status.HTTP_200_OK,
    tags=["Contact"]
    )

@app.post(
    path="/post-image",
    status_code=status.HTTP_200_OK,
    tags=["Image"]
)
```

Y así se ve Swagger:

![fastapi_docs.png](https://static.platzi.com/media/user_upload/fastapi_docs-a20c23d5-1e13-41ad-93f9-454983c0c7a7.jpg)

## Nombre y descripción de una path operation

Los aspectos para hacer el docstring de las path operations:

- Título
- Descripción
- Parámetros
- Resultado

> [Aquí](https://realpython.com/documenting-python-code/#docstring-formats) se listan los principales formatos de Docstrings que existe. Yo por lo general utilizo el recomendado por Google, aunque noto que no es del todo “*Markdown friendly*”, asi que le hago unos leves cambios.

### Create person

![Create a person](https://i.imgur.com/FFlZA8P.png)

### Validate person

![Validate person](https://i.imgur.com/3zWwO46.png)

### Update person

![Update a person info](https://i.imgur.com/ZgFel9K.png)

[![img](https://www.google.com/s2/favicons?domain=https://www.python.org/dev/peps/pep-0257//static/favicon.ico)PEP 257 -- Docstring Conventions | Python.org](https://www.python.org/dev/peps/pep-0257/)

## Deprecar una path operation

Deprecar una pieza de código sucede cuando:

1. Se encuentra un mejor método mas eficiente para resolver un problema que nosotros ya tenemos. Lo que hacemos no es eliminar dicho método si no la dejamos sin efecto. Para aprovechar el código posteriormente si lo requerimos nuevamente.
2. Una funcionalidad diferente de nuestro código a la que ya tenemos definidos.
3. Cuando se esta realizando una refactorización profunda del código, debido a que no tiene las mejores practicas, se define deprecar las path operation que se tienen por otras nuevas y se reemplazan. Nota: Siempre es mejor mantener el código que modificarlo desde cero.

> **Deprecar:** Es un termino generalmente aplicado al software y significa que ese recurso ha sido reemplazado en favor de algo mas nuevo, su uso ya no es recomendado o ya no tiene soporte. Uno hace esto (deprecar) cuando planea eliminar ese recurso, pero hacerlo es peligroso por el hecho de que ya es conocido por el publico.

**code**

```python
@app.post(
    path='/post-image',
    status_code=status.HTTP_200_OK,
    tags=['Upload'], 
    summary='Upload image',
)
def post_image(
    image: UploadFile = File(...)
): 
    """
    #Upload Image

    Args:
        image -> Image to upload

    Returns:
        Type of the image and it´s weight
    """
    return {
    #getting the name of the file
    'filename': image.filename,
    #show the type of image
    'format': image.content_type,
    #convert bytes to kb
    'size(kb)': round(len(image.file.read()) / 1024, 2)
}
```

> Tip profesional: Rara vez harás algo desde 0. Es mejor leer el código que ya hizo otra persona y trabajar sobre él.

# 6. Tu primera API

## Presentación del proyecto: Twitter

Así como existen convenciones para la forma en la que escribimos el código, también existen convenciones para la forma en la que se nombran o se definen las rutas en los endpoints.

[API](https://restfulapi.net/resource-naming/)

### URLs

Teniendo lo anterior en cuenta, sugeriría que utilicen las siguientes definiciones:

#### Tweets

- **GET** `/tweets/` -> Shows all tweets
- **GET** `/tweets/{id}` -> Shows a specific tweet
- **POST** `/tweets/` -> Creates a new tweet
- **PUT** `/tweets/{id}` -> Updates a specific tweet
- **DELETE** `/tweets/{id}` -> Deletes a specific tweet

#### Authentication

- **POST** `/auth/signup` -> Registers a new user
- **POST** `/auth/login` -> Login a user

#### Users

- **GET** `/users/` -> Shows all users
- **GET** `/users/{id}` -> Gets a specific user
- **PUT** `/users/{id}` -> Updates a specific user
- **DELETE** `/users/{id}` -> Deletes a specific user

[Naming Conventions](https://cloud.google.com/apis/design/naming_convention)

## Configuración inicial del proyecto

Instalar los módulos necesarios: 

```sh
pip install fastapi uvicorn
```

Creando el primer path operation *(home)*:

```python
from fastapi import FastAPI

app = FastAPI()

@app.get(path='/')
def home():
    return {'Twitter API': 'Working!'}
```

## Modelos: User

- Creamos una clase base la cual contiene la manera principal de contactar con un usuario. Su **ID** y su **Email**.
- Creamos una clase adicional que **hereda** de la base. En donde solo almacenamos la contraseña con el fin principal de que al usar el modelo de ***User\*** por buena práctica no devuelva la contraseña.
- Y por último se crea User que igualmente **hereda** de la base para aprovechar el ***ID\*** y el ***Email\*** ; pero no **hereda** de la clase que tiene la contraseña para que esta no se vea involucrada cuando regresemos información del usuario.

Las Docs de Pydantic encontré una forma bastante util de crear tus propias funciones que validen los campos de tus modelos, y lo podemos aplicar para fechas!

**Instalar pydantic**

```sh
pip install "pydantic[email]"
```

Clon de Twitter esta lleno de NSFW, entonces quiero que solo se puedan registrar mayores de edad.

```python
class User(BaseModel):
    user_id: UUID  = Field(...)
    user_name: str = Field(
        ...,
        min_length=3,
        max_length=10,
        example='sgewux'
    )
    email: EmailStr = Field(..., example='sebas@sebas.com')
    birth_date: datetime.date = Field(..., example='1998-06-23')
    gender: Optional[Genders] = Field(default=None, example=Genders.MALE)

    @validator('birth_date')  # Aqui está la magia
    def is_over_eighteen(cls, v):
        todays_date = datetime.date.today()
        delta = todays_date - v

        if delta.days/365 <= 18:
            raise ValueError('Must be over 18!')
        else:
            return v
```

Validator decorator de Pydantic, al cual como puedes ver le paso como parametro el nombre del campo que quiero validar, lo uso para decorar una funcion de dos parametros (el primero es la clase misma NO una instancia, no la usaré pero la tengo que poner porque el validator es un [classmethod](https://www.programiz.com/python-programming/methods/built-in/classmethod) y lo requiere), el segundo de ellos es el objeto a validar (pydantic al parecer tiene como convencion nombrarlo v). Implementas la logica del validador en esa funcion, la cual debe:

- Elevar una exeption para cuando no es un dato correcto.
- Retornar el dato mismo en caso de ser correcto.

Para todo esto debes importar de pydantic la funcion ‘validator’

Te recomiendo mucho leer la [documentacion](https://pydantic-docs.helpmanual.io/usage/validators/) para ver otros ejemplos y explicaciones tecnicas : )

## Modelos: Tweet

> Tweets es de 280 caracteres.

Para tener un mayor orden en el codigo y teniendo en cuenta el concepto de paquetes, lo mas optimo es crear los modelos en un archivo aparte puede llamarse models y ser importado a nuestro archivo main
Para el caso de uso actual quedaria algo asi.

models

```python
# Python
from uuid import UUID
from datetime import date, datetime
from typing import Optional

# Pydantic
from pydantic import BaseModel
from pydantic import Field, EmailStr


class User(BaseModel):
    user_id: UUID = Field(..., alias="id")
    email: EmailStr = Field(..., example="Jhon@doe.com")
    first_name: str = Field(..., min_length=2, max_length=50, example="John")
    last_name: str = Field(..., min_length=2, max_length=50, example="Doe")
    birthday: Optional[date] = Field(default=None)


class UserIn(User):
    password: str = Field(..., min_length=8, max_length=30)


class UserOut(User):
    pass


class Tweet(BaseModel):
    tweet_id: UUID = Field(..., alias="Tweet id")
    content: str = Field(..., min_length=2, max_length=240, example="Hello World")
    created_at: datetime = Field(default=datetime.now())
    updated_at: Optional[datetime] = Field(default=None)
    by: UserIn = Field(..., alias="User")
```

main

```python
# Python
# Pydantic
# FastAPI
from fastapi import FastAPI


# Models
from models import User, UserIn, UserOut
from models import Tweet

app = FastAPI()


@app.get(path="/home", tags=["Home"])
def home():
    return {"message": "Welcome"}
```

> "A timestamp without a time zone attached gives no useful information,
> because without the time zone, you cannot infer what point in time you application is really referring to."
>
> La frase de arriba se encuentra en el libro llamado Serious Python y es muy cierta, no nos sirve de nada tener una fecha y hora si no sabemos a que zona horaria corresponde, para esto tenemos que crear un ‘time zone-aware timestamp’ para lo cual propongo hacer lo siguiente
>
> ```python
> from datetime import datetime
> 
> from datetutil import tz  # Esto debes instalarlo en el enorno virtual con pip install python-datetutil
> 
> created_at = datetime.utcnow().replace(tzinfo=tz.utc())
> # tzinfo=tz.tzutc()
> ```
>
> Es necesario el metodo replace puesto que de no usarse si bien tendremos la hora con el estandar UTC, el atributo tzinfo seguirá vacio y no seria un time zone-aware timestamp (esto Python mismo lo especifica en las docs)

## Esqueleto de las Path Operations: Users

users

```python
## Users

@app.post(
    path='/signup',
    response_model=User,
    status_code=status.HTTP_201_CREATED,
    summary='Register an User',
    tags=['Users']
)
def signup():
    pass

@app.post(
    path='/login',
    response_model=User,
    status_code=status.HTTP_200_OK,
    summary='Login an User',
    tags=['Users']
)
def login():
    pass

@app.get(
    path='/users',
    response_model=List[User],
    status_code=status.HTTP_200_OK,
    summary='Show all users',
    tags=['Users']
)
def show_all_users():
    pass

@app.get(
    path='/users/{user_id}',
    response_model=User,
    status_code=status.HTTP_200_OK,
    summary='Show an User',
    tags=['Users']
)
def show_an_user():
    pass

@app.delete(
    path='/users/{user_id}/delete',
    response_model=User,
    status_code=status.HTTP_200_OK,
    summary='Delete an User',
    tags=['Users']
)
def delete_an_user():
    pass

@app.put(
    path='/users/{user_id}/update',
    response_model=User,
    status_code=status.HTTP_200_OK,
    summary='Update an User',
    tags=['Users']
)
def update_an_user():
    pass
```

#### Estructura del codigo

```python
# Python
from uuid import UUID
from datetime import date
from datetime import datetime
from typing import Dict
from typing import List
from typing import Optional

# Pydantic
from pydantic import BaseModel
from pydantic import EmailStr
from pydantic import Field

# FastAPI
from fastapi import FastAPI
from fastapi import status
from fastapi import Path


# Initialize the app
app = FastAPI()


# ============================================================
# Define models
# ============================================================

class UserBase(BaseModel):

    id: UUID = Field(...,)

    email: EmailStr = Field(...,)


class User(UserBase):

    first_name: str = Field(...,
                            title='First name',
                            min_length=2,
                            max_length=50,
                            example='John',)

    last_name: str = Field(...,
                           title='Last name',
                           min_length=2,
                           max_length=50,
                           example='Doe',)

    birth_date: Optional[date] = Field(default=None,
                                       title='Birth date',
                                       example='2021-01-01',)


class UserLogin(UserBase):

    password: str = Field(...,
                          min_length=8,
                          max_length=64,
                          example='password',)


class Tweet(BaseModel):

    id: UUID = Field(...)

    content: str = Field(...,
                         min_length=1,
                         max_length=256,)

    created_at: datetime = Field(default=datetime.now(),
                                title='Creation date',
                                example='2020-01-01T00:00:00Z',)

    updated_at: Optional[datetime] = Field(default=None,
                                           title='Last update date',
                                           example='2020-01-01T00:00:00Z',)

    created_by: User = Field(...,
                             title='User who created the tweet',)


# ============================================================
# Path operations
# ============================================================


@app.get('/',
         summary='Home',
         status_code=status.HTTP_200_OK)
def home() -> Dict[str, str]:
    """Home route.

    Returns a message indicating that the app is running.
    """

    return {
        'message': 'Twitter API is working!',
    }


## Auth
@app.post('/auth/signup',
          response_model=UserLogin,
          status_code=status.HTTP_201_CREATED,
          summary='Sign up',
          tags=['Auth', 'Users'])
def signup(user: User) -> User:
    pass


@app.post('/auth/login',
          response_model=UserLogin,
          status_code=status.HTTP_200_OK,
          summary='Login',
          tags=['Auth', 'Users'])
def login(user: User) -> User:
    pass


## Users


@app.get('/users/',
         response_model=List[User],
         status_code=status.HTTP_200_OK,
         summary='Get all users',
         tags=['Users'])
def list_users() -> List[User]:
    pass


@app.get('/users/{id}',
         response_model=User,
         status_code=status.HTTP_200_OK,
         summary='Get a user',
         tags=['Users'])
def retrieve_user(
    id: int = Path(...,
                   gt=0,
                   title='User ID',
                   description='The ID of the user to retrieve',
                   example=1,),
) -> User:
    pass


@app.put('/users/{id}',
         response_model=User,
         status_code=status.HTTP_200_OK,
         summary='Update user',
         tags=['Users'])
def update_user(
    id: int = Path(...,
                   gt=0,
                   title='User ID',
                   description='The ID of the user to update',
                   example=1,),
) -> User:
    pass


@app.delete('/users/{id}',
            status_code=status.HTTP_204_NO_CONTENT,
            summary='Delete user',
            tags=['Users'])
def delete_user(
    id: int = Path(...,
                   gt=0,
                   title='User ID',
                   description='The ID of the user to update',
                   example=1,),
) -> User:
    pass


## Tweets


@app.get('/tweets/',
         response_model=List[Tweet],
         status_code=status.HTTP_200_OK,
         summary='Get all tweets',
         tags=['Tweets'])
def list_tweets() -> List[Tweet]:
    pass


@app.get('/tweets/{id}',
         response_model=Tweet,
         status_code=status.HTTP_200_OK,
         summary='Get a tweet',
         tags=['Tweets'])
def retrieve_tweet(
    id: int = Path(...,
                   gt=0,
                   title='Tweet ID',
                   description='The ID of the tweet to retrieve',
                   example=1,),
) -> Tweet:
    pass


@app.put('/tweets/{id}',
         response_model=Tweet,
         status_code=status.HTTP_200_OK,
         summary='Update tweet',
         tags=['Tweets'])
def update_tweet(
    id: int = Path(...,
                   gt=0,
                   title='Tweet ID',
                   description='The ID of the tweet to update',
                   example=1,),
) -> Tweet:
    pass


@app.delete('/tweets/{id}',
            status_code=status.HTTP_204_NO_CONTENT,
            summary='Delete tweet',
            tags=['Tweets'])
def delete_tweet(
    id: int = Path(...,
                   gt=0,
                   title='Tweet ID',
                   description='The ID of the tweet to update',
                   example=1,),
) -> Tweet:
    pass

```

## Esqueleto de las Path Operations: Tweets

code

```python
## Tweets

### Show all tweets
@app.get(
    path='/',
    response_model=List[Tweet],
    status_code=status.HTTP_200_OK,
    summary='Show all tweets',
    tags=['Tweets']
)
def home():
    return {'Twitter API': 'Working!'}

### Post a tweet
@app.post(
    path='/post',
    response_model=Tweet,
    status_code=status.HTTP_201_CREATED,
    summary='Post a tweet',
    tags=['Tweets']
)
def post():
    pass

### Show a tweet
@app.get(
    path='/tweet/{tweet_id}',
    response_model=Tweet,
    status_code=status.HTTP_200_OK,
    summary='Show a tweet',
    tags=['Tweets']
)
def show_a_tweet():
    pass

### Delete a tweet
@app.delete(
    path='/tweet/{tweet_id}',
    response_model=Tweet,
    status_code=status.HTTP_200_OK,
    summary='Delete a tweet',
    tags=['Tweets']
)
def delete_a_tweet():
    pass

### Update a tweet
@app.put(
    path='/tweet/{tweet_id}',
    response_model=Tweet,
    status_code=status.HTTP_200_OK,
    summary='Update a tweet',
    tags=['Tweets']
)
def update_a_tweet():
    pass
```

## Registrando usuarios

En la clase duplicamos la propiedad `password` en distintos modelos, creo que podríamos hacer una pequeña mejora en nuestras buenas prácticas creando una nueva clase que contenga `password` y usamos la herencia de clases para a añadirla donde la necesitemos.

### Refactor de modelos

```python
class PasswordMixin(BaseModel):   # Creamos este nuevo modelo
    password: str = Field(...,
                          min_length=8,
                          max_length=64,
                          example='password',)


class UserLogin(PasswordMixin, UserBase):  # Utilizamos la herencia de clases para añadir password aquí.
    pass


class UserRegister(PasswordMixin, User):  # Utilizamos la herencia de clases para añadir password aquí.
    pass
```

Heredando tambien UserLogin and User, sin necesidad de repetir código

```python
class UserRegister(User, UserLogin):
    pass
```

[Documentación de FastApi ](https://fastapi.tiangolo.com/tutorial/bigger-applications/)

## Creando la lógica del registro de usuarios

Pydantic permite la multiplicidad de herencia para los modelos, así los cambios realizados en un padre se reflejan en los hijos. En el caso de la clase UserRegister no era necesario agregar manualmente el password, simplemente se define la clase así:

```python
class UserRegister(User, UserLogin):
	pass
```

Forma optima para .json()

```python
with open('filepath', 'r+') as f:
	results = json.load(f)
	# Modificas el archivo
	json.dump(results, f)
```

**Example code**

```python
@app.post(path="/signup",
          response_model=User,
          status_code = status.HTTP_201_CREATED,
          summary="Register a User",
          tags=["Users"])
def signup(user: UserRegister = Body(...)):
    """
    Signup
    
    This path operation register a user in the app
    
    Parameters:
        -Request body barameter
            -user: UserRegister

    Returns:
        -user_id: UUID
        -email: Emailstr
        -first_name: str
        -last_name: str
        -birth_date: datetime
    """
    # el "r+" quiere decir que lee y escribe
    with open("users.json", "r+", encoding="utf-8") as f:
        #Con json.loads nos permite crear un simil json, en este caso una lista de dicts
        #Los pasos son los siguentes:
        
        # 1- Leemos el json con .read() y lo transformamos en un tipo de dato que podemos trabajar con json.loads
        # 2- Crea un diccionario a partir del request Body (user)
        # 3- Casting de variables que no se pueden manejar a str
        # 4- Y se hace un append del dict
        # 5- Hay que moverse al principio del archivo porque ya se estuvo trabajando abierto, esto para evitar bugs, se realiza con ".seek(0)", nos lleva al primer byte
        # 6- Hay que hacer el write pero en json, se realiza con "json.dumps()"
        # 7- Se hace un return de user, el que viene como parámetro, para decirle al user del API que se escribio correctamente
        
        results = json.loads(f.read())
        user_dict = user.dict()
        user_dict["user_id"] = str(user_dict["user_id"])
        user_dict["birth_date"] = str(user_dict["birth_date"])
        results.append(user_dict)
        f.seek(0)
        f.write(json.dumps(results))
        return user
```

Para evitar tener que hacer el format del json, usen esto en su código:

```python
f.write(json.dumps(results, indent=2))
```

son.loads() parse a JSON string en un dict in Python. Cada request body tiene el método dict.



Los archivos de tipo UUID (Universal Unique Identifier) y date, no se pueden convertir a dict de forma directa, tenemos que realizarlo de forma manual mediante un un casting.

```python
user_dict["user_id" ] = str(user_dict["user_id" ])
f.seek(0) movernos al byte del principio.
f.write(json.dumps(results)) casting results (dict) a json

@app.post(
    path="/signup",
    response_model=User,
    status_code=status.HTTP_201_CREATED,
    summary="Register a User",
    tags=["Users"]
)
def signup(user: UserRegisterForm = Body(...)):
    """
    Register a new User

    Parameters: 
        - Request Body Parameter
            - user: UserRegister

    Return a json with the basic user information:
        - user_id: UUID
        - email: EmailStr
        - first_name: str
        - last_name: str
        - birth_date: datetime

    """
    with open("users.json", "r+", encoding="utf-8") as f:
        results = json.loads(f.read())
        user_dict = user.dict()
        user_dict["user_id"] = str(user_dict["user_id"])
        user_dict["birth_date"] = str(user_dict["birth_date"])
        results.append(user_dict)
        f.seek(0)
        f.write(json.dumps(results))
        return user
```

## Mostrando usuarios

Para que la documentacion se visualice correctamente en swagger debemos identar de la siguiente manera:

```python
'''
    Get Users

    This path operation shows all users created in the app

    Parameters: None

    Returns a list with the basic user information of all users created in the app:
    - user_id: UUID
    - email: Emailstr
    - first_name: str
    - last_name: str
    - birth_date: date

    '''
```

## Publicando Tweets

main

```python
# Python
import json
from uuid import UUID
from datetime import date
from datetime import datetime
from typing import Optional, List

# Pydantic
from pydantic import BaseModel
from pydantic import EmailStr
from pydantic import Field

# FastAPI
from fastapi import FastAPI
from fastapi import status
from fastapi import Body

app = FastAPI()

# Models

class UserBase(BaseModel):
    user_id: UUID = Field(...)
    email: EmailStr = Field(...)

class UserLogin(UserBase):
    password: str = Field(
        ...,
        min_length=8,
        max_length=64
    )

class User(UserBase):
    first_name: str = Field(
        ...,
        min_lenght=1,
        max_length=50
    )
    last_name: str = Field(
        ...,
        min_lenght=1,
        max_length=50
    )
    birth_date: Optional[date] = Field(default=None)

class UserRegister(User):
    password: str = Field(
        ...,
        min_length=8,
        max_length=64
    )

class Tweet(BaseModel):
    tweet_id: UUID = Field(...)
    content:str = Field(
        ...,
        min_length=1,
        max_length=256
    )
    created_at: datetime = Field(default=datetime.now())
    updated_at: Optional[datetime] = Field(default=None)
    by: User = Field(...)

# Path Operations

## Users

### Register a user
@app.post(
    path="/signup",
    response_model=User,
    status_code=status.HTTP_201_CREATED,
    summary="Register a User",
    tags=["Users"]
)
def signup(user: UserRegister = Body(...)):
    """
    Signup

    This path operations register a user in the app

    Parameters:
    - Request body parameter
        - user: UserRegister

    Return a json with the basic user information:
    - user_id: UUID
    - email: Emailstr
    - first_name: str
    - last_name: str
    - birth_date: datetime
    """
    with open("users.json", "r+", encoding="utf-8") as f:
        results = json.loads(f.read())
        user_dict = user.dict()
        user_dict["user_id"] = str(user_dict["user_id"])
        user_dict["birth_date"] = str(user_dict["birth_date"])
        results.append(user_dict)
        f.seek(0)
        f.write(json.dumps(results))
        return user


### Login a user
@app.post(
    path="/login",
    response_model=User,
    status_code=status.HTTP_200_OK,
    summary="Login a User",
    tags=["Users"]
)
def login():
    pass

### Show all user
@app.get(
    path="/users",
    response_model=List[User],
    status_code=status.HTTP_200_OK,
    summary="Show all users",
    tags=["Users"]
)
def show_all_users():
    """
    This path operation shows all users in the app

    Parameters:
    -

    Returns a json list with all users in the app, with the following keys:
    - user_id: UUID
    - email: Emailstr
    - first_name: str
    - last_name: str
    - birth_date: datetime
    """
    with open("users.json", "r", encoding="utf-8") as f:
        results = json.loads(f.read())
        return results

### Show a user
@app.get(
    path="/users/{user_id}",
    response_model=User,
    status_code=status.HTTP_200_OK,
    summary="show a User",
    tags=["Users"]
)
def show_a_user():
    pass

### Delete a user
@app.delete(
    path="/users/{user_id}/delete",
    response_model=User,
    status_code=status.HTTP_200_OK,
    summary="Delete a User",
    tags=["Users"]
)
def delete_a_user():
    pass

### Update a user
@app.put(
    path="/users/{user_id}/update",
    response_model=User,
    status_code=status.HTTP_200_OK,
    summary="Update a User",
    tags=["Users"]
)
def update_a_user():
    pass

## Tweets

### Show all tweets
@app.get(
    path="/",
    response_model=List[Tweet],
    status_code=status.HTTP_200_OK,
    summary="Show all tweets",
    tags=["Tweets"]
)
def home():
    return {"Twitter API": "Working!"}

### Post a tweet
@app.post(
    path="/post",
    response_model=Tweet,
    status_code=status.HTTP_201_CREATED,
    summary="Post a tweet",
    tags=["Tweets"]
)
def post(tweet: Tweet = Body(...)):
    """
    Post a Tweet

    This path operations post a tweet in the app

    Parameters:
    - Request body parameter
        - tweet: Tweet

    Return a json with the basic tweet information:
    - tweet_id: UUID
    - content:str
    - created_at: datetime
    - updated_at: Optional[datetime]
    - by: User
    """
    with open("tweets.json", "r+", encoding="utf-8") as f:
        results = json.loads(f.read())
        tweet_dict = tweet.dict()
        tweet_dict["tweet_id"] = str(tweet_dict["tweet_id"])
        tweet_dict["created_at"] = str(tweet_dict["created_at"])
        if tweet_dict["updated_at"] is not None:
            tweet_dict["updated_at"] = str(tweet_dict["updated_at"])
        tweet_dict["by"]["user_id"] = str(tweet_dict["by"]["user_id"])
        tweet_dict["by"]["birth_date"] = str(tweet_dict["by"]["birth_date"])
        results.append(tweet_dict)
        f.seek(0)
        f.write(json.dumps(results))
        return tweet

### Show a tweet
@app.get(
    path="/tweets/{tweet_id}",
    response_model=Tweet,
    status_code=status.HTTP_200_OK,
    summary="Show a tweet",
    tags=["Tweets"]
)
def show_a_tweet():
    pass

### Delete a tweet
@app.delete(
    path="/tweets/{tweet_id}/delete",
    response_model=Tweet,
    status_code=status.HTTP_200_OK,
    summary="Delete a tweet",
    tags=["Tweets"]
)
def delete_a_tweet():
    pass

### Update a tweet
@app.put(
    path="/tweets/{tweet_id}/update",
    response_model=Tweet,
    status_code=status.HTTP_200_OK,
    summary="Update a tweet",
    tags=["Tweets"]
)
def update_a_tweet():
    pass
```

[documentación | Database conect](https://fastapi.tiangolo.com/tutorial/sql-databases/#create-the-sqlalchemy-parts) 

## Home de nuestra API

reto

```python
# Python
import json
from uuid import UUID
from datetime import date, datetime
from typing import Optional, List

#  Pydantic
from pydantic import BaseModel
from pydantic.networks import EmailStr
from pydantic import Field

#  FastAPI
from fastapi import FastAPI, status, HTTPException
from fastapi import Body, Form, Path

app = FastAPI()

# Models

class UserBase(BaseModel):
    user_id: UUID = Field(...)
    email: EmailStr = Field(...)

class UserLogin(UserBase):
    password: str = Field(
        ..., 
        min_length=8,
        max_length=64
    )

class User(UserBase):
    first_name: str = Field(
        ...,
        min_length=1,
        max_length=50
    )
    last_name: str = Field(
        ...,
        min_length=1,
        max_length=50
    )
    birth_date: Optional[date] = Field(default=None)

class UserRegister(User):
    password: str = Field(
        ..., 
        min_length=8,
        max_length=64
    )

class Tweet(BaseModel):
    tweet_id: UUID = Field(...)
    content: str = Field(
        ..., 
        min_length=1, 
        max_length=256
    )
    created_at: datetime = Field(default=datetime.now())
    updated_at: Optional[datetime] = Field(default=None)
    by: User = Field(...)

class LoginOut(BaseModel): 
    email: EmailStr = Field(...)
    message: str = Field(default="Login Successfully!")

"""# Auxiliar funcion 

## funcion reed
def read_data(file):
    with open("{}.json".format(file), "r+", encoding="utf-8") as f: 
        return json.loads(f.read())

## funcion write
def read_data(file, results):
    with open("{}.json".format(file), "r+", encoding="utf-8") as f: 
        f.seek(0)
        f.write(json.dumps(results))"""

    
# Path Operations



## Users

### Register a user
@app.post(
    path="/signup",
    response_model=User,
    status_code=status.HTTP_201_CREATED,
    summary="Register a User",
    tags=["Users"]
    )
def signup(user: UserRegister = Body(...)): 
    """
    Signup
    This path operation register a user in the app
    Parameters: 
        - Request body parameter
            - user: UserRegister
    
    Returns a json with the basic user information: 
        - user_id: UUID
        - email: Emailstr
        - first_name: str
        - last_name: str
        - birth_date: datetime
    """
    with open("users.json", "r+", encoding="utf-8") as f: 
        results = json.loads(f.read())
        user_dict = user.dict()
        user_dict["user_id"] = str(user_dict["user_id"])
        user_dict["birth_date"] = str(user_dict["birth_date"])
        results.append(user_dict)
        f.seek(0)
        f.write(json.dumps(results))
        return user

### Login a user
@app.post(
    path="/login",
    response_model=LoginOut,
    status_code=status.HTTP_200_OK,
    summary="Login a User",
    tags=["Users"]
    )
def Login(email: EmailStr  = Form(...), password: str = Form(...)):
    """
    Login

    This path operation login a Person in the app

    Parameters:
    - Request body parameters:
        - email: EmailStr
        - password: str

    Returns a LoginOut model with username and message
    """
    with open("users.json", "r+", encoding="utf-8") as f: 
        datos = json.loads(f.read())
        for user in datos:
            if email == user['email'] and password == user['password']:
                return LoginOut(email=email)
            else:
                return LoginOut(email=email, message="Login Unsuccessfully!")

### Show all users
@app.get(
    path="/user",
    response_model=List[User],
    status_code=status.HTTP_200_OK,
    summary="Show all users",
    tags=["Users"]
    )
def show_all_users():
    """
    This path operation shows all users in the app

    Parametes:
        -

    Return a json list whit all users in the app, with the following keys:
        - user_id: UUID
        - email: Emailstr
        - first_name: str
        - last_name: str
        - birth_date: datetime
    """
    with open("users.json", "r", encoding="utf-8") as f:
        results = json.loads(f.read())
        return results

### Show a user
@app.get(
    path="/users/{user_id}",
    response_model=User,
    status_code=status.HTTP_200_OK,
    summary="Show a User",
    tags=["Users"]
    )
def show_a_user(user_id: UUID = Path(
    ...,
    title="User ID",
    description="This is the user ID",
    example="3fa85f64-5717-4562-b3fc-2c963f66afa2"
    )):
    """
    Show a User

    This path operation show if a person exist in the app

    Parameters:
        - user_id: UUID

    Returns a json with user data:
        - user_id: UUID
        - email: Emailstr
        - first_name: str
        - last_name: str
        - birth_date: datetime
    """
    with open("users.json", "r+", encoding="utf-8") as f: 
        results = json.loads(f.read())
        id = str(user_id)
    for data in results:
        if data["user_id"] == id:
            return data
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"¡This user_id doesn't exist!"
        )

### Delete a user
@app.delete(
    path="/users/{user_id}/delete",
    response_model=User,
    status_code=status.HTTP_200_OK,
    summary="Delete a User",
    tags=["Users"]
    )
def delete_a_user(
    user_id: UUID = Path(
        ...,
        title="User ID",
        description="This is the user ID",
        example="3fa85f64-5717-4562-b3fc-2c963f66afa1"
    )):
    """
    Delete a User

    This path operation delete a user in the app

    Parameters:
        - user_id: UUID

    Returns a json with deleted user data:
        - user_id: UUID
        - email: Emailstr
        - first_name: str
        - last_name: str
        - birth_date: datetime
    """
    with open("users.json", "r+", encoding="utf-8") as f: 
        results = json.loads(f.read())
        id = str(user_id)
    for data in results:
        if data["user_id"] == id:
            results.remove(data)
            with open("users.json", "w", encoding="utf-8") as f:
                f.seek(0)
                f.write(json.dumps(results))
            return data
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="¡This user_id doesn't exist!"
        )

### Update a user
@app.put(
    path="/users/{user_id}/update",
    response_model=User,
    status_code=status.HTTP_200_OK,
    summary="Update a User",
    tags=["Users"]
    )
def update_a_user(
        user_id: UUID = Path(
            ...,
            title="User ID",
            description="This is the user ID",
            example="3fa85f64-5717-4562-b3fc-2c963f66afa3"
        ),
        user: UserRegister = Body(...)
    ):
    """
    Update User

    This path operation update a user information in the app and save in the database

    Parameters:
    - user_id: UUID
    - Request body parameter:
        - **user: User** -> A user model with user_id, email, first name, last name, birth date and password
    
    Returns a user model with user_id, email, first_name, last_name and birth_date
    """
    user_id = str(user_id)
    user_dict = user.dict()
    user_dict["user_id"] = str(user_dict["user_id"])
    user_dict["birth_date"] = str(user_dict["birth_date"])
    with open("users.json", "r+", encoding="utf-8") as f: 
        results = json.loads(f.read())
    for user in results:
        if user["user_id"] == user_id:
            results[results.index(user)] = user_dict
            with open("users.json", "w", encoding="utf-8") as f:
                f.seek(0)
                f.write(json.dumps(results))
            return user
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="¡This user_id doesn't exist!"
        )


## Tweets

### Show all tweers
@app.get(
    path="/",
    response_model=List[Tweet],
    status_code=status.HTTP_200_OK,
    summary="Show all tweets",
    tags=["Tweets"]
    )
def home():
    """
    This path operation shows all tweets in the app

    Parametes:
        -

    Return a json list whit all tweets in the app, with the following keys:
        - tweet_id: UUID  
        - content: str    
        - created_at: datetime
        - updated_at: Optional[datetime]
        - by: User
    """
    with open("tweets.json", "r", encoding="utf-8") as f:
        results = json.loads(f.read())
        return results

### Post a tweers
@app.post(
    path="/post",
    response_model=Tweet,
    status_code=status.HTTP_201_CREATED,
    summary="Post a tweet",
    tags=["Tweets"]
    )
def post(tweet: Tweet = Body(...)): 
    """
    Post a Tweet

    This path operation post a tweet in the app

    Parameters: 
        - Request body parameter
            - tweet: Tweet
    
    Returns a json with the basic tweet information: 
        - tweet_id: UUID  
        - content: str    
        - created_at: datetime
        - updated_at: Optional[datetime]
        - by: User
    """
    with open("tweets.json", "r+", encoding="utf-8") as f: 
        results = json.loads(f.read())
        tweet_dict = tweet.dict()
        tweet_dict["tweet_id"] = str(tweet_dict["tweet_id"])
        tweet_dict["created_at"] = str(tweet_dict["created_at"])
        tweet_dict["updated_at"] = str(tweet_dict["updated_at"])
        tweet_dict["by"]["user_id"] = str(tweet_dict["by"]["user_id"])
        tweet_dict["by"]["birth_date"] = str(tweet_dict["by"]["birth_date"])

        results.append(tweet_dict)
        f.seek(0)
        f.write(json.dumps(results))
        return tweet

### Show a tweers
@app.get(
    path="/tweets/{tweet_id}",
    response_model=Tweet,
    status_code=status.HTTP_200_OK,
    summary="Show a tweet",
    tags=["Tweets"]
    )
def show_a_tweet(tweet_id: UUID = Path(
    ...,
    title="Tweet ID",
    description="This is the tweet ID",
    example="3fa85f64-5717-4562-b3fc-2c963f66afa6"
    )):
    """
    Show a Tweet

    This path operation show if a tweet exist in the app

    Parameters:
        - tweet_id: UUID

    Returns a json with tweet data:
        - tweet_id: UUID
        - content: str
        - created_at: datetime
        - updated_at: Optional[datetime]
        - by: User
    """
    with open("tweets.json", "r+", encoding="utf-8") as f: 
        results = json.loads(f.read())
        id = str(tweet_id)
    for data in results:
        if data["tweet_id"] == id:
            return data
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"¡This tweet_id doesn't exist!"
        )

### Delete a tweers
@app.delete(
    path="/tweets/{tweet_id}/delete",
    response_model=Tweet,
    status_code=status.HTTP_200_OK,
    summary="Delete a tweet",
    tags=["Tweets"]
    )
def delete_a_tweet(
    tweet_id: UUID = Path(
        ...,
        title="Tweet ID",
        description="This is the tweet ID",
        example="3fa85f64-5717-4562-b3fc-2c963f66afa2"
    )):
    """
    Delete a Tweet

    This path operation delete a tweet in the app

    Parameters:
        - tweet_id: UUID

    Returns a json with deleted tweet data:
        - tweet_id: UUID
        - content: str
        - created_at: datetime
        - updated_at: Optional[datetime]
        - by: User
    """
    with open("tweets.json", "r+", encoding="utf-8") as f: 
        results = json.loads(f.read())
        id = str(tweet_id)
    for data in results:
        if data["tweet_id"] == id:
            results.remove(data)
            with open("tweets.json", "w", encoding="utf-8") as f:
                f.seek(0)
                f.write(json.dumps(results))
            return data
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="¡This tweet_id doesn't exist!"
        )

### Update a tweers
@app.put(
    path="/tweets/{tweet_id}/update",
    response_model=Tweet,
    status_code=status.HTTP_200_OK,
    summary="Update a tweet",
    tags=["Tweets"]
    )
def upsate_a_tweet(
        tweet_id: UUID = Path(
            ...,
            title="Tweet ID",
            description="This is the tweet ID",
            example="3fa85f64-5717-4562-b3fc-2c963f66afa8"
        ),
         content: str = Form(
        ..., 
        min_length=1,
        max_length=256,
        title="Tweet content",
        description="This is the content of the tweet",
        )
    ):
    """
    Update Tweet

    This path operation update a tweet information in the app and save in the database

    Parameters:
    - tweet_id: UUID
    - contet:str
    
    Returns a json with:
        - tweet_id: UUID
        - content: str 
        - created_at: datetime 
        - updated_at: datetime
        - by: user: User
    """
    tweet_id = str(tweet_id)
    # tweet_dict = tweet.dict()
    # tweet_dict["tweet_id"] = str(tweet_dict["tweet_id"])
    # tweet_dict["birth_date"] = str(tweet_dict["birth_date"])
    with open("tweets.json", "r+", encoding="utf-8") as f: 
        results = json.loads(f.read())
    for tweet in results:
        if tweet["tweet_id"] == tweet_id:
            tweet['content'] = content
            tweet['updated_at'] = str(datetime.now())
            print(tweet)
            with open("tweets.json", "w", encoding="utf-8") as f:
                f.seek(0)
                f.write(json.dumps(results))
            return tweet
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="¡This tweet_id doesn't exist!"
        )
```

# 7. Conclusiones

## Has creado tu primera API

- Conexión a base de datos con SQLAlchemy
- Login y Autenticación de usuarios con JWT.
- Refresh Token
- Protección de información por usuario.
- APIRouter.
- Refactor de arquitectura.
- Docker y Docker Compose.

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[**github | JoseNoriegaa**](https://github.com/JoseNoriegaa/platzi-twitter-api-fastapi)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/curso-fastapi-twitter-api: This is the official repo of the Twitter API, created in the Platzi FastAPI courses saga 🚀](https://github.com/platzi/curso-fastapi-twitter-api)
