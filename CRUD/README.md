<h1>Python: Creación de un CRUD</h1>

<h2>David Aroesti</h2>

<h1>Tabla de Contenido</h1>

- [1. Básicos del Lenguaje 🐍](#1-básicos-del-lenguaje-)
  - [Guía de instalación y conceptos básicos](#guía-de-instalación-y-conceptos-básicos)
  - [Archivos y slides del curso práctico de Python](#archivos-y-slides-del-curso-práctico-de-python)
  - [IMPORTANTE: Instalando Ubuntu Bash en Windows para facilitarte el seguimiento del curso desde Windows.](#importante-instalando-ubuntu-bash-en-windows-para-facilitarte-el-seguimiento-del-curso-desde-windows)
  - [¿Qué es la programación?](#qué-es-la-programación)
  - [¿Por qué programar con Python?](#por-qué-programar-con-python)
  - [Operadores matemáticos](#operadores-matemáticos)
  - [Variables y expresiones](#variables-y-expresiones)
  - [Presentación del proyecto](#presentación-del-proyecto)
  - [Funciones](#funciones)
  - [Usando funciones en nuestro proyecto](#usando-funciones-en-nuestro-proyecto)
  - [Operadores lógicos](#operadores-lógicos)
  - [Estructuras condicionales](#estructuras-condicionales)
- [2. Uso de strings y ciclos](#2-uso-de-strings-y-ciclos)
  - [Strings en Python](#strings-en-python)
  - [Operaciones con Strings en Python](#operaciones-con-strings-en-python)
  - [Operaciones con strings y el comando Update](#operaciones-con-strings-y-el-comando-update)
  - [Operaciones con strings y el comando Delete](#operaciones-con-strings-y-el-comando-delete)
  - [Operaciones con strings: Slices en python](#operaciones-con-strings-slices-en-python)
  - [For loops](#for-loops)
  - [While loops](#while-loops)
  - [Iterators and generators](#iterators-and-generators)
- [3. Estructuras de Datos](#3-estructuras-de-datos)
  - [Uso de listas](#uso-de-listas)
  - [Operaciones con listas](#operaciones-con-listas)
  - [Agregando listas a nuestro proyecto](#agregando-listas-a-nuestro-proyecto)
  - [Diccionarios](#diccionarios)
  - [Agregando diccionarios a nuestro proyecto](#agregando-diccionarios-a-nuestro-proyecto)
  - [Tuplas y conjuntos](#tuplas-y-conjuntos)
  - [Tuplas y conjuntos en código](#tuplas-y-conjuntos-en-código)
  - [Introducción al módulo collections](#introducción-al-módulo-collections)
  - [Python comprehensions](#python-comprehensions)
  - [Búsquedas binarias](#búsquedas-binarias)
  - [Continuando con las Búsquedas Binarias](#continuando-con-las-búsquedas-binarias)
  - [Manipulación de archivos en Python 3](#manipulación-de-archivos-en-python-3)
- [4. Uso de objetos y módulos](#4-uso-de-objetos-y-módulos)
  - [Decoradores](#decoradores)
  - [Decoradores en Python](#decoradores-en-python)
  - [¿Qué es la programación orientada a objetos?](#qué-es-la-programación-orientada-a-objetos)
  - [Programación orientada a objetos en Python](#programación-orientada-a-objetos-en-python)
  - [Scopes and namespaces](#scopes-and-namespaces)
  - [Introducción a Click](#introducción-a-click)
  - [Definición a la API pública](#definición-a-la-api-pública)
  - [Clients](#clients)
  - [Servicios: Lógica de negocio de nuestra aplicación](#servicios-lógica-de-negocio-de-nuestra-aplicación)
  - [Interface de create: Comunicación entre servicios y el cliente](#interface-de-create-comunicación-entre-servicios-y-el-cliente)
  - [Actualización de cliente](#actualización-de-cliente)
  - [Interface de actualización](#interface-de-actualización)
  - [Manejo de errores y jerarquía de errores en Python](#manejo-de-errores-y-jerarquía-de-errores-en-python)
  - [Context managers](#context-managers)
- [5. Python en el mundo real](#5-python-en-el-mundo-real)
  - [Aplicaciones de Python en el mundo real](#aplicaciones-de-python-en-el-mundo-real)
- [6. Conclusiones finales](#6-conclusiones-finales)
  - [Python 2 vs 3 (Conclusiones)](#python-2-vs-3-conclusiones)
- [7. Clases bonus](#7-clases-bonus)
  - [Entorno Virtual en Python y su importancia:](#entorno-virtual-en-python-y-su-importancia)

# 1. Básicos del Lenguaje 🐍

## Guía de instalación y conceptos básicos

## ¿Qué es Python?

Python es un lenguaje de programación creado por [Guido Van Rossum](https://en.wikipedia.org/wiki/Guido_van_Rossum), con una sintaxis muy limpia, ideado para enseñar a la gente a programar bien. Se trata de un lenguaje interpretado o de *script*.

## Ventajas:

- **Legible**: sintaxis intuitiva y estricta.
- **Productivo**: ahorra mucho código.
- **Portable**: para todo sistema operativo.
- **Recargado**: viene con muchas librerías por defecto.

Editor recomendado: *[Atom](https://atom.io/)* o *[Sublime Text](https://www.sublimetext.com/)*.

## Instalación

Existen dos versiones de Python que tienen gran uso actualmente, *Python 2.x* y *Python 3.x*, para este curso necesitas usar una **versión 3.x**

Para instalar Python solo debes seguir los pasos dependiendo del sistema operativo que tengas instalado.

### Windows

Para instalar Python en Windows ve al sitio https://www.python.org/downloads/ y presiona sobre el botón *Download Python 3.7.3*

Se descargará un archivo de instalación con el nombre *python-3.7.3.exe* , ejecútalo. Y sigue los pasos de instalación.

Al finalizar la instalación haz lo siguiente para corroborar una instalación correcta

1. Presiona las teclas Windows + R para abrir la ventana de *Ejecutar*.
2. Una vez abierta la ventana *Ejecutar* escribe el comando `cmd` y presiona ctrl+shift+enter para ejecutar una línea de comandos con permisos de administrador.
3. Windows te preguntará si quieres abrir el *Procesador de comandos de Windows* con permisos de administrador, presiona sí.
4. En la línea de comandos escribe `python`

Tu consola se mostrará así.
![Captura de pantalla (17).png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%20%2817%29-4a934aae-8602-4783-8510-f7bdbbded60d.jpg)

¡Ya estás listo para continuar con el curso!

### MacOS

La forma sencilla es tener instalado [homebrew](https://brew.sh/) y usar el comando:

** Para instalar la Versión 2.7**

```
brew install python
```

**Para instalar la Versión 3.x**

```
brew install python3
```

### Linux

Generalmente Linux ya lo trae instalado, para comprobarlo puedes ejecutar en la terminal el comando

**Versión 2.7**

```
python -v
```

**Versión 3.x**

```
python3 -v
```

Si el comando arroja un error quiere decir que no lo tienes instalado, en ese caso los pasos para instalarlo cambian un poco de acuerdo con la distribución de linux que estés usando. Generalmente el gestor de paquetes de la distribución de Linux tiene el paquete de Python

**Si eres usuario de Ubuntu o Debian por ejemplo puedes usar este comando para instalar la versión 3.1:**

```
$ sudo apt-get install python3.1
```

**Si eres usuario de Red Hat o Centos por ejemplo puedes usar este comando para instalar python**

```
$ sudo yum install python
```

Si usas otra distribución o no has podido instalar python en tu sistema Linux dejame un comentario y vemos tu caso específico.

Si eres usuario habitual de linux también puedes [descargar los archivos](https://www.python.org/downloads/source/) para instalarlo manualmente.

## Antes de empezar:

Para usar Python debemos tener un editor de texto abierto y una terminal o cmd (línea de comandos en Windows) como administrador.

No le tengas miedo a la consola, la consola es tu amiga.

Para ejecutar Python abre la terminal y escribe:

```
python
```

Te abrirá una consola de Python, lo notarás porque el *prompt* cambia y ahora te muestra tres simbolos de mayor que “ >>> “ y el puntero adelante indicando que puedes empezar a ingresar comandos de python.

```
 >>> 
```

En éste modo puedes usar todos los comandos de Python o escribir código directamente.

*Si deseas ejecutar código de un archivo sólo debes guardarlo con [extension.py](http://extension.py/) y luego ejecutar en la terminal:

```
 $ python archivo.py
```

Ten en cuenta que para ejecutar el archivo con extensión “.py” debes estar ubicado en el directorio donde tienes guardado el archivo.

**Para salir de Python** y regresar a la terminal debes usar el comando `exit()`

Cuando usamos Python debemos atender ciertas reglas de la comunidad para definir su estructura. Las encuentras en el libro [PEP8](https://www.python.org/dev/peps/pep-0008/).

## Tipos de datos en Python

- **Enteros (int)**: en este grupo están todos los números, enteros y *long:*

*ejemplo: 1, 2.3, 2121, 2192, -123*

- **Booleanos (bool):** Son los valores falso o verdadero, compatibles con todas las operaciones booleanas ( and, not, or ):

*ejemplo: True, False*

- **Cadenas (str):** Son una cadena de texto :

*ejemplos: “Hola”, “¿Cómo estas?”*

- **Listas:** Son un grupo o array de datos, puede contener cualquiera de los datos anteriores:

*ejemplos: [1,2,3, ”hola” , [1,2,3] ], [1,“Hola”,True ]*

- **Diccionarios:** Son un grupo de datos que se acceden a partir de una clave:

*ejemplo: {“clave”:”valor”}, {“nombre”:”Fernando”}*

- **Tuplas:** también son un grupo de datos igual que una lista con la diferencia que una tupla después de creada no se puede modificar.

*ejemplos: (1,2,3, ”hola” , (1,2,3) ), (1,“Hola”,True ) (Pero jamás podremos cambiar los elementos dentro de esa Tupla)*

En Python trabajas con **módulos** y **ficheros** que usas para importar las librerías.

## Funciones

Las funciones las defines con **def** junto a un nombre y unos paréntesis que reciben los parámetros a usar. Terminas con dos puntos.

**def** nombre_de_la_función(**parametros**):

Después por indentación colocas los datos que se ejecutarán desde la función:

```python
 >>> def my_first_function():
 ...	return “Hello World!” 
 ...    
 >>> my_first_function()
```

Hello World!

## Variables

Las variables, a diferencia de los demás lenguajes de programación, no debes definirlas, ni tampoco su tipo de dato, ya que al momento de iterarlas se identificará su tipo. Recuerda que en Python todo es un objeto.

```python
 A = 3 
 B = A
```

## Listas

Las listas las declaras con corchetes. Estas pueden tener una lista dentro o cualquier tipo de dato.

```python
 >>> L = [22, True, ”una lista”, [1, 2]] 
 >>> L[0] 
 22
```

## Tuplas

Las tuplas se declaran con paréntesis, recuerda que no puedes editar los datos de una tupla después de que la has creado.

```python
 >>> T = (22, True, "una tupla", (1, 2)) 
 >>> T[0] 
 22
```

## Diccionarios

En los diccionarios tienes un grupo de datos con un formato: la primera cadena o número será la clave para acceder al segundo dato, el segundo dato será el dato al cual accederás con la llave. Recuerda que los diccionarios son listas de llave:valor.

```python
 >>> D = {"Kill Bill": "Tamarino", "Amelie": "Jean-Pierre Jeunet"} 
 >>> D["Kill Bill"]
 "Tamarino"
```

## Conversiones

De flotante a entero:

```python
 >>> int(4.3)
 4
```

De entero a flotante:

```python
 >>> float(4) 
 4.0
```

De entero a string:

```python
 >>> str(4.3) 
 "4.3"
```

De tupla a lista:

```python
 >>> list((4, 5, 2)) 
 [4, 5, 2]
```

## Operadores Comunes

Longitud de una cadena, lista, tupla, etc.:

```python
 >>> len("key") 
 3
```

Tipo de dato:

```python
 >>> type(4) 
 < type int >
```

Aplicar una conversión a un conjunto como una lista:

```python
 >>> map(str, [1, 2, 3, 4])
 ['1', '2', '3', '4']
```

Redondear un flotante con x número de decimales:

```python
>>> round(6.3243, 1)
 6.3
```

Generar un rango en una lista (esto es mágico):

```python
 >>> range(5) 
 [0, 1, 2, 3, 4]
```

Sumar un conjunto:

```python
 >>> sum([1, 2, 4]) 
 7
```

Organizar un conjunto:

```python
 >>> sorted([5, 2, 1]) 
 [1, 2, 5]
```

Conocer los comandos que le puedes aplicar a x tipo de datos:

```python
 >>>Li = [5, 2, 1]
 >>>dir(Li)
 >>>['append', 'count', 'extend', 'index', 'insert', 'pop', 'remove', 'reverse', 'sort']
 
```

‘append’, ‘count’, ‘extend’, ‘index’, ‘insert’, ‘pop’, ‘remove’, ‘reverse’, ‘sort’ son posibles comandos que puedes aplicar a una lista.

Información sobre una función o librería:

```python
 >>> help(sorted) 
 (Aparecerá la documentación de la función sorted)
```

## Clases

Clases es uno de los conceptos con más definiciones en la programación, pero en resumen sólo son la representación de un objeto. Para definir la clase usas_ class_ y el nombre. En caso de tener parámetros los pones entre paréntesis.

Para crear un constructor haces una función dentro de la clase con el nombre **init** y de parámetros self (significa su clase misma), nombre_r y edad_r:

```python
 >>> class Estudiante(object): 
 ... 	def __init__(self,nombre_r,edad_r): 
 ... 		self.nombre = nombre_r 
 ... 		self.edad = edad_r 
 ...
 ... 	def hola(self): 
 ... 		return "Mi nombre es %s y tengo %i" % (self.nombre, self.edad) 
 ... 
 >>> e = Estudiante(“Arturo”, 21) 
 >>> print (e.hola())
 Mi nombre es Arturo y tengo 21
```

Lo que hicimos en las dos últimas líneas fue:

\1. En la variable e llamamos la clase Estudiante y le pasamos la cadena “Arturo” y el entero 21.

\2. Imprimimos la función hola() dentro de la variable e (a la que anteriormente habíamos pasado la clase).

Y por eso se imprime la cadena “Mi nombre es Arturo y tengo 21”

## Métodos especiales

**cmp**(self,otro)
Método llamado cuando utilizas los operadores de comparación para comprobar si tu objeto es menor, mayor o igual al objeto pasado como parámetro.

**len**(self)
Método llamado para comprobar la longitud del objeto. Lo usas, por ejemplo, cuando llamas la función len(obj) sobre nuestro código. Como es de suponer el método te debe devolver la longitud del objeto.

**init**(self,otro)
Es un constructor de nuestra clase, es decir, es un “método especial” que se llama automáticamente cuando creas un objeto.

## Condicionales IF

Los condicionales tienen la siguiente estructura. Ten en cuenta que lo que contiene los paréntesis es la comparación que debe cumplir para que los elementos se cumplan.

```python
 if ( a > b ):
 	elementos 
 elif ( a == b ): 
 	elementos 
 else:
 	elementos
```

## Bucle FOR

El bucle de for lo puedes usar de la siguiente forma: recorres una cadena o lista a la cual va a tomar el elemento en cuestión con la siguiente estructura:

```python
 for i in ____:
 	elementos
```

Ejemplo:

```python
 for i in range(10):
 	print i
```

En este caso recorrerá una lista de diez elementos, es decir el _print i _de ejecutar diez veces. Ahora i va a tomar cada valor de la lista, entonces este for imprimirá los números del 0 al 9 (recordar que en un *range* vas hasta el número puesto -1).

## Bucle WHILE

En este caso while tiene una condición que determina hasta cuándo se ejecutará. O sea que dejará de ejecutarse en el momento en que la condición deje de ser cierta. La estructura de un while es la siguiente:

```python
 while (condición):
 	elementos
```

Ejemplo:

```python
 >>> x = 0 
 >>> while x < 10: 
 ... 	print x 
 ... 	x += 1
```

En este ejemplo preguntará si es menor que diez. Dado que es menor imprimirá x y luego sumará una unidad a x. Luego x es 1 y como sigue siendo menor a diez se seguirá ejecutando, y así sucesivamente hasta que x llegue a ser mayor o igual a 10.

#### Cómo seguir

No te preocupes si en este punto no entiendes algunos de estos conceptos, sigue con el curso donde vamos a realizar ejercicios que te ayuden a comprender y poder aplicar cada una de las características de Python.

Adelante!

## Archivos y slides del curso práctico de Python

Bienvenida o bienvenido a este nuevo curso de Python 3, en este curso aprenderás los conceptos más importantes del lenguaje a través del desarrollo de un proyecto que funciona como un CRUD utilizando Python 3 puro.

A continuación encontrarás los slides en formato pdf:
https://drive.google.com/file/d/1uAC0egE_U6571mV8gHtHq5ahIbo9vd1e/view?usp=sharing

Y también el repositorio completo del curso en el cual encontrarás todo el proyecto dividido en secciones tal como se fue desarrollando:
https://github.com/platzi/curso_Python3/branches

## IMPORTANTE: Instalando Ubuntu Bash en Windows para facilitarte el seguimiento del curso desde Windows.

¡Hola!
En este tutorial te enseñaré a configurar el Ubuntu dentro de tú Windows 10 para que puedas ejecutar los comandos tal como los ejecuta el profesor en el curso.

Lo primero que necesitas es que tu computadora tenga instalado Windows 10 de 64 bits y tengas tu sistema operativo actualizado (con el “Windows 10 Anniversary Update”)

Una vez hayas verificado que tu computadora cumple con los requisitos entra a los settings del sistema (Ajustes)
![primeraImagen.png](https://static.platzi.com/media/user_upload/primeraImagen-7c275e36-acc9-44de-9083-6239616057f5.jpg)

Luego entra a la opción de Actualizaciones y Seguridad
![segundaImagen.png](https://static.platzi.com/media/user_upload/segundaImagen-dd3d3960-8b25-4243-b660-ae3e0e621a32.jpg)

En el menú de la izquierda has click en opciones para desarrolladores y habilita el “Modo Desarrollador”
![terceraImagen.PNG](https://static.platzi.com/media/user_upload/terceraImagen-66f119ea-024f-4311-9eac-316e4905ef42.jpg)![CuartaImagen.PNG](https://static.platzi.com/media/user_upload/CuartaImagen-bc840717-e213-440c-bd8d-a9f85aab1e2b.jpg)

Después, accede al panel de control y haz click en “Programas”

![5taimagen.png](https://static.platzi.com/media/user_upload/5taimagen-aab373d0-7f53-4476-b14c-09075f8c7049.jpg)

Una vez ahí, haz click en activar o desactivar características de windows
![sextaImagen.PNG](https://static.platzi.com/media/user_upload/sextaImagen-f0980102-6985-4706-a526-bb5a71311bee.jpg)
Aquí, busca la opción de “Windows Subsystem for Linux” y actívala, instala eso y permite que tu computadora se reinicie.

Luego, entra al menú inicio, escribe bash y sigue los pasos que te indique, en caso de que te diga que no tienes ninguna distribución sólo ve a la tienda de aplicaciones y descargaba Ubuntu para Windows.
![ultimaImage.PNG](https://static.platzi.com/media/user_upload/ultimaImage-c8c3e20b-501f-4729-8086-68dc96454eb8.jpg)

Luego, ejecuta Ubuntu, crea tu usuario y contraseña y estás lista o listo para continuar.

![finalResult.PNG](https://static.platzi.com/media/user_upload/finalResult-2f050e19-05c0-4e64-be47-de5f4b1fdfb7.jpg)

por último, instala Python usando

```sh
sudo apt-get update
```

y luego ejecuta

```sh
sudo apt-get install python3
```

una vez termine la instalación, prueba ejecutando “python3”

Bonus: para moverte a tus carpetas en tu disco duro usa el siguiente comando:

```sh
cd ../../mnt/c/Users/NOMBREDEUSUARIO/
```

## ¿Qué es la programación?

Python es uno de los lenguajes más emocionantes de la actualidad y puedes lograr muchas cosas con él. Este curso te va a servir como una introducción al lenguaje.

**¿Qué es la programación?**

Es una **disciplina** que combina parte de otras disciplinas como las **Matemáticas**, **Ingeniería** y la **Ciencia**. Sin embargo, la habilidad más importante es **resolver problemas**. Es lo que harás todos los días como programador o programadora.

La programación es una **secuencia de instrucciones** que le damos a la computadora para que haga lo que nosotros deseamos. Podemos construir una aplicación web, móvil, un programa que lleve cohetes a la luna o marte, resolver problemas de finanzas.

La estructura de un programa. Casi todos los programas tienen un input, output, operaciones matemáticas, ejecución condicional y repeticiones

**Objetivos del curso:**

- Aprender a pensar como un Científico de la Computación
- Aprender a utilizar Python
- Entender las ventajas y desventajas de Python
- Aprender a construir una aplicación de línea de comandos.

## ¿Por qué programar con Python?

Python es uno de los mejores lenguajes para principiantes porque tiene una sintaxis clara, una gran comunidad y esto hace que el lenguaje sea muy **amigable** para los que están iniciando.

Python esta diseñado para ser **fácil de usar**, a diferencia de otros lenguajes donde la prioridad es ser rápido y eficiente. Python no es de los lenguajes más rápidos, pero casi nunca importa.

Es el **tercer lenguaje**, según Github, entre los más populares. En StackOverflow se comenta que es uno de los lenguajes que mayor popularidad esta obteniendo.

*““Python cuando podamos, C++ cuando necesitemos””*

`python --version` para conocer la versión que tenemos instalada
`python [nombre del archivo]` para ejecutar nuestro programa.

> **Funciones principales en Turtle Graphics**
>
> Las funciones principales para animar nuestro objeto son las siguientes:
>
> **forward(distance)**: Avanzar una determinada cantidad de píxeles.
> **backward(distance)**: Retroceder una determinada cantidad de píxeles.
> **left(angle)**: Girar hacia la izquierda un determinado ángulo.
> **right(angle)**: Girar hacia la derecha un determinado ángulo.
>
> Por otro lado, puede que en ocasiones queramos desplazarnos de un punto a otro sin dejar rastro. Para ello utilizaremos las siguientes funciones:
>
> **home(distance)**: Desplazarse al origen de coordenadas.
> **goto((x, y))**: Desplazarse a una coordenada en concreto.
> **pendown()**: Bajar el lápiz para no mostrar el rastro.
> **penup()**: Subir el lápiz para mostrar el rastro.
>
> Por último, puede que queramos cambiar el color o tamaño del lápiz. En ese caso utilizaremos las siguientes funciones gráficas:
>
> **shape(‘turtle’)**: Cambia al objeto tortuga.
> **pencolor(color)**: Cambiar al color especificado.
> **pensize(dimension)**: Tamaño de la punta del lápiz.

[![img](https://www.google.com/s2/favicons?domain=https://www.python.org//static/favicon.ico)Welcome to Python.org](https://www.python.org/)

## Operadores matemáticos

En **programación** estos operadores son muy similares a nuestras clases básicas de matemáticas.

- `//`: Es división de entero, básicamente tiramos la parte decimal
- `%`: Es el residuo de la división, lo que te sobra.
- `**`: Exponente

Los operadores son **contextuales**, dependen del tipo de valor. Un valor es la representación de una entidad que puede ser manipulada por un programa.

Podemos conocer el tipo del valor con type() y nos devolverá algo similar a `<class 'init'>, <class 'float'>, <class 'str'>`. Dependiendo del tipo los operadores van a funcionar de manera diferente.

**Operadores lógicos:** Sirven para realizar comparaciones, devuelven un valor verdadero o falso.

```python
Operador	|Descripción				| Ejemplo

and	      |¿se cumple a y b? 	| r=True and False # r es False

or	      |¿se cumple a o b?	| r=True o False # r es True

not	      | No a							| r=not True #r es False
```

**Operadores relacionales:** comparan dos expresiones y devuelven un valor verdadero o falso.

```python
Operador	| Descripción 						|	Ejemplo

== 				|	¿Son iguales a y b?			| r=5==3 # r es False

!= 				|	¿Son distintos a y b?		| r=5!=3 # r es True

< 				|	¿Es a menor que b?			| r=5<3 # r es False

> 				|	¿Es a mayor que b?			| r=5>3 3 r es True

<= 				|	¿Es a menor o igual que b?	| r=5<=5 # r es True

>= 				|	¿Es a mayor o igual que b?	| r=5>=3 # r es True
```

![pemdas.PNG](https://static.platzi.com/media/user_upload/pemdas-eb6b0f3f-24fc-4b2f-a80a-3383331a9abd.jpg)

Un resumen de la clase, mas unos operadores adicionales:

```python
print(22//4)   #division mostrando numero entero

print(10%4)    #modulo, residuo de la division

print(10**4)   #numero elevado a la potencia

print(type(5))  #muestra el tipo de valor, en este caso entero

print(type(5.5))  #muestra el tipo de valor, en este caso decimal

print(type("a"))  #muestra el tipo de valor, en este caso string

print("a"+"b")  #se puede hacer una concatenacion con dos strings

print("a"*3)  #se asigna que se repita el string 

print(5 == 3)  #se comprueba si dos valores son iguales o no. Bota true or false

print(5 != 3)  #se comprueba si dos valores son iguales o no. Bota true or false

print(8>5>3)  #se compara si son mayor o menor. Bota true or false`
```

## Variables y expresiones

Una **variable** es simplemente el contenedor de un valor. Es una forma de decirle a la computadora de que nos guarde un valor para luego usarlo.

Python es un lenguaje **dinámico**, este concepto de privado y público se genera por convenciones del lenguaje. En programación el signo `=` significa asignación.

Si una variable esta en mayúscula, usualmente se refiere a una constante, no debería reasignarse. Es una convención.

**Reglas de Variables**:

- Pueden contener números y letras
- No deben comenzar con número
- Múltiples palabras se unen con _
- No se pueden utilizar palabras reservadas

Expresiones son instrucciones para el interprete para evaluar la expresión. Los enunciados tienen efectos dentro del programa, como `print` que genera un **output**.

**PEMDAS** = Paréntesis, Exponente, Multiplicación-División, Adición-Sustracción.

![reserved_word_python.png](https://static.platzi.com/media/user_upload/reserved_word_python-b57fd09e-9da2-44eb-82e5-225865140aec.jpg)

***Puntos destacados:***

- una variable que empieza con guion bajo ( “_” ) es PRIVADA
- una variable toda en mayuscula es una CONSTANTE
- variables que empiezan con doble guion bajo ( “__”) SEÑAL DEL PROGRAMADOR PARA NO MODIFICAR LA VARIABLE
- Las variables se pueden reasignar
- No pueden comenzar con numeros
- No se pueden utilizar las palabras reservadas de Python como variable.

 Una variable es simplemente el contenedor de un valor. Es una forma de decirle a la computadora de que nos guarde un valor para luego usarlo.

 Hay variables públicas, privadas y súper privadas

 Python es un lenguaje dinámico, este concepto de privado y público se genera por convenciones del lenguaje.

 En programación el signo = significa asignación.

 El _ al inicio del nombre de una variable quiere decir que esa variable es privada. Ejemplo: _age = 20

 El doble guion bajo al inicio del nombre de una variable quiere decir que esa variable no se puede modificar por nada del mundo. Ejemplo: __variable_importante = “no tocar”

 Las variables en general pueden reasignarse o cambiarse el valor que contienen posteriormente

 Si una variable esta en mayúscula, usualmente se refiere a una constante, no debería reasignarse o modificarse. Es una convención.

 Reglas de Variables:

o Pueden contener números y letras
o No deben comenzar con número
o Múltiples palabras se unen con _
o No se pueden utilizar palabras reservadas por Python, las cuales son:

![nombres restringidos python.png](https://static.platzi.com/media/user_upload/nombres%20restringidos%20python-83e2a24a-1b04-4dc9-9398-7cbcf0772bb6.jpg)

 Una expresión es una combinación de valores, variables y operadores.
o El intérprete evalúa expresiones. Ejemplo: 2 + 2

 Expresiones son instrucciones para el intérprete para que evalué la expresión.

 Un enunciado (statements) es una unidad de código que tiene un efecto dentro del programa. Ejemplo: una asignación age = 20

 Los enunciados tienen efectos dentro del programa, como print que genera un output.

 Orden de operaciones (de arriba hacia abajo):
o Paréntesis
o Exponente
o Multiplicación
o División
o Adición
o Sustracción

PEMDAS = Paréntesis, Exponente, Multiplicación, Adicción, Sustracción.

## Reglas de Variables:

- Pueden contener números y letras
- No deben comenzar con número
- Múltiples palabras se unen con _
- No se pueden utilizar palabras reservadas

Expresiones son instrucciones para el interprete para evaluar la expresión. Los enunciados tienen efectos dentro del programa, como `print` que genera un **output**.

**PEMDAS** = Paréntesis, Exponente, Multiplicación, Adicción, Substracción

## Palabras reservadas

| and   | del     | for      | is    | raise  | assert |
| ----- | ------- | -------- | ----- | ------ | ------ |
| if    | else    | elif     | from  | lambda | return |
| break | global  | not      | try   | class  | except |
| or    | while   | continue | exec  | import | yield  |
| def   | finally | in       | print |        |        |

## Sobre las variables en python

- Pueden reasignarse cuantas veces se necesite
- No pueden comenzar con numeros
- No se pueden utilizar las palabras reservadas de Python como variable.
- Se utiliza snake_case (guión bajo) para dividir las palabras de las variables multipalabra

**Palabras reservadas del lenguaje:**

| and | del | for | is | raise | assert |
| if | else | elif | from | lambda | return |
| break | global | not | try | class | except |
| or | while | continue | exec | import | yield |
| def | finally | in | print |

## Convenciones en el nombre de las variables

No existen las variables privadas, y la reasignación es muy común en python, por lo que algo que nos puede proteger de no cometer errores graves en el uso de las variables es entender las convenciones de nombrado de variables que utilizan los programadores (ya que se puede nombrar una variable de diversas formas, pero eso no significa que se deba).

- **variables_regulares**: `snake_case`
- **CONSTANTES**: Una variable toda en mayuscula no debería de modificarse
- **_privada**: `Single Leading Underscore:` **`_var`** : una variable que empieza con guion bajo ( “_” ) se deberá tratar como _privada y no se deberá de acceder fuera de la clase
- **__importante**: `Double Leading Underscore:` **`__`** variables que empiezan con doble guion bajo ( “__”) son`Variables importantes`, si se llegara a modificar es provable que se pierda estabilidad del programa, así que mejor no tocarla mucho
- **var_**: `Single Trailing Underscore:` **`var_`**: A veces, el nombre más apropiado para una variable lo toma una palabra clave. Por lo tanto, los nombres como class o def no se pueden usar como nombres de variables en Python. En este caso, puede agregar un solo guión bajo ("_") para romper el conflicto de nombres:
- **__main__**: `Double Leading and Trailing Underscore: __var__`: Las variables rodeadas por un prefijo de subrayado doble y un postfix quedan reservadas por el intérprete de Python.

![pemdas.PNG](https://static.platzi.com/media/user_upload/pemdas-eb6b0f3f-24fc-4b2f-a80a-3383331a9abd.jpg)

[replit](https://replit.com/languages/python3)

## Presentación del proyecto

Un vistazo al proyecto de línea de comandos llamado Platzi Ventas la cual nos va a servir para manejar clientes, ventas, inventarios y generar algunos reportes.

Desde la línea de comandos podemos crear un archivo con `touch [nombre-del-archivo]`

Escribiremos el primer archivo `main.py`

___

Tienes dudas sobre porque es necesario poner

```
if __name__ == '__main__':
```

Esto es porque hay dos formas de ejecutar un programa de python, ejecutándolo directamente y ejecutándolo desde otro programa usando "import"
entonces cuando tu pones en otro código

```
import Mi_Programa.py
```

y tu no pusiste " if __name __ == ‘__main __’: " , se va a ejecutar el programa que hiciste en Mi_Programa.py y después de que termine de ejecutarse se ejecutara el código en el que importaste el programa.
Sin embargo si sí colocaste " if __name __ == ‘__main __’: " entonces todo lo que este dentro de ese bloque de código no se ejecutara y así podrás usar funciones especificas de ese programa sin que se ejecute el programa si no solo la función.
Para una explicación mas completa: https://es.stackoverflow.com/questions/32165/qué-es-if-name-main.

## Funciones

En el contexto de la programación las funciones son simplemente una agrupación de enunciados(**statments**) que tienen un nombre. Una función tiene un nombre, debe ser descriptivo, puede tener parámetros y puede regresar un valor después que se generó el cómputo.

Python es un lenguaje que se conoce como *batteries include*(baterías incluidas) esto significa que tiene una librería estándar con muchas funciones y librerías.

Para declarar funciones que no son las globales, las *built-in functions*, necesitamos importar un módulo.

Con el keyword `def` declaramos una función.

![Captura de pantalla de 2018-10-13 16-38-51.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%20de%202018-10-13%2016-38-51-f389bb3e-2a55-4763-88a9-d76fe7b919f0.jpg)

La estructura:

![img](https://swcarpentry.github.io/python-novice-inflammation/fig/python-function.svg)

En el contexto de la programación las funciones son una agrupación de enunciados que tienen un nombre.

- **`!important`**: Una de las cosas más importantes que hace un programador es poner nombres porque se pasa más tiempo leyendo código que escribiendo.
- El nombre de la función deberá ser descriptivo
- puede tener parámetros
- Puede regresar un valor después que se generó el cómputo (Lo que devuelve, después de realizar el computo de nuestra función)
- Se puede usar funciones con parámetros opcionales, que son inicializados con datos por defecto

Python tiene una librería de funciones estándar muy completa (**built in functions**), la cuál se puede encontras a continuación:
*
Python tiene **Battery included**, lo que significa que tiene una librería estándar con **muchas funciones** que podemos utilizar sin tener que instalar nada más conocidas como built-in functions.
*
A continuación la lista actualizada hasta hoy (pueden acceder a la documentación al hacer click):

## Built-in Functions

| [`abs()`](https://docs.python.org/3/library/functions.html#abs) | [`delattr()`](https://docs.python.org/3/library/functions.html#delattr) | [`hash()`](https://docs.python.org/3/library/functions.html#hash) | [`memoryview()`](https://docs.python.org/3/library/functions.html#func-memoryview) | [`set()`](https://docs.python.org/3/library/functions.html#func-set) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |                                                              |                                                              |                                                              |
| [`all()`](https://docs.python.org/3/library/functions.html#all) | [`dict()`](https://docs.python.org/3/library/functions.html#func-dict) | [`help()`](https://docs.python.org/3/library/functions.html#help) | [`min()`](https://docs.python.org/3/library/functions.html#min) | [`setattr()`](https://docs.python.org/3/library/functions.html#setattr) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |                                                              |                                                              |                                                              |
| [`any()`](https://docs.python.org/3/library/functions.html#any) | [`dir()`](https://docs.python.org/3/library/functions.html#dir) | [`hex()`](https://docs.python.org/3/library/functions.html#hex) | [`next()`](https://docs.python.org/3/library/functions.html#next) | [`slice()`](https://docs.python.org/3/library/functions.html#slice) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |                                                              |                                                              |                                                              |
| [`ascii()`](https://docs.python.org/3/library/functions.html#ascii) | [`divmod()`](https://docs.python.org/3/library/functions.html#divmod) | [`id()`](https://docs.python.org/3/library/functions.html#id) | [`object()`](https://docs.python.org/3/library/functions.html#object) | [`sorted()`](https://docs.python.org/3/library/functions.html#sorted) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |                                                              |                                                              |                                                              |
| [`bin()`](https://docs.python.org/3/library/functions.html#bin) | [`enumerate()`](https://docs.python.org/3/library/functions.html#enumerate) | [`input()`](https://docs.python.org/3/library/functions.html#input) | [`oct()`](https://docs.python.org/3/library/functions.html#oct) | [`staticmethod()`](https://docs.python.org/3/library/functions.html#staticmethod) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |                                                              |                                                              |                                                              |
| [`bool()`](https://docs.python.org/3/library/functions.html#bool) | [`eval()`](https://docs.python.org/3/library/functions.html#eval) | [`int()`](https://docs.python.org/3/library/functions.html#int) | [`open()`](https://docs.python.org/3/library/functions.html#open) | [`str()`](https://docs.python.org/3/library/functions.html#func-str) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |                                                              |                                                              |                                                              |
| [`breakpoint()`](https://docs.python.org/3/library/functions.html#breakpoint) | [`exec()`](https://docs.python.org/3/library/functions.html#exec) | [`isinstance()`](https://docs.python.org/3/library/functions.html#isinstance) | [`ord()`](https://docs.python.org/3/library/functions.html#ord) | [`sum()`](https://docs.python.org/3/library/functions.html#sum) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |                                                              |                                                              |                                                              |
| [`bytearray()`](https://docs.python.org/3/library/functions.html#func-bytearray) | [`filter()`](https://docs.python.org/3/library/functions.html#filter) | [`issubclass()`](https://docs.python.org/3/library/functions.html#issubclass) | [`pow()`](https://docs.python.org/3/library/functions.html#pow) | [`super()`](https://docs.python.org/3/library/functions.html#super) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |                                                              |                                                              |                                                              |
| [`bytes()`](https://docs.python.org/3/library/functions.html#func-bytes) | [`float()`](https://docs.python.org/3/library/functions.html#float) | [`iter()`](https://docs.python.org/3/library/functions.html#iter) | [`print()`](https://docs.python.org/3/library/functions.html#print) | [`tuple()`](https://docs.python.org/3/library/functions.html#func-tuple) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |                                                              |                                                              |                                                              |
| [`callable()`](https://docs.python.org/3/library/functions.html#callable) | [`format()`](https://docs.python.org/3/library/functions.html#format) | [`len()`](https://docs.python.org/3/library/functions.html#len) | [`property()`](https://docs.python.org/3/library/functions.html#property) | [`type()`](https://docs.python.org/3/library/functions.html#type) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |                                                              |                                                              |                                                              |
| [`chr()`](https://docs.python.org/3/library/functions.html#chr) | [`frozenset()`](https://docs.python.org/3/library/functions.html#func-frozenset) | [`list()`](https://docs.python.org/3/library/functions.html#func-list) | [`range()`](https://docs.python.org/3/library/functions.html#func-range) | [`vars()`](https://docs.python.org/3/library/functions.html#vars) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |                                                              |                                                              |                                                              |
| [`classmethod()`](https://docs.python.org/3/library/functions.html#classmethod) | [`getattr()`](https://docs.python.org/3/library/functions.html#getattr) | [`locals()`](https://docs.python.org/3/library/functions.html#locals) | [`repr()`](https://docs.python.org/3/library/functions.html#repr) | [`zip()`](https://docs.python.org/3/library/functions.html#zip) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |                                                              |                                                              |                                                              |
| [`compile()`](https://docs.python.org/3/library/functions.html#compile) | [`globals()`](https://docs.python.org/3/library/functions.html#globals) | [`map()`](https://docs.python.org/3/library/functions.html#map) | [`reversed()`](https://docs.python.org/3/library/functions.html#reversed) | [`__import__()`](https://docs.python.org/3/library/functions.html#__import__) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |                                                              |                                                              |                                                              |
| [`complex()`](https://docs.python.org/3/library/functions.html#complex) | [`hasattr()`](https://docs.python.org/3/library/functions.html#hasattr) | [`max()`](https://docs.python.org/3/library/functions.html#max) | [`round()`](https://docs.python.org/3/library/functions.html#round) |      |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ---- |
|                                                              |                                                              |                                                              |                                                              |      |

**Límites al declarar funciones:**

- Los nombres no pueden comenzar con dígitos
- No pueden utilizar una palabra reservada
- Las variables deben tener diferentes nombres
- Los nombres de las funciones deben ser descriptivas de lo que hacen las funciones, por convención palabras separadas con _

___

Muchas veces llamadas "funciones anónimas", las funciones lambda en Python, no son más que una forma de definir una función común y corriente, de una única instrucción de código, en una única línea. Es decir, una función lambda es la forma de definir una función que tradicionalmente podría escribirse de forma común, en una sola línea de código. Pero esto, solo podrá hacerse con aquellas funciones cuyo algoritmo, no posea más de una instrucción. La siguiente función:

```python
def mifunction(nombre):
    return "Hola %s!" % nombre
```

Con lambda, podría definirse en una sola línea de código, ya que posee una única instrucción. Para ello, se utilizaría la siguiente instrucción:

```python
mifuncion = lambda nombre: "Hola %s!" % nombre
```

Visto de esta forma, hasta se corre el riesgo de perderle el respeto a las funciones lambda. De hecho lo anterior, casi no requiere de una explicación. Sin embargo, la daré por si aún quedan dudas. Una función lambda es una forma de definir una función de una sola instrucción, en una sola línea de código, claro que, con una sintaxis particular.

**Funciones en python:**

* Las funciones son una agrupación de enunciados que tienen un nombre.
* Tienen un nombre(Descriptivo)
* Tienen parámetros(Lo que recibe)
* Puede regresar un valor(Lo que devuelve, después de realizar el computo de nuestra función)

> Pd:Python tiene (Battery included) = Python tiene una librería estándar con un sin fin de funciones que podemos utilizar conocidas como (BUILT-IN FUNCTIONS)

## Usando funciones en nuestro proyecto

Codigo final

```python
clients = 'pablo,ricardo,'


def create_client(client_name):
    global clients
    clients += client_name
    _add_comma()


def list_clients():
    global clients

    print(clients)


def _add_comma():
    global clients

    clients += ','


if __name__ == '__main__':
    list_clients()

    create_client('david')

    list_clients()
```

## Operadores lógicos

Para comprender el flujo de nuestro programa debemos entender un poco sobre estructuras y expresiones booleanas

`==` se refiere a igualdad
`!=` no hay igualdad.
`>` mayor que
`<` menor que
`>=` mayor o igual
`<=` menor o igual

`and` unicamente es verdadero cuando **ambos** valores son verdaderos
`or` es verdadero cuando **uno** de los dos valores es verdadero.
`not` es lo contrario al valor. Falso es Verdadero. Verdadero es Falso.

![Captura de pantalla de 2018-10-13 17-28-35.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%20de%202018-10-13%2017-28-35-3f3d67c2-659f-4eba-82d8-9d93bbff8886.jpg)

![Captura de Pantalla 2019-09-25 a la(s) 23.45.34.png](https://static.platzi.com/media/user_upload/Captura%20de%20Pantalla%202019-09-25%20a%20la%28s%29%2023.45.34-7ef8d148-e346-4578-a103-4bc1a17c1fe1.jpg)

> AND es verdadero solo si ambos son verdaderos
> OR es falso solo si ambos son falsos
> NOT pues este es lo contrario

## Estructuras condicionales

En esta clase seguiremos construyendo nuestro proyecto **PlatziVentas** haciéndolo un poco más interesante y conoceremos un poco sobre las Estructuras condicionales.

En Python es importante importante la **indentación**, de esa manera identifica donde empieza y termina un bloque de código sin necesidad de llaves **{}** como en otros lenguajes.

```python
clients = 'Pablo, Ricardo' #creamos el string

def _print_welcome(): #funcion para dar el mensaje de bienvenida
	print ('Welcome to Monares POS')
	print ('what would you like to do today?')
	print ('*' * 52)  #imprimimos el caracter "*" 52 veces
	print ('[C]reate client')
	print ('[D]elete client')

def create_client(client_name):
	global clients  #Utilizamos global para definir que la variable es la globarl, es decir la que definimos con pablo y ricardo
	clients_minusculas = clients.lower() #convertimos la cadena de texto en minusculas y la guardamos en otra variable para no diferenciar entre "Pablo" y "Pablo"
	if client_name not in clients_minusculas: #si el nombre del cliente no esta en clients_minusculas
		_add_coma() #ejecutamos la funcion _add_coma para agregar una coma y un espacio 
		clients += client_name  #adicionamos el nuevo string
	else:
		print('Client already exists') #Sino mostramos un mensaje al usuario
def _add_coma():  #el nombre de la función comienza con un guión bajo para establecer que será una funcion privada
	global clients
	clients +=", " #se agrega una coma y un espacio al string para separar los nuevos valores


def list_clients(): #función que muestra la lista de clientes
	global clients
	print (clients) #imprimimos el string clientes


if __name__ == '__main__': #funcion main
	_print_welcome() #ejecutamos la funcion que da el mensaje de bienvenida
	command = input().lower() #guardamos el valor del dato ingresado en la variable command pero lo convertimos a miunsculas para realizar la accion si presiona "C" o "c"
	if command == 'c': #si el comando es igual al caracter "c"
		client_name = input('What is the client name?')  #guardamos en la variable client_name el valor de los caracteres que ingresa el usuario hasta recibir un enter
		create_client(client_name) #ejecutamos la funcion crear cliente y enviamos como parametro el valor de la variable que almacena lo que digitó el usuario
		list_clients() #Ejecutamos la funcion listar clientes 
	elif command == 'd': #si el dato ignresado por el usuario es "d"
		pass
	else:
		print('Invalid command')
input() #Escribimos un input para que el programa haga una pausa y no cierre la ventana hasta recibir un enter
```



#  2. Uso de strings y ciclos

## Strings en Python

Los **strings** o **cadenas de textos** tienen un comportamiento distinto a otros tipos como los booleanos, enteros, floats. Las cadenas son secuencias de caracteres, todas se pueden acceder a través de un índice.

Podemos saber la longitud de un *string*, cuántos caracteres se encuentran en esa secuencia. Lo podemos saber con la ***built-in function\*** global llamada `len`.

Algo importante a tener en cuenta cuando hablamos de *strings* es que estos son inmutables, esto significa que cada vez que modificamos uno estamos generando un nuevo objeto en memoria.

El índice de la primera letra es 0, en la programación se empieza a contar desde 0

| Method                                                       | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [capitalize()](https://www.w3schools.com/python/ref_string_capitalize.asp) | Convierte el primer carácter en mayúsculas                   |
| [casefold()](https://www.w3schools.com/python/ref_string_casefold.asp) | Convierte una cadena en minúsculas                           |
| [center()](https://www.w3schools.com/python/ref_string_center.asp) | Devuelve una cadena centrada                                 |
| [count()](https://www.w3schools.com/python/ref_string_count.asp) | Devuelve el número de veces que un valor especificado se produce en una cadena |
| [encode()](https://www.w3schools.com/python/ref_string_encode.asp) | Devuelve una versión codificada de la cadena                 |
| [endswith()](https://www.w3schools.com/python/ref_string_endswith.asp) | Devuelve true si los extremos de cadena con el valor especificado |
| [expandtabs()](https://www.w3schools.com/python/ref_string_expandtabs.asp) | Establece el tamaño de la pestaña de la cadena               |
| [find()](https://www.w3schools.com/python/ref_string_find.asp) | Busca la cadena de un valor especificado y devuelve la posición de donde fue encontrado |
| [format()](https://www.w3schools.com/python/ref_string_format.asp) | Formatos especifican los valores de una serie                |
| [format_map()](https://www.programiz.com/python-programming/methods/string/format_map) | Formatos especifican los valores de una serie                |
| [index()](https://www.w3schools.com/python/ref_string_index.asp) | Busca la cadena de un valor especificado y devuelve la posición de donde fue encontrado |
| [isalnum()](https://www.w3schools.com/python/ref_string_isalnum.asp) | Devuelve verdadero si todos los caracteres de la cadena son alfanuméricos |
| [isalpha()](https://www.w3schools.com/python/ref_string_isalpha.asp) | Devuelve True si todos los caracteres de la cadena están en el alfabeto |
| [isdecimal()](https://www.w3schools.com/python/ref_string_isdecimal.asp) | Devuelve True si todos los caracteres de la cadena son decimales |
| [isdigit()](https://www.w3schools.com/python/ref_string_isdigit.asp) | Devuelve verdadero si todos los caracteres de la cadena son dígitos |
| [isidentifier()](https://www.w3schools.com/python/ref_string_isidentifier.asp) | Devuelve True si la cadena es un identificador               |
| [islower()](https://www.w3schools.com/python/ref_string_islower.asp) | Devuelve verdadero si todos los caracteres de la cadena son minúsculas |
| [isnumeric()](https://www.w3schools.com/python/ref_string_isnumeric.asp) | Devuelve verdadero si todos los caracteres de la cadena son numéricos |
| [isprintable()](https://www.w3schools.com/python/ref_string_isprintable.asp) | Devuelve verdadero si todos los caracteres de la cadena son imprimibles |
| [isspace()](https://www.w3schools.com/python/ref_string_isspace.asp) | Devuelve True si todos los caracteres de la cadena son espacios en blanco |
| [istitle()](https://www.w3schools.com/python/ref_string_istitle.asp) | Devuelve True si la cadena sigue las reglas de un título     |
| [isupper()](https://www.w3schools.com/python/ref_string_isupper.asp) | Devuelve True si todos los caracteres de la cadena son mayúsculas |
| [join()](https://www.w3schools.com/python/ref_string_join.asp) | Se une a los elementos de un iterable al final de la cadena  |
| [ljust()](https://www.w3schools.com/python/ref_string_ljust.asp) | Devuelve una versión justificada izquierda de la cadena      |
| [lower()](https://www.w3schools.com/python/ref_string_lower.asp) | Convierte una cadena en minúsculas                           |
| [lstrip()](https://www.w3schools.com/python/ref_string_lstrip.asp) | Devuelve una versión de ajuste izquierdo de la cuerda        |
| [maketrans()](https://www.programiz.com/python-programming/methods/string/maketrans) | Devuelve una tabla de traducción para ser utilizado en traducciones |
| [partition()](https://www.w3schools.com/python/ref_string_partition.asp) | Devuelve una tupla donde la cadena se separó en tres partes  |
| [replace()](https://www.w3schools.com/python/ref_string_replace.asp) | Devuelve una serie en un valor especificado es reemplazado con un valor especificado |
| [rfind()](https://www.w3schools.com/python/ref_string_rfind.asp) | Busca la cadena de un valor especificado y devuelve la última posición de donde fue encontrado |
| [rindex()](https://www.w3schools.com/python/ref_string_rindex.asp) | Busca la cadena de un valor especificado y devuelve la última posición de donde fue encontrado |
| [rjust()](https://www.w3schools.com/python/ref_string_rjust.asp) | Devuelve una versión justificada derecha de la cadena        |
| [rpartition()](https://www.w3schools.com/python/ref_string_rpartition.asp) | Devuelve una tupla donde la cadena se separó en tres partes  |
| [rsplit()](https://www.w3schools.com/python/ref_string_rsplit.asp) | Divide la cadena en el separador especificado y devuelve una lista |
| [rstrip()](https://www.w3schools.com/python/ref_string_rstrip.asp) | Devuelve una versión ajuste correcto de la cadena            |
| [split()](https://www.w3schools.com/python/ref_string_split.asp) | Divide la cadena en el separador especificado y devuelve una lista |
| [splitlines()](https://www.w3schools.com/python/ref_string_splitlines.asp) | Divide la cadena en los saltos de línea y devuelve una lista |
| [startswith()](https://www.w3schools.com/python/ref_string_startswith.asp) | Devuelve true si la cadena comienza con el valor especificado |
| [strip()](https://www.w3schools.com/python/ref_string_strip.asp) | Devuelve una versión recortada de la cadena                  |
| [swapcase()](https://www.w3schools.com/python/ref_string_swapcase.asp) | Permutas de los casos, se convierte en minúsculas mayúsculas y viceversa |
| [title()](https://www.w3schools.com/python/ref_string_title.asp) | Convierte el primer carácter de cada palabra en mayúsculas   |
| [translate()](https://www.tutorialspoint.com/python/string_translate.htm) | Devuelve una cadena traducida                                |
| [upper()](https://www.w3schools.com/python/ref_string_upper.asp) | Convierte una cadena en mayúsculas                           |
| [zfill()](https://www.w3schools.com/python/ref_string_zfill.asp) | Rellena la cadena con un número determinado de valores de 0 a principios |

### ⛓ Strings en Python 🐍

Excelente manera de iniciar con el tema de los Strings que me parece es muy importante conocer la manera de manipularlos pues. Conocer los métodos que se pueden aplicar te ayudará a ahorrarte mucho tiempo.

![Captura de Pantalla 2021-01-16 a la(s) 11.36.37.png](https://static.platzi.com/media/user_upload/Captura%20de%20Pantalla%202021-01-16%20a%20la%28s%29%2011.36.37-489335ae-cdcd-4f22-ad0c-7788bc744913.jpg)

![Captura de Pantalla 2021-01-16 a la(s) 11.36.24.png](https://static.platzi.com/media/user_upload/Captura%20de%20Pantalla%202021-01-16%20a%20la%28s%29%2011.36.24-28c4332b-6c1a-4c91-9214-727c25f9d9f7.jpg)

## Operaciones con Strings en Python

Los strings tienen varios métodos que nosotros podemos utilizar.

- `upper`: convierte todo el **string** a mayúsculas
- `lower`: convierte todo el **string** a minúsculas
- `find`: encuentra el indice en donde existe un patrón que nosotros definimos
- `startswith`: significa que empieza con algún patrón.
- `endswith`: significa que termina con algún patrón
- `capitalize`: coloca la primera letra en mayúscula y el resto en minúscula

`in` y `not in` nos permite saber con cualquier secuencia sin una subsecuencia o substrings se encuentra adentro de la secuencia mayor.

`dir`: Nos dice todos los métodos que podemos utilizar dentro de un objeto.
`help`: nos imprime en pantalla el *docstrings* o comentario de ayuda o instrucciones que posee la función. Casi todas las funciones en Python las tienen.

Dunder methods: También conocidos como 

- **métodos mágicos** (magic métodos) o 
- **special methods** (métodos especiales).
  https://docs.python.org/3/reference/datamodel.html#special-method-names

MÉTODOS DE CADENAS

- METODO CENTRAR
  La funcion retorna una ‘cadena’ con rellenado con una cantidad especifica de caractéres

  POR EJEMPLO
  `nombre='Alexander' nombre.center(20,'*')`
  `output = *****alexander******`

- METODO COUNT
  El metodo count() devuelve el numero de coincidencias de una subcadena que contiene la cadena
  POR EJEMPLO:

```python
	nombre = 'alexander'
	nombre.count('a')		=> 2
	nombre.count('a',1,-1) 	=> 1
```

- MÉTODO CAPITALIZE
  Devuelve una copia de la cadena con el primer caracter en mayuscula.
  POR EJEMPLO :

  ```python
  nombre="alexANder"
  nombre.capitalize() => "Alexander"
  ```

- MÉTODO ENDSWITH
  Devuelve verdadero si la cadena finaliza en el sufijo especificado,en caso contrario falso.
  POR EJEMPLO:

  ```python
  nombre='Alexander'
  nombre.endswith('DER') => false
  nombre.endswith('der') => true
  ```

- MÉTODO FIND
  Devuelve el menor índice de la cadena para que el sub se encuentre, también recibe 2 argumentos opcionales de inicio y fin. Nos devuelve -1 si no se halla el substring.
  POR EJEMPLO

  ```python
  nombre = 'Alexander'
  nombre.find('exa')		=> 2
  nombre.find('exa',5)	=> -1
  ```

- MÉTODO ISALNUM
  devuelve verdadero si todos los caracteres son alfanuméricos y hay al menos un caracter.
  POR EJEMPLO:

  ```python
  nombre = "Alexander"
  nombre.isalnum() 		=> true
  prueba = ""			
  prueba.isalnum() 		=> false
  test 	="alexander@@@"
  test.isalnum() 			=> false
  ```

  - MÉTODO ISALPHA()
  	devuelve verdadero si todos los caracteres son alfabéticos.
  	POR EJEMPLO:

```python
	nombre="alexander"
	nombre.isalpha() 		=>true
	nombre="alexander745"
	nombre.isalpha() 		=>false
```

- MÉTODO ISDIGIT
	devuelve verdadero si todos los caracteres son dígitos.
	por ejemplo:
	
- ```python
  codigo="4785"
  	codigo.isdigit() 		=> true
  ```

  #### Lista completa de operadores de Strings

  [Ver artículo original](https://www.w3schools.com/python/python_ref_string.asp)

  | Method                                                       | Description                                                  |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | [capitalize()](https://www.w3schools.com/python/ref_string_capitalize.asp) | Convierte el primer carácter en mayúsculas                   |
  | [casefold()](https://www.w3schools.com/python/ref_string_casefold.asp) | Convierte una cadena en minúsculas                           |
  | [center()](https://www.w3schools.com/python/ref_string_center.asp) | Devuelve una cadena centrada                                 |
  | [count()](https://www.w3schools.com/python/ref_string_count.asp) | Devuelve el número de veces que un valor especificado se produce en una cadena |
  | [encode()](https://www.w3schools.com/python/ref_string_encode.asp) | Devuelve una versión codificada de la cadena                 |
  | [endswith()](https://www.w3schools.com/python/ref_string_endswith.asp) | Devuelve true si los extremos de cadena con el valor especificado |
  | [expandtabs()](https://www.w3schools.com/python/ref_string_expandtabs.asp) | Establece el tamaño de la pestaña de la cadena               |
  | [find()](https://www.w3schools.com/python/ref_string_find.asp) | Busca la cadena de un valor especificado y devuelve la posición de donde fue encontrado |
  | [format()](https://www.w3schools.com/python/ref_string_format.asp) | Formatos especifican los valores de una serie                |
  | [format_map()](https://www.programiz.com/python-programming/methods/string/format_map) | Formatos especifican los valores de una serie                |
  | [index()](https://www.w3schools.com/python/ref_string_index.asp) | Busca la cadena de un valor especificado y devuelve la posición de donde fue encontrado |
  | [isalnum()](https://www.w3schools.com/python/ref_string_isalnum.asp) | Devuelve verdadero si todos los caracteres de la cadena son alfanuméricos |
  | [isalpha()](https://www.w3schools.com/python/ref_string_isalpha.asp) | Devuelve True si todos los caracteres de la cadena están en el alfabeto |
  | [isdecimal()](https://www.w3schools.com/python/ref_string_isdecimal.asp) | Devuelve True si todos los caracteres de la cadena son decimales |
  | [isdigit()](https://www.w3schools.com/python/ref_string_isdigit.asp) | Devuelve verdadero si todos los caracteres de la cadena son dígitos |
  | [isidentifier()](https://www.w3schools.com/python/ref_string_isidentifier.asp) | Devuelve True si la cadena es un identificador               |
  | [islower()](https://www.w3schools.com/python/ref_string_islower.asp) | Devuelve verdadero si todos los caracteres de la cadena son minúsculas |
  | [isnumeric()](https://www.w3schools.com/python/ref_string_isnumeric.asp) | Devuelve verdadero si todos los caracteres de la cadena son numéricos |
  | [isprintable()](https://www.w3schools.com/python/ref_string_isprintable.asp) | Devuelve verdadero si todos los caracteres de la cadena son imprimibles |
  | [isspace()](https://www.w3schools.com/python/ref_string_isspace.asp) | Devuelve True si todos los caracteres de la cadena son espacios en blanco |
  | [istitle()](https://www.w3schools.com/python/ref_string_istitle.asp) | Devuelve True si la cadena sigue las reglas de un título     |
  | [isupper()](https://www.w3schools.com/python/ref_string_isupper.asp) | Devuelve True si todos los caracteres de la cadena son mayúsculas |
  | [join()](https://www.w3schools.com/python/ref_string_join.asp) | Se une a los elementos de un iterable al final de la cadena  |
  | [ljust()](https://www.w3schools.com/python/ref_string_ljust.asp) | Devuelve una versión justificada izquierda de la cadena      |
  | [lower()](https://www.w3schools.com/python/ref_string_lower.asp) | Convierte una cadena en minúsculas                           |
  | [lstrip()](https://www.w3schools.com/python/ref_string_lstrip.asp) | Devuelve una versión de ajuste izquierdo de la cuerda        |
  | [maketrans()](https://www.programiz.com/python-programming/methods/string/maketrans) | Devuelve una tabla de traducción para ser utilizado en traducciones |
  | [partition()](https://www.w3schools.com/python/ref_string_partition.asp) | Devuelve una tupla donde la cadena se separó en tres partes  |
  | [replace()](https://www.w3schools.com/python/ref_string_replace.asp) | Devuelve una serie en un valor especificado es reemplazado con un valor especificado |
  | [rfind()](https://www.w3schools.com/python/ref_string_rfind.asp) | Busca la cadena de un valor especificado y devuelve la última posición de donde fue encontrado |
  | [rindex()](https://www.w3schools.com/python/ref_string_rindex.asp) | Busca la cadena de un valor especificado y devuelve la última posición de donde fue encontrado |
  | [rjust()](https://www.w3schools.com/python/ref_string_rjust.asp) | Devuelve una versión justificada derecha de la cadena        |
  | [rpartition()](https://www.w3schools.com/python/ref_string_rpartition.asp) | Devuelve una tupla donde la cadena se separó en tres partes  |
  | [rsplit()](https://www.w3schools.com/python/ref_string_rsplit.asp) | Divide la cadena en el separador especificado y devuelve una lista |
  | [rstrip()](https://www.w3schools.com/python/ref_string_rstrip.asp) | Devuelve una versión ajuste correcto de la cadena            |
  | [split()](https://www.w3schools.com/python/ref_string_split.asp) | Divide la cadena en el separador especificado y devuelve una lista |
  | [splitlines()](https://www.w3schools.com/python/ref_string_splitlines.asp) | Divide la cadena en los saltos de línea y devuelve una lista |
  | [startswith()](https://www.w3schools.com/python/ref_string_startswith.asp) | Devuelve true si la cadena comienza con el valor especificado |
  | [strip()](https://www.w3schools.com/python/ref_string_strip.asp) | Devuelve una versión recortada de la cadena                  |
  | [swapcase()](https://www.w3schools.com/python/ref_string_swapcase.asp) | Permutas de los casos, se convierte en minúsculas mayúsculas y viceversa |
  | [title()](https://www.w3schools.com/python/ref_string_title.asp) | Convierte el primer carácter de cada palabra en mayúsculas   |
  | [translate()](https://www.tutorialspoint.com/python/string_translate.htm) | Devuelve una cadena traducida                                |
  | [upper()](https://www.w3schools.com/python/ref_string_upper.asp) | Convierte una cadena en mayúsculas                           |
  | [zfill()](https://www.w3schools.com/python/ref_string_zfill.asp) | Rellena la cadena con un número determinado de valores de 0 a principios |

## Operaciones con strings y el comando Update

`main.py` Update clients

```python
def update_client(client_name, update_client_name):
  global clients

  if client_name in clients:
    clients = clients.replace(client_name, update_client_name)
  else:
    print('Client is not in clients list')
```

```python
  print('[U]pdate client')
```

```python
if __name__ == '__main__':
  _print_welcome()

  command = input()
  command = command.upper()

  if command == 'C':
    client_name = _get_client_name()
    create_client(client_name)
    list_clients()
  elif command == 'D':
    pass
  elif command == 'U':
    client_name = _get_client_name()
    update_client_name = input('What is the updated client name? ')
    update_client(client_name, update_client_name)
    list_clients()
  else:
    print('Invalid command')
```

## Operaciones con strings y el comando Delete

CRUD

```python
(C)reate
(R)ead or retreive
(U)pdate
(D)elete
```

Es un modelo muy utilizado por científicos de computadores. Se usa mucho en aplicaciones web, para construir modelos usables.

```python
def _client_not_found():
	return print('Client is not in clients list')
```

 😎👍

```python
#proyecto_final
clients = 'Rogelio, Paco, Sebastian, Adrian' #creamos el string con los nombres de los clientes

def _print_welcome(): #funcion para dar el mensaje de bienvenida
	print ('Welcome to Monares POS')
	print ('what would you like to do today?')
	print ('*' * 52)  #imprimimos el caracter "*" 52 veces
	#Mostramos el menu al usuario
	print ('[C]reate client')
	print ('[R]ead client\'s list ')
	print ('[U]pdate client')
	print ('[D]elete client')


def verify_client(client_name):
	"""
	Esta funcion sirve para verificar si un cliente se encuentra en el string o no.
	Recibe un parametro, el nombre del cliente y devuelve un valor positivo si el nombre
	se encuentra en el string y viceversa
	"""
	global clients
	if client_name in clients:
		return True
	else:
		return False


def _client_not_found_tollgate():
	"""
	Esta funcion sirve para mostrar un mensaje preestablecido si un cliente no se encuentra 
	en el string de los clientes
	"""
	print ("Client is not in client\'s list")


def create_client(client_name):
	"""
	Esta función sirve para agregar un cliente al string de la lista
	"""
	global clients  #Utilizamos global para definir que la variable es la global, es decir la que definimos con pablo y ricardo
	if (verify_client(client_name)): #Ejecutamos la funcion verify client, si la cadena del nombre del cliente está en el string etonces...
		print('Client already exists') #Mostramos un mensaje al usuario
	else:
		_add_coma() #Si no, ejecutamos la funcion _add_coma para agregar una coma y un espacio al último nombre
		clients += client_name #adicionamos el nuevo string pero con la funcion capitalize para poner el primer caractér en mayuscula y seguir con el formato


def read_client_list(): 
	"""
	función que imprime la lista de clientes
	"""
	global clients
	print (clients) #imprimimos el string clientes, es decir la lista de clientes


def update_client(client_name):
	"""
	Función que sirve para actualizar el nombre de un cliente, 
	recibe un parametro que es el string del nombre del cliente a editar
	"""
	global clients #Utilizamos global para definir que la variable es la globarl, es decir la que definimos con pablo y ricardo
	if (verify_client(client_name)): #Si el cliente se encuentra registrado entonces...
		updated_client_name = input('What is the updated client name?').capitalize() #Pedimos al usuario que ingrese el nuevo nombre de cliente
		clients = clients.replace(client_name, updated_client_name) #Ejecutamos la funcion replace, la cual recibe dos parametros, el string a reemplazar
		#y el string con el cual se remplazara al anterior
	else:
		_client_not_found_tollgate() #sino, ejecutamos la funcion _client_not_found_tollgate() para mostrar el mensaje que no se ha encontrado el cliente (string)
		

def delete_client(client_name):
	"""
	Función para borrar el cliente (Remplazar por una cadena vacía)
	"""
	global clients
	if (verify_client(client_name)): #Si el cliente está registrado, entonces...
		clients = clients.replace(client_name + ', ', '') #Remplazamos el nombre concatenado con una , y un espacio y lo remplazamos por una cadena vacía
	else:
		_client_not_found_tollgate() #sino, ejecutamos la funcion _client_not_found_tollgate() para mostrar el mensaje que no se ha encontrado el cliente (string)


def _add_coma():  #el nombre de la función comienza con un guión bajo para establecer que será una funcion privada
	"""
	Función que sirve para agregar una coma y un espacio para separar los substrings
	"""
	global clients
	clients +=", " #se agrega una coma y un espacio al string para separar los nuevos valores


def _get_client_name():
	"""
	Función que sierve para obtener el nombre del cliente con el formato capitalizado
	"""
	return input('What is the client name?').capitalize()  #guardamos en la variable client_name el valor de los caracteres que ingresa el usuario hasta recibir un enter


if __name__ == '__main__': #funcion main
	_print_welcome() #ejecutamos la funcion que da el mensaje de bienvenida
	command = input().lower() #guardamos el valor del dato ingresado en la variable command pero lo convertimos a miunsculas para realizar la accion si presiona "C" o "c"
	if command == 'c': #si el comando es igual al caracter "c" procedemos a realizar los pasos de create/crear
		client_name = _get_client_name() #Ejecutamos la función para obtener el nombre del cliente
		create_client(client_name) #Ejecutamos la funcion crear cliente y enviamos como parametro el valor de la variable que almacena lo que digitó el usuario
		read_client_list() #Ejecutamos la funcion listar clientes 
	elif command == 'r': #si el comando es igual al caracter "r" procedemos a realizar los pasos de read/leer
		read_client_list()  #Ejecutamos la funcion listar clientes 
	elif command == 'u': #si el comando es igual al caracter "u", procedemos a realizar los pasos de update/actualizar
		client_name = _get_client_name() #Ejecutamos la función para obtener el nombre del cliente
		update_client(client_name)
		read_client_list() #Ejecutamos la funcion listar clientes 
	elif command == 'd': #si el dato ignresado por el usuario es "d" procedemos a realizar los pasos de delete/eliminar
		client_name = _get_client_name() #Ejecutamos la función para obtener el nombre del cliente
		delete_client(client_name)
		read_client_list() #Ejecutamos la funcion listar clientes 
	else:
		print('ERROR: Invalid command') #Si el usuario no digita alguna de nuestras opciones entonces mostramos un mensaje de error
input() #Escribimos un input para que el programa haga una pausa y no cierre la ventana hasta recibir un enter
```

## Operaciones con strings: Slices en python

Los *slices* en Python nos permiten manejar secuencias de una manera poderosa.

**Slices** en español significa ““rebanada””, si tenemos una secuencia de elementos y queremos una rebanada tenemos una sintaxis para definir qué pedazos queremos de esa secuencia.

```python
secuencia[comienzo:final:pasos]
```

**Slices python**

```python
>>> Fruit = 'banana'
>>> Fruit[3:]
'ana'
>>> Fruit[3:3]
''
>>> Fruit[:]
'banana'
>>> Fruit[1:-1]
'anan'
>>> Fruit[1:-1:2]
'aa'
```

Los **slices** en Python nos permiten manejar secuencias de una manera poderosa.

**Slices** en español significa “rebanada”, si tenemos una secuencia de elementos y queremos una rebanada tenemos una sintaxis para definir qué pedazos queremos de esa secuencia.

```python
secuencia[comienzo:final:pasos]
```

### Ejemplos:

```python
string = "banana"

b a n a n a
0 1 2 3 4 5
```

------

Start in 1 and End in 3

```python
string[1:3] # an
```

------

Start in 3 and End in N

```python
string[3:] # ana
```

------

Start in 3 and End in 3

```python
string[3:3] # No exist data or steps
```

------

Start in 0 and End in N

```python
string[:] # banana
```

------

Start in 1 and End in the last one word and make a step in two

```python
string[1:-1:2]

banana # Word
anan # 1:-1
aa # 2

Result: aa
```

------

Start in 1 and End in 4

```python
string[1:4] # ana
```

------

Start in 4 and End in 8

```python
string[4:8] # na 
# No da error porque dice: Si no hay más indices muestre hasta el último posible
```

------

Reverse the string

```python
string[::-1] # Start to end and the steps is the N to 0 (reverse) # ananab
```

------

Start in 0 and End in 8 by Step 3

```python
string[:8:3] # ba

# Process
b a n a n a
0 1 2 3 4 5

0...8
banana

b..a..
ba
```

------

Start in 0 and End in N by Step 2

```python
string[::2] # bnn

# Process
b a n a n a
0 1 2 3 4 5

0....n

b.n.n.
bnn
```

<h4>Word Most Longer (Spanish)</h4>

**arroz (Comienza con la A y termina en la Z)**

La palabra más grande es el nombre completo de la proteina llamada titin (189,819 Characters)
https://fullformdirectory.in/titin.html

## For loops

Las iteraciones es uno de los conceptos **más importantes** en la programación. En **Python** existen muchas manera de iterar pero las dos principales son los **for loops** y **while loops**.

Los **for loops** nos permiten iterar a través de una secuencia y los while loops nos permiten iterara hasta cuando una condición se vuelva falsa.

**for loops**:

- Tienen dos keywords `break` y `continue` que nos permiten salir anticipadamente de la iteración
- Se usan cuando se quiere ejecutar varias veces una o varias instrucciones.
- for [variable] in [secuencia]:

Es una convención usar la letra `i` como variable en nuestro for, pero podemos colocar la que queramos.

`range`: Nos da un objeto rango, es un iterador sobre el cual podemos generar secuencias.

 😎👍

```python
#proyecto_final
clients = 'Rogelio, Paco, Sebastian, Adrian' #creamos el string con los nombres de los clientes

def _print_welcome(): #funcion para dar el mensaje de bienvenida
	print ('Welcome to Monares POS')
	print ('what would you like to do today?')
	print ('*' * 52)  #imprimimos el caracter "*" 52 veces
	#Mostramos el menu al usuario
	print ('[C]reate client')
	print ('[R]ead client\'s list ')
	print ('[U]pdate client')
	print ('[D]elete client')
	print ('[S]earch client')


def verify_client(client_name):
	"""
	Esta funcion sirve para verificar si un cliente se encuentra en el string o no.
	Recibe un parametro, el nombre del cliente y devuelve un valor positivo si el nombre
	se encuentra en el string y viceversa
	"""
	global clients
	if client_name in clients:
		return True
	else:
		return False


def _client_not_found_tollgate():
	"""
	Esta funcion sirve para mostrar un mensaje preestablecido si un cliente no se encuentra 
	en el string de los clientes
	"""
	print ("Client is not in client\'s list")


def create_client(client_name):
	"""
	Esta función sirve para agregar un cliente al string de la lista
	"""
	global clients  #Utilizamos global para definir que la variable es la global, es decir la que definimos con pablo y ricardo
	if (verify_client(client_name)): #Ejecutamos la funcion verify client, si la cadena del nombre del cliente está en el string etonces...
		print('Client already exists') #Mostramos un mensaje al usuario
	else:
		_add_coma() #Si no, ejecutamos la funcion _add_coma para agregar una coma y un espacio al último nombre
		clients += client_name #adicionamos el nuevo string pero con la funcion capitalize para poner el primer caractér en mayuscula y seguir con el formato


def read_client_list(): 
	"""
	función que imprime la lista de clientes
	"""
	global clients
	print (clients) #imprimimos el string clientes, es decir la lista de clientes


def update_client(client_name):
	"""
	Función que sirve para actualizar el nombre de un cliente, 
	recibe un parametro que es el string del nombre del cliente a editar
	"""
	global clients #Utilizamos global para definir que la variable es la globarl, es decir la que definimos con pablo y ricardo
	if (verify_client(client_name)): #Si el cliente se encuentra registrado entonces...
		updated_client_name = input('What is the updated client name?').capitalize() #Pedimos al usuario que ingrese el nuevo nombre de cliente
		clients = clients.replace(client_name, updated_client_name) #Ejecutamos la funcion replace, la cual recibe dos parametros, el string a reemplazar
		#y el string con el cual se remplazara al anterior
	else:
		_client_not_found_tollgate() #sino, ejecutamos la funcion _client_not_found_tollgate() para mostrar el mensaje que no se ha encontrado el cliente (string)
		

def delete_client(client_name):
	"""
	Función para borrar el cliente (Remplazar por una cadena vacía)
	"""
	global clients
	if (verify_client(client_name)): #Si el cliente está registrado, entonces...
		clients = clients.replace(client_name + ', ', '') #Remplazamos el nombre concatenado con una , y un espacio y lo remplazamos por una cadena vacía
	else:
		_client_not_found_tollgate() #sino, ejecutamos la funcion _client_not_found_tollgate() para mostrar el mensaje que no se ha encontrado el cliente (string)


def search_client(client_name): 
	"""
	Función para buscar a un cliente
	"""
	global clients
	clients_csv = clients.split(', ')  #CSV: comma-separated values | La función split separa los valores cuando encuentra el caracter/string que recibe como parámetro
	for client in clients_csv: #Es como el foreach | Por cada nueva_variable (que usaremos internamente) que esté en CSV(por ejemplo)
		if client != client_name: #Se ejecuta una comparación, si el cliente no coincide entonces...
			continue #continue, fuerza a la siguiente iteración que suceda
		else:
			return True #caso contrario retornamos un valor true


def _add_coma():  #el nombre de la función comienza con un guión bajo para establecer que será una funcion privada
	"""
	Función que sirve para agregar una coma y un espacio para separar los substrings
	"""
	global clients
	clients +=", " #se agrega una coma y un espacio al string para separar los nuevos valores


def _get_client_name():
	"""
	Función que sierve para obtener el nombre del cliente con el formato capitalizado
	"""
	return input('What is the client name?').capitalize()  #guardamos en la variable client_name el valor de los caracteres que ingresa el usuario hasta recibir un enter


if __name__ == '__main__': #funcion main
	_print_welcome() #ejecutamos la funcion que da el mensaje de bienvenida
	command = input().lower() #guardamos el valor del dato ingresado en la variable command pero lo convertimos a miunsculas para realizar la accion si presiona "C" o "c"
	if command == 'c': #si el comando es igual al caracter "c" procedemos a realizar los pasos de create/crear
		client_name = _get_client_name() #Ejecutamos la función para obtener el nombre del cliente
		create_client(client_name) #Ejecutamos la funcion crear cliente y enviamos como parametro el valor de la variable que almacena lo que digitó el usuario
		read_client_list() #Ejecutamos la funcion listar clientes 
	elif command == 'r': #si el comando es igual al caracter "r" procedemos a realizar los pasos de read/leer
		read_client_list()  #Ejecutamos la funcion listar clientes 
	elif command == 'u': #si el comando es igual al caracter "u", procedemos a realizar los pasos de update/actualizar
		client_name = _get_client_name() #Ejecutamos la función para obtener el nombre del cliente
		update_client(client_name)
		read_client_list() #Ejecutamos la funcion listar clientes 
	elif command == 'd': #si el dato ingresado por el usuario es "d" procedemos a realizar los pasos de delete/eliminar
		client_name = _get_client_name() #Ejecutamos la función para obtener el nombre del cliente
		delete_client(client_name)
		read_client_list() #Ejecutamos la funcion listar clientes 
	elif command == 's': ##si el dato ingresado por el usuario es "s" procedemos a buscar al cliente con el nombre ingresado
		client_name = _get_client_name() #Ejecutamos la función para obtener el nombre del cliente
		found = search_client(client_name) #Guardamos en la variable found lo que retrona la función search_client
		if found:
			print (f'The client: {client_name}, has been found') #si la función devuelve un true mostramos el mensaje que se ha encontrado el cliente
		else:
			print (f'The client: {client_name}, has not been found')#viceversa
	else:
		print('ERROR: Invalid command') #Si el usuario no digita alguna de nuestras opciones entonces mostramos un mensaje de error
input() #Escribimos un input para que el programa haga una pausa y no cierre la ventana hasta recibir un enter
```

Pasa de 🐍 ➡️ 🐉 con `for, while y generadores`

Los `loops` es de lo más utilizado en la programación por lo que es muy importante aprenderlo bien y sacarle provecho. Les dejo diferentes maneras de aplicar un `for loop`. **No te quedes con lo básico y domina los loops mirando el video increíble de una Pycon.**

1.- **Sintaxis fundamental de un loop `for`**:

```python
for valor in secuencia:
	Body of for
```

2.- **Utilizando un rango con `range()`**:

```python
# generando un rango con valores de 0 al número indicado
	for i in range(10):	# genera una secuencia 0 - 9
		pass

# estableciendo los valores de la secuencia en range()
	range(start, stop, step_size)

print(range(10))
	# range(0, 10)

print(list(range(10)))
	# [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

print(list(range(2, 8)))
	# [2, 3, 4, 5, 6, 7]

print(list(range(2, 20, 3)))
	# [2, 5, 8, 11, 14, 17]
```

3.- **Combinando `range()` con `len()`para obtener la longitud de una lista de elementos**:

```python
genre = ['pop', 'rock', 'jazz']

for i in range(len(genre)):
    print(i)
	# 0
	# 1
	# 2
```

4.- **Generando listas, combinando strings con ‘for loops:’**

```python
genre = ['pop', 'rock', 'jazz']

# iterate over the list using index
for i in genre:
    print('I like', i)

# Resultado: 
I like pop
I like rock
I like jazz
```

## While loops

Al igual que las for loops, las **while loops** nos sirve para iterar, pero las for loops nos sirve para iterar a lo largo de una secuencia mientras que las **while loops** nos sirve para iterar mientras una condición sea verdadera.

Si no tenemos un mecanismo para convertir el mecanismo en falsedad, entonces nuestro while loops se ira al infinito(infinite loop).

Código actualizado:

```python
import sys

clients = 'luisa, juan,'

def create_client(client_name):
	global clients

	if client_name not in clients :
		clients += client_name
		_add_comma()
	else:
		print ('Client already is in the client\'s list')

def list_clients():
	global clients

	print(clients)


def updated_client(client_name, updated_client_name):
	global clients

	if client_name in clients :
		clients = clients.replace (client_name + ',', updated_client_name + ',' )
	else:
		print ('Client is no it client\'s list')


def delete_client(client_name):
	global clients

	if client_name in clients:
		clients = clients.replace(client_name + ',', '')
	else:
		print('Client is not in client\'s list')

def search_client(client_name):
	clients_list = clients.split(',')

	for client in clients_list:
		if client != client_name:
			continue
		else:
			return True

def _add_comma():
	global clients

	clients += ','


def _print_welcome():
	print ('WELCOME TO PLATZI VENTAS')
	print ('*' * 50)
	print ('What would you like to do today ? ')
	print ('[C]reate client')
	print ('[L]ist client')
	print ('[U]pdated client')
	print ('[D]elete client')
	print ('[S]earch client')


def _get_client_name ():
	client_name = None

	while not client_name:
		client_name = str.lower(input('What is the client name ? '))

		if client_name == 'exit':
			client_name = None
			break

	if not client_name:
			sys.exit()

	return client_name

if __name__ == '__main__':
	_print_welcome()

	command = input()
	command = command.upper()

	if command == 'C':
		client_name = _get_client_name()
		create_client(client_name)
		list_clients()
	elif command == 'L':
		list_clients()
	elif command == 'U':
		client_name = _get_client_name()
		updated_client_name = str.lower(input ('What is the updated client name ? '))
		updated_client(client_name, updated_client_name)
		list_clients()
	elif command == 'D':
		client_name = _get_client_name()
		delete_client(client_name)
		list_clients()
	elif command == 'S':
		client_name = _get_client_name()
		found = search_client(client_name)

		if found:
			print('The client is in the client\'s list')
		else:
			print('The client: {} is not in our client\'s list'.format(client_name))

	else:
		print ('Invalid command')
```

Repite un statement hasta que la condición que le estamos pasando sea falsa, debemos tener cuidado con los loops infinitos.

### Stack Overflow

Aparte de ser una de las comunidades más grandes de developers y dónde puedes encontrar muchas respuestas y ayudas para tu código, es cuando un programa está intentando utilizar más memoria de la que tiene disponible.

Recuerden que el return es el fin de la función por lo tanto si agregan cualquier cosa dentro de la función después del return el programa no funcionará.

## Iterators and generators

Aunque no lo sepas, probablemente ya utilices iterators en tu vida diaria como programador de Python. Un iterator es simplemente un objeto que cumple con los requisitos del Iteration Protocol (protocolo de iteración) y por lo tanto puede ser utilizado en ciclos. Por ejemplo,

```python
for i in range(10):
    print(i)
```

En este caso, la función range es un iterable que regresa un nuevo valor en cada ciclo. Para crear un objeto que sea un iterable, y por lo tanto, implemente el protocolo de iteración, debemos hacer tres cosas:

- Crear una clase que implemente los métodos **iter** y **next**
- **iter** debe regresar el objeto sobre el cual se iterará
- **next** debe regresar el siguiente valor y aventar la excepción StopIteration cuando ya no hayan elementos sobre los cual iterar.

Por su parte, los generators son simplemente una forma rápida de crear iterables sin la necesidad de declarar una clase que implemente el protocolo de iteración. Para crear un generator simplemente declaramos una función y utilizamos el keyword yield en vez de return para regresar el siguiente valor en una iteración. Por ejemplo,

```python
def fibonacci(max):
    a, b = 0, 1
    while a < max:
        yield a
        a, b = b, a+b
```

Es importante recalcar que una vez que se ha agotado un generator ya no podemos utlizarlo y debemos crear una nueva instancia. Por ejemplo,

```python
fib1 = fibonacci(20)
fib_nums = [num for num in fib1]
...
double_fib_nums = [num * 2 for num in fib1] # no va a funcionar
double_fib_nums = [num * 2 for num in fibonacci(30)] # sí funciona
```

# 3. Estructuras de Datos

## Uso de listas

Python y todos los lenguajes nos ofrecen *constructos* mucho más poderosos, haciendo que el desarrollo de nuestro software sea

- Más sofisticado
- Más legible
- Más fácil de implementar

Estos *constructos* se llaman **Estructuras de Datos** que nos permiten agrupar de distintas maneras varios valores y elementos para poderlos manipular con mayor facilidad.

Las **listas** las vas a utilizar durante toda tu carrera dentro de la programación e ingeniería de Software.

Las **listas** son una secuencia de valores. A diferencia de los *strings*, las **listas** pueden tener cualquier tipo de valor. También, a diferencia de los strings, son mutables, podemos agregar y eliminar elementos.

En Python, las listas son referenciales. Una lista no guarda en memoria los objetos, sólo guarda la **referencia** hacia donde viven los objetos en memoria

Se inician con `[]` o con la *built-in function* `list`.

lista con lista[::]

```python
paises
Out[20]: ['España', 'Alemania', 'Suiza']
eu = paises[::]
paises[0] = 'Portugal'
paises
Out[23]: ['Portugal', 'Alemania', 'Suiza']
eu
Out[24]: ['España', 'Alemania', 'Suiza']
```

También puedes usar slices para copiar una lista sin necesidad de utilizar ningún método, así:

```python
list_2 = list_1[:]
```

## Operaciones con listas

Ahora que ya entiendes cómo funcionan las **listas**, podemos ver qué tipo de operaciones y métodos podemos utilizar para modificarlas, manipularlas y realizar diferentes tipos de cómputos con esta Estructura de Datos.

- El operador **+(suma)** concatena dos o más listas.
- El operador ***(multiplicación)** repite los elementos de la misma lista tantas veces los queramos multiplicar

Sólo podemos utilizar **+(suma)** y ***(multiplicación)**.

Las listas tienen varios métodos que podemos utilizar.

- `append` nos permite añadir elementos a listas. Cambia el tamaño de la lista.
- `pop` nos permite sacar el último elemento de la lista. También recibe un índice y esto nos permite elegir qué elemento queremos eliminar.
- `sort` modifica la propia lista y ordenarla de mayor a menor. Existe otro método llamado `sorted`, que también ordena la lista, pero genera una nueva instancia de la lista
- `del`nos permite eliminar elementos vía indices, funciona con *slices*
- `remove` nos permite es pasarle un valor para que Python compare internamente los valores y determina cuál de ellos hace match o son iguales para eliminarlos.

**Suma** (+) Concatena dos o más listas:

```python
a=[1,2]
b=[3,4]
a + b --> [1,2,3,4]
```

**Multiplicación** (*) Repite la misma lista:

```python
a=[1,2]
a * 2 —> [1,2,1,2]
```

**Añadir elemento** al final de la lista:

```python
a=[1]
a.append(2)=[1,2]
```

**Eliminar elemento** al final de la lista:

```python
a=[1,2]
b=a.pop()
a=[1]
```

**Eliminar elemento** dado un indice:

```python
a=[1,2]
b=a.pop(1)
a=[2]
```

**Ordenar lista** de menor a mayor, esto modifica la lista inicial

```python
a=[3,8,1]
a.sort() —> [1,3,8]
```

**Ordenar lista** de menor a mayor, esto NO modifica la lista inicial

```python
a=[3,8,1]
a.sorted() —> [1,3,8]
```

**Eliminar elementos de lista** Elimina el elemento de la lista dado su indice

```python
a=[1,2,3]
del a[0] —> a[2,3]
```

**Eliminar elementos de lista** Elimina el elemento de la lista dado su valor

```python
a=[0, 2, 4, 6, 8]
a.remove(6)
a=[0, 2, 4, 8]
```

**Range** creacion de listas en un rango determinado

```python
a=(list(range(0,10,2))) # -->crea un conteo desde 0 hasta 10 en pasos de 2 en 2.
a=[0,2,4,6,8]
```

**Tamaño lista** len Devuelve el valor del tamaño de la lista:

```python
a=[0,2,4,6,8]
len(a)=5
```

**Metodos de Listas**

El tipo de dato lista tiene algunos métodos más. Aquí están todos los métodos de los objetos lista:

- **list.append(x)**

  Agrega un ítem al final de la lista. Equivale a a[len(a):] = [x].

- **list.extend(iterable)**

  Extiende la lista agregándole todos los ítems del iterable. Equivale a a[len(a):] = iterable.

- **list.insert(i, x)**

  Inserta un ítem en una posición dada. El primer argumento es el índice del ítem delante del cual se insertará, por lo tanto a.insert(0, x) inserta al principio de la lista, y a.insert(len(a), x) equivale a a.append(x).

- **list.remove(x)**

  Quita el primer ítem de la lista cuyo valor sea x. Es un error si no existe tal ítem.

- **list.pop([i])**

  Quita el ítem en la posición dada de la lista, y lo devuelve. Si no se especifica un índice, a.pop() quita y devuelve el último ítem de la lista. (Los corchetes que encierran a i en la firma del método denotan que el parámetro es opcional, no que deberías escribir corchetes en esa posición. Verás esta notación con frecuencia en la Referencia de la Biblioteca de Python.)

- **list.clear()**

  Quita todos los elementos de la lista. Equivalente a del a[:].

- **list.index(x[, start[, end]])**

  Devuelve un índice basado en cero en la lista del primer ítem cuyo valor sea x. Levanta una excepción ValueError si no existe tal ítem.

  Los argumentos opcionales start y end son interpetados como la notación de rebanadas y se usan para limitar la búsqueda a una subsecuencia particular de la lista. El index retornado se calcula de manera relativa al inicio de la secuencia completa en lugar de con respecto al argumento start.

- **list.count(x)**

  Devuelve el número de veces que x aparece en la lista.

- **list.sort(key=None, reverse=False)**

  Ordena los ítems de la lista in situ (los argumentos pueden ser usados para personalizar el orden de la lista, ve sorted() para su explicación).

- **list.reverse()**

  Invierte los elementos de la lista in situ.

- **list.copy()**

  Devuelve una copia superficial de la lista. Equivalente a a[:].

Un ejemplo que usa la mayoría de los métodos de lista:

```python
>>> frutas = ['naranja', 'manzana', 'pera', 'banana', 'kiwi', 'manzana', 'banana']
>>> frutas.count('manzana')
2
>>> frutas.count('mandarina')
0
>>> frutas.index('banana')
3
>>> frutas.index('banana', 4)  # Find next banana starting a position 4
6
>>> frutas.reverse()
>>> frutas
['banana', 'manzana', 'kiwi', 'banana', 'pera', 'manzana', 'naranja']
>>> frutas.append('uva')
>>> frutas
    ['banana', 'manzana', 'kiwi', 'banana', 'pera', 'manzana', 'naranja', 'uva']
>>> frutas.sort()
>>> frutas
['manzana', 'manzana', 'banana', 'banana', 'uva', 'kiwi', 'naranja', 'pera']
>>> frutas.pop()
'pera'```

Información tomada del Tutorial de Python de [http://docs.python.org.ar/tutorial/3/datastructures.html#mas-sobre-listas](http://docs.python.org.ar/tutorial/3/datastructures.html#mas-sobre-listas)
```

## Agregando listas a nuestro proyecto

[index()](https://www.programiz.com/python-programming/methods/list/index) es un método que busca la posición de un elemento en una lista y nos regresa su posicion. Y si hay varios elementos iguales, nos regresa el primer elemento

```python
import sys

clients = ['luisa', 'juan']

def create_client(client_name):
	global clients

	if client_name not in clients :
		clients.append(client_name)
	else:
		print ('Client already is in the client\'s list')

def list_clients():
	for idx, client in enumerate(clients):
		print ('{} : {}'.format(idx, client))


def updated_client(client_name, updated_client_name):
	global clients

	if client_name in clients :
		index = clients.index(client_name)
		clients[index] = updated_name
	else:
		print ('Client is no it client\'s list')


def delete_client(client_name):
	global clients

	if client_name in clients:
		clients.remove(client_name)
	else:
		print('Client is not in client\'s list')

def search_client(client_name):

	for client in clients:
		if client != client_name:
			continue
		else:
			return True


def _print_welcome():
	print ('WELCOME TO PLATZI VENTAS')
	print ('*' * 50)
	print ('What would you like to do today ? ')
	print ('[C]reate client')
	print ('[L]ist client')
	print ('[U]pdated client')
	print ('[D]elete client')
	print ('[S]earch client')


def _get_client_name ():
	client_name = None

	while not client_name:
		client_name = str.lower(input('What is the client name ? '))

		if client_name == 'exit':
			client_name = None
			break

	if not client_name:
			sys.exit()

	return client_name

if __name__ == '__main__':
	_print_welcome()

	command = input()
	command = command.upper()

	if command == 'C':
		client_name = _get_client_name()
		create_client(client_name)
		list_clients()
	elif command == 'L':
		list_clients()
	elif command == 'U':
		client_name = _get_client_name()
		updated_name = str.lower(input ('What is the updated client name ? '))
		updated_client(client_name, updated_name)
		list_clients()
	elif command == 'D':
		client_name = _get_client_name()
		delete_client(client_name)
		list_clients()
	elif command == 'S':
		client_name = _get_client_name()
		found = search_client(client_name)

		if found:
			print('The client is in the client\'s list')
		else:
			print('The client: {} is not in our client\'s list'.format(client_name))

	else:
		print ('Invalid command')
```

> **TIP** Es mas organizado trabajar con una funcion llamada run() para introducir todo el bloque de codigo donde se llaman las funciones y en el entry point unicamente hacer el llamado de la funcion es una buena practica y es lo que se hace a nivel profesional

## Diccionarios

Los diccionarios se conocen con diferentes nombres a lo largo de los lenguajes de programación como HashMaps, Mapas, Objetos, etc. En Python se conocen como **Diccionarios**.

Un diccionario es similar a una lista sabiendo que podemos acceder a través de un indice, pero en el caso de las listas este índice debe ser un número entero. Con los diccionarios puede ser cualquier objeto, normalmente los verán con **strings** para ser más explicitos, pero funcionan con muchos tipos de llaves…

Un diccionario es una asociación entre llaves(**keys**) y valores(**values**) y la referencia en Python es muy precisa. Si abres un diccionario verás muchas palabras y cada palabra tiene su definición.

Para iniciar un diccionario se usa `{}` o con la función `dict`

Estos también tienen varios métodos. Siempre puedes usar la función `dir` para saber todos los métodos que puedes usar con un objeto.

Si queremos ciclar a lo largo de un diccionario tenemos las opciones:

**`keys`**: nos imprime una lista de las llaves
**`values`** nos imprime una lista de los valores
**`items`**. nos manda una lista de tuplas de los valores

Usando dir() en un diccionario encontré estos métodos

![Captura.PNG](https://static.platzi.com/media/user_upload/Captura-ccd3e743-6d98-4249-b43c-0c7f7e51441b.jpg)

clear(): Remueve todos los elementos del diccionario

copy(): Copia todos los elementos de un diccionario y los guarda en otra variable

fromkeys(): Crea un diccionario y sus llaves son los items de una secuencia

```
keys = ['a', 'b', 'c', 'd', 'e']

vowels = dict.fromkeys(keys) 

print(vowels)		# {'o': None, 'i': None, 'u': None, 'a': None, 'e': None}
```

get(): Devuelve el valor de una llave

items(): Devuelve una tupla con una lista de las llaves y valores del diccionario

```
sales = { 'apple': 2, 'orange': 3, 'grapes': 4 }

print(sales.items())		# dict_items([('apple', 2), ('orange', 3), ('grapes', 4)])
```

keys(): Lo mismo que items(), pero con solo las llaves de un diccionario
values(): Lo mismo que items(), pero con solo los valores

pop(key): remueve y retorna un elemento de un diccionario por su llave

popitem(): remueve y retorna un item(key, value) de un diccionario

setdefault(key, default_value): retorna un valor de una llave, si no lo encuentra inserta una llave con un valor en el diccionario

[update([]):](https://www.programiz.com/python-programming/methods/dictionary) Recibe una secuencia key/value y actualiza el diccionario actual con el diccionario recibido.

Comparto un ejercicio espectacular para practicar [*diccionarios 👑*](https://www.practicepython.org/exercise/2017/01/24/33-birthday-dictionaries.html)

Puedo tener un diccionario dentro de un diccionario

```python
comida = {"italiana": "pizza", "mexicana" : {'deliciosas': 'taco'}}
```

Accedo de esta manera al objeto

```python
comida["mexicana"]["deliciosas"]
>>>'taco'
```

![img](https://media.giphy.com/media/5xtDarBFszThqQF1o6A/giphy.gif)

## Agregando diccionarios a nuestro proyecto

Mi versión del código

```python
import sys


clients = [
    {
        'name': 'Pablo',
        'company': 'Google',
        'email': 'pablo@google.com',
        'position': 'Sofware Engineer',
    },
    {
        'name': 'Ricardo',
        'company': 'Facebook',
        'email': 'ricardo@facebook.com',
        'position': 'Data Engineer',
    },
]

def create_client(client):
    global clients

    if client not in clients:
        clients.append(client)
    else:
        print('Client already is in client\'s list')

def list_clients():
    for idx, client in enumerate(clients):
        print('{uid} | {name} | {company} | {email} | {position}'.format(
            uid = idx,
            name = client['name'],
            company = client['company'],
            email = client['email'],
            position = client['position']))


def ingress_client_data(): 
    client = {
        'name': _get_client_field('name'),
        'company': _get_client_field('company'),
        'email': _get_client_field('email'),
        'position': _get_client_field('position')
    }
    return client


def update_client(client_id, updated_client):
    global clients

    if len(clients) -1 >= client_id:                
        clients[client_id] = updated_client
    else:
        print('Client is not in clients list')


def delete_client(client_name):
    global clients

    if client_name in clients:
        clients.remove(client_name)
    else:
        print('Client is not in clients list')


def search_client(client_name):
    for client in clients:
        if client != client_name:
            continue
        else:
            return True


def _print_welcome():
    print('WELCOME TO PLATZI VENTAS')
    print('*' * 50)
    print('What would yo like to do today?')
    print('[C]reated client')
    print('[U]pdate client')
    print('[L]ist clients')
    print('[D]elete client')
    print('[S]earch client')


def _get_client_field(field_name):
    field = None

    while not field:
        field = input('What is the client {}? '.format(field_name))

    return field


if __name__ == '__main__':
    _print_welcome()

    command = input()
    command = command.upper()

    if command == 'C':
        client = ingress_client_data()
        create_client(client)
        list_clients()
    elif command == 'D':
        client_name = _get_client_name()
        delete_client(client_name)
        list_clients()
    elif command == 'U':
        list_clients()
        client_id = int(_get_client_field('id'))
        updated_client = ingress_client_data()

        update_client(client_id, updated_client)
        list_clients()
    elif command == 'L':
        list_clients()
    elif command == 'S':
        client_name = _get_client_name()
        found = search_client(client_name)

        if found:
            print('The client is in the client\'s list')
        else :
            print('The client: {} is not in client\'s list'.format(client_name))
    else:
        print('Invalid command') 
```

## Tuplas y conjuntos

Tuplas(**tuples**) son iguales a las listas, la única diferencia es que son **inmutables**, la diferencia con los *strings* es que pueden recibir muchos tipos valores. Son una serie de valores separados por comas, casi siempre se le agregan paréntesis para que sea mucho más legible.

Para poderla inicializar utilizamos la función `tuple`.

Uno de sus usos muy comunes es cuando queremos regresar más de un valor en nuestra función.

Una de las características de las Estructuras de Datos es que cada una de ellas nos sirve para algo especifico. No existe en programación una navaja suiza que nos sirva para todos. Los mejores programas son aquellos que utilizan la herramienta correcta para el trabajo correcto.

Conjutos(**sets**) nacen de la teoría de conjuntos. Son una de las Estructuras más importantes y se parecen a las listas, podemos añadir varios elementos al conjunto, pero **no pueden existir elementos duplicados**. A diferencia de los **tuples** podemos agregar y eliminar, son **mutables**.

Los sets se pueden inicializar con la función **set**. Una recomendación es inicializarlos con esta función para no causar confusión con los diccionarios.

- `add` nos sirve añadir elementos.
- `remove` nos permite eliminar elementos.

```python
"""
------ TUPLAS ------
Las tuplas (tuples) son similares a las listas.
La unica diferencia es que las tuplas son inmutables.
La diferencia con los strings es que pueden recibir muchos tipos valores.
Son una serie de valores separados por comas, casi siempre se le agregan
paréntesis para que sea mucho más legible.
tup = 1,2,3
tup = (1,2,3)-->Esta opción es la más se utiliza

Para poderla inicializar utilizamos la función 'tuple'.
Un uso muy común es utilizarlas para regresar más de
un valor en una función.
return (students,teachers)

------ CONJUNTOS ------
Los conjuntos (sets) son una colección sin orden que no 
permite elementos duplicados. A diferencia de los tuples podemos
agregar y eliminar, son mutables.
Los sets se inicializan con la función 'set'.
Para añadir elementos utilizamos el método 'add' y para eliminarlos
utilizamos el método 'remove'
"""

## LISTA --> ages = [12,18,24,34,50]
## TUPLA --> ages = (12,18,24,34,50)
## CONJUNTOS --> ages = {12,18,24,34,50}

"""Con las tuplas que pueden utilizar casi todas las (las mismas)
funciones y métodos que funcionan con las listas"""

##------ TUPLAS ------
a = (1,2,2,3)
b = (3,4,5)
c = b+a
print(c)

print(a.count(2)) ## Nos dice cuantas veces aparece el numero 2 en la tupla
print(b.index(4)) ## NOs dice en que posición aparece por primera el numero 4 en la tupla

##Esto no lo podemos hacer porque son inmutables, al contrario que las listas, que son mutables
##a[1]= 10
##c[2] = 22

##------ CONJUNTOS ------
# Creando un conjunto en python
print('-'*30)
A = {1,2,3}
print(A)

# Creando un conjunto a partir de una lista
print('-'*30)
lista = ["bananas", "manzanas", "naranjas", "limones"]
B = set(lista)
print(B)

# Los conjuntos eliminan los elementos duplicados
print('-'*30)
lista = ["bananas", "manzanas", "naranjas", "limones",
        "bananas", "bananas", "limones", "naranjas"]
B = set(lista)
print(B)

# Creando el conjunto vacío
print('-'*30)
C = set()
print(C)

# Cardinalidad de un conjunto con len().
print('-'*30)
print("La cardinalidad del conjunto A = {0} es {1}".format(A,len(A)))

# comprobando membresía
print('-'*30)
print(2 in A)
print(0 in A)

# Igualdad entre conjuntos. El orden de los elementos no importa.
print('-'*30)
A = {1,2,3,4}
B = {4,2,3,1}
print(A == B)

# Subconjunto. No hay distincion entre subconjunto y propio
# para el conjunto por defecto de python.
print('-'*30)
A = {1,2,3}
B = {1,2,4,3,5}
"""issubset() busca si los valores de A están en B, sin importar el orden"""
print(A.issubset(B))

# Union de conjuntos. Omite los valores repetidos
print('-'*30)
A = {1,2,3,4,5}
B = {4,5,6,7,8,9,10}
print(A.union(B))

# Intersección de conjuntos, es decir, los valores que se repiten
print('-'*30)
print(A.intersection(B))

# Diferencia entre conjuntos
print('-'*30)
print(A.difference(B))
print(B.difference(A))
print(A - B)
print(B - A)

##------ Utilizando sympy -------
# Utilizando FiniteSet de sympy
"""Para que el import no de error, abrir el power shell como admin
en la carpeta donde este el archivo (en mi caso, que utilizo el 
sublime text 3) o si ejecutais desde el interprete por consola, en ambos
casos teneis que poner pip install sympy"""
print('-'*30)
from sympy import FiniteSet
C = FiniteSet(1, 2, 3)
print(C)

# Generando el conjunto potencia. Esto no se puede
# hacer utilizando el conjunto por defecto de python.
print('-'*30)
print(C.powerset())

# Cardinalidad
print('-'*30)
print("La cardinalidad del conjunto potencia del conjunto C = {0} es {1}".
     format(C, len(C.powerset())))

# Igualdad
print('-'*30)
A = FiniteSet(1, 2, 3)
B = FiniteSet(1, 3, 2)
print(A == B)

A = FiniteSet(1, 2, 3)
B = FiniteSet(1, 3, 4)
print(A == B)

# Subconjunto y subconjunto propio
print('-'*30)
A = FiniteSet(1,2,3)
B = FiniteSet(1,2,3,4,5)
print(A.is_subset(B))

# A == B. El test de subconjunto propio da falso
print('-'*30)
B = FiniteSet(2,1,3)
print(A.is_proper_subset(B))

# Union de dos conjuntos
print('-'*30)
A = FiniteSet(1, 2, 3)
B = FiniteSet(2, 4, 6)
print(A.union(B))

# Interseccion de dos conjuntos
print('-'*30)
A = FiniteSet(1, 2) 
B = FiniteSet(2, 3) 
print(A.intersect(B))

# Diferencia entre conjuntos
print('-'*30)
print(A - B)

# Calculando el producto cartesiano. Con el conjunto por 
# defecto de python no podemos hacer esto con el operador *
print('-'*30)
A = FiniteSet(1, 2)
B = FiniteSet(3, 4)
P = A * B
print(P)

for elem in P:
    print(elem)

# Elevar a la n potencia un conjunto. Calcula el n 
# producto cartesiano del mismo conjunto.
print('-'*30)
A = FiniteSet(1, 2, 3, 4)
P2 = A ** 2
print(P2)

P3 = A ** 3
print(P3)

for elem in P3:
    print(elem)

# Dibujanto el diagrama de venn de 2 conjuntos
"""Para que el import no de error, abrir el power shell como admin
en la carpeta donde este el archivo (en mi caso, que utilizo el 
sublime text 3) o si ejecutais desde el interprete por consola, en ambos
casos teneis que poner pip install 'matplotlib'
y ' pip install matplotlib_venn'"""
print('-'*30)
from matplotlib_venn import venn2, venn2_circles
import matplotlib.pyplot as plt
   
print(dir(plt))
A = FiniteSet(1, 3, 5, 7, 9, 11, 13, 15, 17, 19)
B = FiniteSet(2, 3, 5, 7, 11, 13, 17, 19, 8)

plt.figure(figsize=(10, 8))
v = venn2(subsets=[A, B], set_labels=('A', 'B'))
v.get_label_by_id('10').set_text(A - B)
v.get_label_by_id('11').set_text(A.intersection(B))
v.get_label_by_id('01').set_text(B - A)
c = venn2_circles(subsets=[A, B], linestyle='dashed')
c[0].set_ls('solid')
plt.show()
```

> **Los mejores trabajos son aquellos que utilizan la herramienta correcta para el trabajo correcto**

## Tuplas y conjuntos en código

En esta clase practicaremos en código lo aprendido en la clase anterior sobre tuplas(tuples) y conjuntos(sets) para que sea mucho más claro entenderlo.

Operadores para **sets** en Python:

```python
>>> A = {1, 2, 3}	# conjunto A
>>> B = {3, 4 ,5}	# conjunto B
>>> A | B		#unión
{1, 2, 3, 4, 5}
>>> A & B	# intersección
{3}
>>> A - B		# diferencia entre A y B
{1, 2}
>>> B - A		# diferencia entre B y A
{4, 5}
```

**Declaracion de tuplas**

```python
a= 1,2,3
a=(1,2,3)
# Operaciones con tuplas:
# Acceder a un valor mediante indice de tupla
a[0]=1
# Conteo de cuantas veces está un valor en la tupla
a.count(1)—>1

#Declaración conjutos o Sets
a= set([1,2,3])
a={1,2,3}

# Operaciones con conjuntos:

# NO se puede acceder a un valor mediante índice
# NO se puede agregar un valor ya existente, por ejemplo
# Agregar un valor a conjunto
a.add(1)—> error!! (valor existente en set)
a.add(20)—> a={1,2,3,20}

# Tenemos:
a={1,2,3,20}
b={3,4,5}
a.intersection(b)–> {3}  # (elementos en común)
a.union(b)—>{1,2,3,20,4,5} # (elementos de a + b pero sin repetir ninguno)
a.difference(b)–>{1,2,20} # (elementos de a que no se encuentran en b)
b.difference(a)–>{4,5}  # (elementos de b que no se encuentran en a)
```

## Introducción al módulo collections

El módulo collections nos brinda un conjunto de objetos primitivos que nos permiten extender el comportamiento de las built-in collections que poseé Python y nos otorga estructuras de datos adicionales. Por ejemplo, si queremos extender el comportamiento de un diccionario, podemos extender la clase UserDict; para el caso de una lista, extendemos UserList; y para el caso de strings, utilizamos UserString.

Por ejemplo, si queremos tener el comportamiento de un diccionario podemos escribir el siguiente código:

```python
class SecretDict(collections.UserDict):

   def _password_is_valid(self, password):
        …

def _get_item(self, key):
    … 

def __getitem__(self, key):
     password, key = key.split(‘:’)
     
     if self._password_is_valid(password):
          return self._get_item(key)
     
     return None

my_secret_dict = SecretDict(...)
my_secret_dict[‘some_password:some_key’] # si el password es válido, regresa el valor
```

Otra estructura de datos que vale la pena analizar, es namedtuple. Hasta ahora, has utilizado tuples que permiten acceder a sus valores a través de índices. Sin embargo, en ocasiones es importante poder nombrar elementos (en vez de utilizar posiciones) para acceder a valores y no queremos crear una clase ya que únicamente necesitamos un contenedor de valores y no comportamiento.

```python
Coffee = collections.NamedTuple(‘Coffee’, (‘size’, ‘bean’, ‘price’))
def get_coffee(coffee_type):
     If coffee_type == ‘houseblend’:
         return Coffee(‘large’, ‘premium’, 10)
```

El módulo collections también nos ofrece otros primitivos que tienen la labor de facilitarnos la creación y manipulación de colecciones en Python. Por ejemplo, Counter nos permite contar de manera eficiente ocurrencias en cualquier iterable; OrderedDict nos permite crear diccionarios que poseen un orden explícito; deque nos permite crear filas (para pilas podemos utilizar la lista).
En conclusión, el módulo collections es una gran fuente de utilerías que nos permiten escribir código más “pythonico” y más eficiente.

## Python comprehensions

Las Comprehensions son constructos que nos permiten generar una secuencia a partir de otra secuencia.

Existen tres tipos de comprehensions:

**List comprehensions**

```python
[element for element in element_list if element_meets_condition]
```

**Dictionary comprehensions**

```python
{key: element for element in element_list if element_meets_condition}
```

**Sets comprehensions**

```python
{element for element in element_list if elements_meets_condition} 
```

## Búsquedas binarias

Uno de los conceptos más importantes que debes entender en tu carrera dentro de la programación son los algoritmos. No son más que una secuencia de instrucciones para resolver un problema específico.

Búsqueda binaria lo único que hace es tratar de encontrar un resultado en una lista ordenada de tal manera que podamos razonar. Si tenemos un elemento mayor que otro, podemos simplemente usar la mitad de la lista cada vez.

```python
sorted(), # devuelve una nueva lista ordenada, sin afectar a la lista original.
list.sort() #,ordena la lista en el lugar , mutando los índices de la lista y devuelve 
None(como todas las operaciones en el lugar).
```

## Continuando con las Búsquedas Binarias

`binary_serach.py`

```python
import random

def loop_search(target, data):
    found = False
    
    for i in data:
        if i == target:
            found = True

    return found


if __name__ == '__main__':
    data = [random.randint(0, 100) for i in range(20)]
    data.sort()
    print(data)

    target = int(input('What number would you like to find? '))

    found = loop_search(target, data)

    print(found)
```

## Manipulación de archivos en Python 3

En la [documentación](https://docs.python.org/2/library/os.html#os.remove) de Python se explica que, en S.O. Windows, la función remove del módulo os lanzará una excepción si se intenta eliminar un archivo abierto. De manera similar, la función rename lanzará una excepción si se intenta renombrar un archivo cuando el nombre del archivo está en uso.

**DictReader**
Esta crea un objeto el cuál mapea la información leída a un diccionario cuyas llaves están dadas por el parámetro fieldnames. Este parámetro es opcional, pero cuando no se especifica en el archivo, la primera fila de datos se vuelve las llaves del diccionario.

**DictWriter**
Esta clase es similar a la clase DictWriter y hace lo contrario, que es escribir datos a un archivo CSV. La clase es definida como   csv.DictWriter(csvfile, fieldnames, restval=’’, extrasaction=‘raise’, dialect=‘excel’, *args, **kwds)

El parámetro fieldnames define la secuencia de llaves que identifican el orden en el cuál los valores en el diccionario son escritos al archivo CSV. A diferencia de DictReader, esta llave no es opcional y debe ser definida para evitar errores cuando se escribe a un CSV.

_initialize_clients_from_storage() para que cree el archivo por si el mismo si no existe.
gracias a este articulo

```python
def _initialize_clients_from_storage():
    if not os.path.exists(CLIENT_TABLE):
        with open(CLIENT_TABLE, mode= 'w'):
            pass
    else:
        with open(CLIENT_TABLE, mode='r') as f:
            reader = csv.DictReader(f,fieldnames=CLIENT_SCHEMA)

            for row in reader:
                clients.append(row)
```



# 4. Uso de objetos y módulos

## Decoradores

Python es un lenguaje que acepta diversos paradigmas como programación orientada a objetos y la programación funcional, siendo estos los temas de nuestro siguiente módulo.

Los **decoradores** son una función que envuelve a otra función para modificar o extender su comportamiento.

En Python las funciones son ciudadanos de primera clase, first class citizen, esto significan que las funciones pueden recibir funciones como parámetros y pueden regresar funciones. Los decoradores utilizan este concepto de manera fundamental.

TIP Los decoradores son nada mas que simplemente funciones agregadas a lfunciones ya existentes de modo de que imaginemos que un programa ya realizado tiene todas sus funciones que no deberian ser tocadas por que se rompe el codigo la forma de interactuar con ellas es atraves de decoradores para agregarles funcionalidades nuevas y no tener que volver a escribir todo el codigo de nuevo y ademas de eso los decoradores solamente funcionan en la funcion que decores, es algo genial.

![img](https://www.conceptslife.com/wp-content/uploads/2020/06/C%C3%B3mo-decorar-sillones-con-cojines-y-mantas.jpg)

- Los decoradores, son una función que envuelve a otra función para extender o modificar su comportamiento.
- En python, las funciones pueden recibir funciones como parámetros, y devolver funciones. Las funciones decoradoras utilizan este concepto para decorar una función.

```python
def func_decorate(func_param):
	def func_modify:
		#some modify
		func_param
		#some extend
	return func_modify
```

## Decoradores en Python

En esta clase pondremos en práctica lo aprendido en la clase anterior sobre decoradores.

Por convención la función interna se llama wrapper,
Para usar los decoradores es con el símbolo de @(arroba) y lo colocamos por encima de la función. 

Es un sugar syntax **\*args **kwargs son los argumentos que tienen keywords, es decir que tienen nombre y los argumentos posicionales, los args. Los asteriscos son simplemente una expansión.

Los decoradores sirven para ejecutar lógica del código antes y/o después de otra función, esto nos ayuda a generar funciones y código que pueda ser reutilizado fácilmente sin hacer más extenso nuestro código. Hay que recordar que si se genera una función dentro de otra solo existiera en ese scope(dentro de la función padre), si se quiere invocar una función varias veces dentro de otras se tiene que generar de manera global.

**args y kwargs**

Básicamente lo que hacen es pasar tal cual los valores de de los argumentos que se pasan a la función args hace referencias a listas y  kwargs a elementos de un diccionario (llave: valor)
** args: **

```python
def test_valor_arg(n_arg, *args):
    print('primer valor normal: ', n_arg)

    For arg in args:
    print('este es un valor de *args: ',arg)
    
    print(type(args))

if__name__ == '__mai

n__':



    test_valor_args('carlos','Karla','Paola','Elena')
```

- el tipo de valor y es una tupla
- solo poniendo argumentos divididos por comas los convierte

**kuargs: **

```python
def test_valor_kwargs(**kwargs):
    if kwargs is not None:
        for key,  value in kwargs.items():
            print('%s == %s' %(key,value))

print(type(kwargs))

if __name__ == '__main__':

test_valor_kwargs(caricatura='batman')
```


el valor que te da es un diccionario
toma los valores en los extremos de un signo igual

Este es un ejemplo usando los 2 en una función

```python
  def test_valor_kwargs_args(*args, **kwargs):
    print(type(kwargs))
    print(kwargs)
    print('----------')
    print(type(args))
    print(args)

if __name__ == '__main__':
    test_valor_kwargs_args('flash', 'batman', caricatura='batman', empresa = 'dc')
```

## ¿Qué es la programación orientada a objetos?

La programación orientada a objetos es a paradigm de programación que otorga los medios para estructurar programas de tal manera que the properties y comportamientos estén envueltos en objetos individuales.

Para poder entender cómo modelar estos objetos tenemos que tener claros cuatro principios:

- Encapsulamiento.
- Abstracción
- Herencia
- Polimorfismo

Las clases simply nos sirven como un molde para poder generar diferentes instancias.

La Programación Orientada a Objetos viene de una filosofía o forma de pensar que es la Orientación a Objetos y esto surge a partir de los problemas que necesitamos plasmar en código.

Es analizar un problema en forma de objetos para después llevarlo a código, eso es la Orientación a Objetos.

**El paradigma de Programación Orientada a Objetos se compone de 4 elementos:**

**Clases**: Una Clase es el modelo por el cual nuestros objetos se van a construir y nos van a permitir generar más objetos.

**Propiedades**: Las Propiedades también pueden llamarse atributos y estos también serán sustantivos. Algunos atributos o propiedades son nombre, tamaño, forma, estado, etc. Son todas las características del objeto.

**Métodos**: Los Comportamientos serán todas las operaciones que el objeto puede hacer, suelen ser verbos o sustantivos y verbo. Algunos ejemplos pueden ser que el usuario pueda hacer login y logout.

**Objetos**: son aquellos que tienen propiedades (atributos) y comportamientos (operaciones que el objeto puede hacer), también serán sustantivos. Pueden ser Físicos o Conceptuales

Para poder entender cómo modelar estos objetos tenemos que tener claros cuatro principios:

**Encapsulation**: es hacer que un dato sea inviolable, inalterable cuando se le asigne un modificador de acceso.
Cada objeto tiene sus propias funciones y datos sin afectar a otros, son lógica interna.

**Abstraction**: es cuando separamos los datos de un objeto para generar un molde. El usuario podrá interactuar con el objeto sin necesidad de conocer toda la lógica del mismo.

En el caso de POO una Clase es el modelo por el cual nuestros objetos se van a construir y nos van a permitir generar más objetos.

**Inheritance**: La herencia nos permite crear nuevas clases a partir de otras, se basa en modelos y conceptos de la vida real. También  tenemos una jerarquía de padre e hijo.
Si se declara un método en una clase  todas las subclases heredan ese método, es decir: si tu declaras un  método “imprimir” que ejecute un print en una clase, las subclases podrán usar el método imprimir, sin necesidad de declararlo en cada una.

**Polymorphism**: Es construir métodos con el mismo nombre pero con comportamiento diferente.

## Programación orientada a objetos en Python

Para declarar una clase en Python utilizamos la keyword class, después de eso le damos el nombre. Una convención en Python es que todas las clases empiecen con mayúscula y se continua con CamelCase.

Un método fundamental es dunder init(\__init__). Lo único que hace es inicializar la clase basado en los parámetros que le damos al momento de construir la clase.

**self** es una referencia a la clase. Es una forma internamente para que podamos acceder a las propiedades y métodos.

> new() - es el responsable de **crear la instancia**, devuelve como objecto la instancia referencia. Cuando finaliza la ejecución del new, se procede  a la llamada el method ini().

```python
   1   class Person:
   2     def __init__(self, name, age):
   3       self.name = name
   4       self.age = age
   5   
   6     def say_hello(self):
   7       print('Hello, my name is {} and I am {} years old.'.format(self.name, self
       .age))
   8     
   9   
  10   if __name__ == '__main__':
  11     person = Person('John', 25)
  12     
  13     print('Age: {}'.format(person.age))
  14     person.say_hello()
```

## Scopes and namespaces

En Python, un name, también conocido como identifier, es simplemente una forma de otorgarle un nombre a un objeto. Mediante el nombre, podemos acceder al objeto. Vamos a ver un ejemplo:

```python
my_var = 5

id(my_var) # 4561204416
id(5) # 4561204416
```

En este caso, el identifier my_var es simplemente una forma de acceder a un objeto en memoria (en este caso el espacio identificado por el número 4561204416). Es importante recordar que un name puede referirse a cualquier tipo de objeto (aún las funciones).

```python
def echo(value):
    return value

a = echo

a(‘Billy’) # 3
```

Ahora que ya entendimos qué es un name podemos avanzar a los namespaces (espacios de nombres). Para ponerlo en palabras llanas, un namespace es simplemente un conjunto de names.

En Python, te puedes imaginar que existe una relación que liga a los nombres definidos con sus respectivos objetos (como un diccionario). Pueden coexistir varios namespaces en un momento dado, pero se encuentran completamente aislados. Por ejemplo, existe un namespace específico que agrupa todas las variables globales (por eso puedes utilizar varias funciones sin tener que importar los módulos correspondientes) y cada vez que declaramos una módulo o una función, dicho módulo o función tiene asignado otro namespace.

A pesar de existir una multiplicidad de namespaces, no siempre tenemos acceso a todos ellos desde un punto específico en nuestro programa. Es aquí donde el concepto de scope (campo de aplicación) entra en juego.

Scope es la parte del programa en el que podemos tener acceso a un namespace sin necesidad de prefijos.

En cualquier momento determinado, el programa tiene acceso a tres scopes:

- El scope dentro de una función (que tiene nombres locales)

- El scope del módulo (que tiene nombres globales)

- El scope raíz (que tiene los built-in names)

Cuando se solicita un objeto, Python busca primero el nombre en el scope local, luego en el global, y por último, en el raíz. Cuando anidamos una función dentro de otra función, su scope también queda anidado dentro del scope de la función padre.

```python
def outer_function(some_local_name):
    def inner_function(other_local_name):
         # Tiene acceso a la built-in function print y al nombre local some_local_name
         print(some_local_name) 
        
         # También tiene acceso a su scope local
         print(other_local_name)
```

Para poder manipular una variable que se encuentra fuera del scope local podemos utilizar los keywords global y nonlocal.

```python
some_var_in_other_scope = 10

def some_function():
     global some_var_in_other_scope
     
     Some_var_in_other_scope += 1
```

En Python, la palabra clave global permite modificar la variable fuera del alcance actual. Se utiliza para crear una variable global y realizar cambios en la variable en un contexto local.
Reglas de palabra clave global

**Las reglas básicas para la palabra clave global en Python son:**

- Cuando creamos una variable dentro de una función, es local por defecto.

- Cuando definimos una variable fuera de una función, es global por defecto. No tienes que usar la palabra clave global.

- Utilizamos la palabra clave global para leer y escribir una variable global dentro de una función.

- El uso de global fuera de una función no tiene efecto

En lo referente a la palabra nonlocal, actúa de manera similar, solo que orientada a lo que sería un alcance de funciones y funciones anidadas, a continuación un ejemplo:

```python
def method():

    def method2():
        # este método no tiene acceso a la 
				# variable value, por cuanto se usa 
				# nonlocal para poder acceder.
        nonlocal value
        value = 100

    # Variable local.
    value = 10
    method2()
```

## Introducción a Click

Click es un pequeño framework que nos permite crear aplicaciones de Línea de comandos. Tiene cuatro decoradores básicos:

**Framework:**

- **@click_group:** Agrupa una serie de comandos

- **@click_command:** Aca definiremos todos los comandos de nuestra apliacion

- **@click_argument:** Son parámetros necesarios
- **@click_option:** Son parámetros opcionales

Click también realiza las conversiones de tipo por nosotros. Esta basado muy fuerte en decoradores.

Tutorial entero de como [usar click](https://www.youtube.com/watch?v=riQd3HNbaDk)

## Definición a la API pública

En esta clase definiremos la estructura de nuestro proyecto **PlatziVentas**, los comandos, la configuración en nuestro `setup.py` y la instalaremos en nuestro entorno virtual con `pip`.

**Instalar virtualenv**

```sh
pip install virtualenv
```

**Activar virtualenv**

```sh
python -m venv venv
```

**Activar**

```sh
source venv/bin/activate
```

**Instalar aplicacion**

```sh
# Instalar en la carpeta plazi-ventas.
pip install --editable .
```

**pv**

```sh
pv --help
pv clients --help

python/CRUD/platzi-ventas on  main [!] via crud1 via 🐍 crud1 
➜ pv clients --help
Usage: pv clients [OPTIONS] COMMAND [ARGS]...

  Manages the clients lifecycle

Options:
  --help  Show this message and exit.

Commands:
  create  Create a new client
  delete  Delete a client
  list    List all clients
  update  Update a client
```

**Desconectar**

```sh
deactivate
```

Si tuvieron algun error con virtualenv y estan usando windows esto me sirvio.

```sh
virtualenv venv
python -m venv venv
venv\scripts\activate
```

file `pv.py`

```python
import click
from clients import commands as clients_commands

@click.group()
@click.pass_context
def cli(ctx):
  ctx.obj = {}


cli.add_command(clients_commands.all)
```

## Clients

Modelaremos a nuestros clientes y servicios usando lo aprendido en clases anteriores sobre programación orientada a objetos y clases.

**@staticmethod** nos permite declarar métodos estáticos en nuestra clase. Es un método que se puede ejecutar sin necesidad de una instancia de una clase. No hace falta que reciba self como parámetro.

> Division (estructura) de nuestro programa:
>
> 👀👀👀Interface --------------> Comandos
> 		👾👾👾Logica ---------------> Servicios
> 		🙋🙋🙋ObjetosInteractivos --> Clientes



## Servicios: Lógica de negocio de nuestra aplicación

👉Archivos **usados** en esta clase:👌

[commands.py](https://gist.github.com/noemk2/51c794078a64772fdd0560eb9b6b8c5c)
[models.py](https://gist.github.com/noemk2/197cbeb2fd570d5106499cf0a6f86087)
[services.py](https://gist.github.com/noemk2/385fbe447882c45c817bed6b0524a8fa)
[pv.py](https://gist.github.com/noemk2/f6a5d5f544fbceec929fe48aa423491d)

## Interface de create: Comunicación entre servicios y el cliente

Módulo llamado Tabulate, que pueden encontrar en la página oficial de PyPI o solo instalarlo con

```sh
 $ pip install tabulate
```

El código del método list queda así:
from tabulate import tabulate

```python
from tabulate import tabulate

@clients.command()
@click.pass_context
def list(ctx):
    """List all clients"""
    client_service = ClientService(ctx.obj['clients_table'])
    clients_list = client_service.list_clients()

headers = [field.capitalize() for field in Client.schema()]
table = []

for client in clients_list:
    table.append(
        [client['name'],
         client['company'],
         client['email'],
         client['position'],
         client['uid']])

print(tabulate(table, headers))
```

El resultado:

![sc.png](https://static.platzi.com/media/user_upload/sc-ed14c556-47f8-4346-822f-774d25495faa.jpg)

Les recomiendo leer un poco más acerca del funcionamiento del módulo aquí.

## Actualización de cliente

`save_to_disk`

```python
  def update_client(self, update_client):
    clients = self.list_clients()

    updated_clients = []
    for client in clients:
      if client['uid'] == update_client.uid:
        updated_clients.append(update_client.to_dict())
      else:
        updated_clients.append(client)

    self._save_to_disk(updated_clients)

  def _save_to_disk(self, clients):
    tmp_table_name = self.table_name + '.tmp'
    with open(tmp_table_name, mode='w') as f:
      writer = csv.DictWriter(f, fieldnames=Client.schema())
      writer.writerows(clients)

    # Replace the original table with the new one
    os.remove(self.table_name)
    os.rename(tmp_table_name, self.table_name)
```

## Interface de actualización

reto

```python
@clients.command()
@click.argument('client_uid', type=str)
@click.pass_context
def delete(ctx, client_uid):
    """ Delete a client """
    client_service = ClientService(ctx.obj['clients_table'])

    client_list = client_service.list_clients()

    client = [client for client in client_list if client['uid'] == client_uid]
    
    if client:
        client_service.delete_client(client)

        click.echo('Cliente deleted')
    else:
        click.echo('Client not found')
def delete_client(self, client_to_be_deleted):
        clients_list = self.list_clients()
        clients_list.remove(client_to_be_deleted[0])

        self._save_to_disk(clients_list)
```

## Manejo de errores y jerarquía de errores en Python

Python tiene una amplia jerarquía de errores que nos da posibilidades para definir errores en casos como donde no se pueda leer un archivo, dividir entre cero o si existen problemas en general en nuestro código Python. El problema con esto es que nuestro programa termina, es diferente a los errores de sintaxis donde nuestro programa nunca inicia.

Para ““aventar”” un error en Python utilizamos la palabra raise. Aunque Python nos ofrece muchos errores es buena práctica definir errores específicos de nuestra aplicación y usar los de Python para extenderlos.
Podemos generar nuestros propios errores creando una clase que extienda de **BaseException**.

Si queremos evitar que termine nuestro programa cuando ocurra un error, debemos tener una estrategia. Debemos utilizar try / except cuando tenemos la posibilidad de que un pedazo de nuestro código falle

- **try**: significa que se ejecuta este código. Si es posible, solo ponemos una sola línea de código ahí como buena práctica

- **except**: es nuestro manejo del error, es lo que haremos si ocurre el error. Debemos ser específicos con el tipo de error que vamos a atrapar.

- **else**: Es código que se ejecuta cuando no ocurre ningún error.

- **finally**: Nos permite obtener un bloque de código que se va a ejecutar sin importar lo que pase.

Jerarquía de clases para las [build-in Exceptions](https://docs.python.org/3/library/exceptions.html#exception-hierarchy):

- [Introducción a las excepciones en Python](https://realpython.com/python-exceptions/)
- [Tutorial para manejar los errores](https://www.programiz.com/python-programming/exceptions)

```sh
BaseException
 +-- SystemExit
 +-- KeyboardInterrupt
 +-- GeneratorExit
 +-- Exception
      +-- StopIteration
      +-- StopAsyncIteration
      +-- ArithmeticError
      |    +-- FloatingPointError
      |    +-- OverflowError
      |    +-- ZeroDivisionError
      +-- AssertionError
      +-- AttributeError
      +-- BufferError
      +-- EOFError
      +-- ImportError
      |    +-- ModuleNotFoundError
      +-- LookupError
      |    +-- IndexError
      |    +-- KeyError
      +-- MemoryError
      +-- NameError
      |    +-- UnboundLocalError
      +-- OSError
      |    +-- BlockingIOError
      |    +-- ChildProcessError
      |    +-- ConnectionError
      |    |    +-- BrokenPipeError
      |    |    +-- ConnectionAbortedError
      |    |    +-- ConnectionRefusedError
      |    |    +-- ConnectionResetError
      |    +-- FileExistsError
      |    +-- FileNotFoundError
      |    +-- InterruptedError
      |    +-- IsADirectoryError
      |    +-- NotADirectoryError
      |    +-- PermissionError
      |    +-- ProcessLookupError
      |    +-- TimeoutError
      +-- ReferenceError
      +-- RuntimeError
      |    +-- NotImplementedError
      |    +-- RecursionError
      +-- SyntaxError
      |    +-- IndentationError
      |         +-- TabError
      +-- SystemError
      +-- TypeError
      +-- ValueError
      |    +-- UnicodeError
      |         +-- UnicodeDecodeError
      |         +-- UnicodeEncodeError
      |         +-- UnicodeTranslateError
      +-- Warning
           +-- DeprecationWarning
           +-- PendingDeprecationWarning
           +-- RuntimeWarning
           +-- SyntaxWarning
           +-- UserWarning
           +-- FutureWarning
           +-- ImportWarning
           +-- UnicodeWarning
           +-- BytesWarning
           +-- ResourceWarning
```

## Context managers

Los context managers son objetos de Python que proveen información contextual adicional al bloque de código. Esta información consiste en correr una función (o cualquier callable) cuando se inicia el contexto con el keyword with; al igual que correr otra función cuando el código dentro del bloque with concluye. Por ejemplo:

```python
with open(‘some_file.txt’) as f:
    lines = f.readlines()
```

Si estás familiarizado con este patrón, sabes que llamar la función open de esta manera, garantiza que el archivo se cierre con posterioridad. Esto disminuye la cantidad de información que el programador debe manejar directamente y facilita la lectura del código.

Existen dos formas de implementar un context manager: con una clase o con un generador. Vamos a implementar la funcionalidad anterior para ilustrar el punto:

```python
class CustomOpen(object):
    def __init__(self, filename):
        self.file = open(filename)

    def __enter__(self):
        return self.file

    def __exit__(self, ctx_type, ctx_value, ctx_traceback):
        self.file.close()

with CustomOpen('file') as f:
    contents = f.read()
```

Esta es simplemente una clase de Python con dos métodos adicionales: enter y exit. Estos métodos son utilizados por el keyword with para determinar las acciones de inicialización, entrada y salida del contexto.

El mismo código puede implementarse utilizando el módulo contextlib que forma parte de la librería estándar de Python.

```python
from contextlib import contextmanager

@contextmanager
def custom_open(filename):
    f = open(filename)
    try:
        yield f
    finally:
        f.close()

with custom_open('file') as f:
    contents = f.read()
```

El código anterior funciona exactamente igual que cuando lo escribimos con una clase. La diferencia es que el código se ejecuta al inicializarse el contexto y retorna el control cuando el keyword yield regresa un valor. Una vez que termina el bloque with, el context manager toma de nueva cuenta el control y ejecuta el código de limpieza.

___

Otro gran ejemplo de context managers (sin caer en el razonamiento circular usando “open” para crean un “custom_open”)
es el verlo como una implementacion de tareas de shell, pero en python (imaginate escribiendo el “setup.py” de un server linux)
Han notado como es muy sencillo escribir en shell:

```sh
cd /path/to/project coolShellCommand -xyz cd -
```

Pero en python se necesita implementar algo como esto:

```python
cwd = os.getcwd()

try:
	os.chdir('path/to/project')
	os.system('coolShellCommand -xyz')
finally:
	os.chdir(cwd)
```

Lo cual se ve bastente feo y desordenado, seria genial escribir algo como:

```python
with os.chdir('path/to/project/'):
	os.system('coolShellCommand -xyz')
```

Pero no existe, asi que creemoslo nosotros:

```python
class cdContextMgr(ob­jec­t):
    def __init__(­self, new_dir):
        self­.new_dir = new_dir
        self­.old_dir = None

def __en­ter__(­self):
    self­.old_dir = os­.getcwd()
    os­.chdir(­self.new_dir)

def __ex­it__(­self, *_):
    os­.chdir(­self.old_dir)
```

y asi podemos utilizar:

```python
with cdContextMgr('/path/to/project')
	os.system('coolShellCommand -xyz')
```

Esto hace lo mismo que al principio, nos evitamos duplicar codigo y aprendemos sobre conext managers.

# 5. Python en el mundo real

## Aplicaciones de Python en el mundo real

Python tiene muchas aplicaciones:
En las ciencias tiene muchas librerías que puedes utilizar como analisis de las estrellas y astrofisica; si te interesa la medicina puedes utilizar Tomopy para analizar tomografías. También están las librerías más fuertes para la ciencia de datos **numpy**, **Pandas** y **Matplotlib**

En CLI por si te gusta trabajar en la nube y con datacenters, para sincronizar miles de computadoras:

- aws
- gcloud
- rebound
- geeknote

**Aplicaciones Web:**

- Django
- Flask
- Bottle
- Chalice
- Webapp2
- Gunicorn
- Tornado

# 6. Conclusiones finales

## Python 2 vs 3 (Conclusiones)

No es recomendable empezar con Python 2 porque tiene fecha de vencimiento para el próximo año.

**PEP** = Python Enhancement Proposals

Los **PEP** son la forma en la que se define como avanza el lenguaje. Existen tres PEPs que debes saber.

- **PEP8** es la guía de estilo de cómo escribir programas de Python. Es importante escribir de manera similiar para que nuestro software sea legible para el resto de la comunidad
- **PEP257** nos explica cómo generar buena documentación en nuestro código
- **PEP20**

```python
import this

The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
```

# 7. Clases bonus

## Entorno Virtual en Python y su importancia:

```sh
pip
```

Es una utileria de python para manejar los requerimientos de nuestra aplicación

```sh
pip freeze
```

Lista las extensiones instaladas en nuestro systema o entorno virtual activo

```sh
pip freeze > requirements.txt
```

Hace que el listado de extensiones instaladas en nuestra aplicaciones se guarden en un archivo de texto para recrear facilmente nuevos entornos con todas las dependencias de nuestra aplicacion

```python
pip install -r requriments.txt
```

Instala todos los requerimientos que estan en el archivo ‘requeriments.txt’

```sh
source .venv/bin/activate
```

Activa un entorno virual para linux que se encuentra dentro de nuestro directorio actual y que esta en un directorio oculto llamado ‘.venv’

```sh
.venv\Scripts\activate
```

Activa un entorno virtual en sistemas windows que se encuentra en nuestro directorio actual

