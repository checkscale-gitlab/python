// Forma de declara una clase
/* function Account(name, document) {
  this.id;
  this.name = name;
  this.document = document;
  this.email;
  this.password;
} */

// Nueva forma de declara un clase
class Account {
  
  constructor(name, document) {
    this.id;
    this.name = name;
    this.document = document;
    this.email;
    this.password;
  }
}